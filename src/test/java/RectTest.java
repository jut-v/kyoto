import Util.Point2;
import Util.Rect;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RectTest {
  @BeforeAll
  static void beforeAll() {
    System.out.println("Start RectTst");
  }

  @AfterAll
  static void afterAll() {
    System.out.println("End RectTest");
  }

  @Test
  void testExistOn() {
    assertTrue(TestAssist.createRect(0, 0, 10, 10).existOn(TestAssist.createRect(5, 5, 20, 20)));
    assertFalse(TestAssist.createRect(0, 0, 10, 10).existOn(TestAssist.createRect(20, 20, 40, 40)));
  }

  @Test
  void testWholeIn() {
    assertTrue(TestAssist.createRect(5, 5, 10, 10).wholeIn(TestAssist.createRect(7.5f, 7.5f, 8f, 8f)));
    assertFalse(TestAssist.createRect(5, 5, 10, 10).wholeIn(TestAssist.createRect(6, 6, 12, 12)));
  }
}
