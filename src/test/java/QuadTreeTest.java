import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class QuadTreeTest {
  QuadTree quadTree;
  boolean[][] isCollided;
  boolean[][] isCollidedCheck;
  ArrayList<ObstacleModel> obstacleModels;

  @Test
  void LinearQuadTreeSpaceTestCase() {
    isCollided = new boolean[100][100];
    isCollidedCheck = new boolean[100][100];
    quadTree = new QuadTree(1920, 1080, 3);
    obstacleModels = new ArrayList<>();
    String line;
    BufferedReader bufferedReader;
    try {
      bufferedReader = new BufferedReader(new InputStreamReader(QuadTreeTest.class.getResource("LinearQuadTest1.csv").openStream()));
      while ((line = bufferedReader.readLine()) != null) {
        String[] arr = line.split(",");
        obstacleModels.add(TestAssist.createObstacleModel(Float.parseFloat(arr[0]), Float.parseFloat(arr[1]), Float.parseFloat(arr[2]), Float.parseFloat(arr[3])));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    obstacleModels.forEach(o -> {
      quadTree.addObstacleModel(o, o.getBody());
    });
    checkCollision(0, new ArrayList<>());
    for (int i = 0; i < obstacleModels.size(); i++) {
      for (int j = 0; j < i - 1; j++) {
        if (obstacleModels.get(i).getBody().existOn(obstacleModels.get(j).getBody())) {
          isCollided[i][j] = true;
          isCollided[j][i] = true;
        }
      }
    }
    int cnt1 = 0;
    int cnt2 = 0;
    for (int i = 0; i < 100; i++) {
      for (int j = 0; j < 100; j++) {
        if (isCollided[i][j]) {
          cnt1++;
        }
        if (isCollidedCheck[i][j]) {
          cnt2++;
        }
      }
    }
    assertEquals(cnt1, cnt2, 10);
  }

  private void checkCollision(int index, ArrayList<ObstacleModel> checkCollidedModels) {
    ArrayList<ObstacleModel> checkObstacleModels = quadTree.getQuadTree().get(index);
    int length = checkObstacleModels.size();
    for (int i = 0; i < length; i++) {
      for (int j = 0; j < i - 1; j++) {
        ObstacleModel obstacleModel1 = checkObstacleModels.get(i);
        ObstacleModel obstacleModel2 = checkObstacleModels.get(j);
        if (obstacleModel1.getBody().existOn(obstacleModel2.getBody())) {
          isCollidedCheck[obstacleModels.indexOf(obstacleModel1)][obstacleModels.indexOf(obstacleModel2)] = true;
          isCollidedCheck[obstacleModels.indexOf(obstacleModel2)][obstacleModels.indexOf(obstacleModel1)] = true;
        }
      }
    }
    int collidedLength = checkCollidedModels.size();
    for (int i = 0; i < length; i++) {
      for (int j = 0; j < collidedLength; j++) {
        ObstacleModel obstacleModel1 = checkObstacleModels.get(i);
        ObstacleModel obstacleModel2 = checkCollidedModels.get(j);
        if (obstacleModel1.getBody().existOn(obstacleModel2.getBody())) {
          isCollidedCheck[obstacleModels.indexOf(obstacleModel1)][obstacleModels.indexOf(obstacleModel2)] = true;
          isCollidedCheck[obstacleModels.indexOf(obstacleModel2)][obstacleModels.indexOf(obstacleModel1)] = true;
        }
      }
    }

    checkCollidedModels.addAll(checkObstacleModels);
    if (quadTree.getQuadTree().size() > index * 4 + 2) {
      for (int i = 0; i < 4; i++) {
        int nextIndex = index * 4 + 1 + i;
        checkCollision(nextIndex, checkCollidedModels);
      }
    }
  }
}
