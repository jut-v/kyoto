import Util.Point2;
import Util.Rect;

public class TestAssist {
  public static Rect createRect(float x1, float y1, float x2, float y2) {
    return new Rect(new Point2(x1, y1), new Point2(x2, y2));
  }

  public static ObstacleModel createObstacleModel(float x1, float y1, float x2, float y2) {
    return new ObstacleModel(createRect(x1, y1, x2, y2), null);
  }
}
