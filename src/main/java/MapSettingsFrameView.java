import javax.swing.*;

import settingsUI.SErrorDialog;

import java.awt.*;
import java.awt.event.*;

public class MapSettingsFrameView extends JFrame implements ActionListener {
  private EditView ev; //このフレームを呼び出したView
  private MapSettingsView mapSettingsView; //マップの設定を行うパネル
  private MapPlayableSettingsView mapPlayableSettingsView; //プレイヤーのキャラクターの設定を行うパネル
  private ObjectServantSettingsView servantSettingsView; //プレイターの召喚物の設定を行うパネル
  private JTabbedPane tabbedPane; //パネルを切り替えるためのタブ
  private JButton saveButton; //保存ボタン
  
  public MapSettingsFrameView(EditView ev, MapModel mm) {
    super();
    this.ev = ev;
    this.setTitle("マップ設定");
    this.setSize(640, 480);
    this.setLayout(new BorderLayout());
    tabbedPane = new JTabbedPane();
    mapSettingsView = new MapSettingsView(mm);
    mapPlayableSettingsView = new MapPlayableSettingsView(mm.getPlayableCharacterModel());
    servantSettingsView = new ObjectServantSettingsView(mm.getPlayableCharacterModel(), mm);
    saveButton = new JButton("保存");
    saveButton.addActionListener(this);
    tabbedPane.add("マップ設定", mapSettingsView);
    tabbedPane.add("プレイヤー設定", mapPlayableSettingsView);
    tabbedPane.add("プレイヤー召喚物設定", servantSettingsView);
    this.add(tabbedPane, BorderLayout.CENTER);
    this.add(saveButton, BorderLayout.SOUTH);
    this.setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == saveButton) {
      try {
        mapSettingsView.setValues();
        mapPlayableSettingsView.setValues();
        servantSettingsView.setValues();
        ev.updateView();
      } catch (Exception ex) {
        new SErrorDialog(ex);
      }
    }
  }
}
