package Util.gamedata;

import Util.Rect;

import java.util.Objects;

// cameraの保存時のモデル
public final class CameraModelData {
  private final Rect camPos;

  public CameraModelData(Rect camPos) {
    this.camPos = camPos;
  }

  public Rect camPos() {
    return camPos;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (CameraModelData) obj;
    return Objects.equals(this.camPos, that.camPos);
  }

  @Override
  public int hashCode() {
    return Objects.hash(camPos);
  }

  @Override
  public String toString() {
    return "Util.gamedata.CameraModelData[" +
      "camPos=" + camPos + ']';
  }

}
