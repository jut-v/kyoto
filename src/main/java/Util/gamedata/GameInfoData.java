package Util.gamedata;

import java.util.Objects;

// GameInfoの保存時のModel
public final class GameInfoData {
  private final String thumbnailPath;
  private final String gameName;
  private final String gameUUID;

  public GameInfoData(String thumbnailPath, String gameName, String gameUUID) {
    this.thumbnailPath = thumbnailPath;
    this.gameName = gameName;
    this.gameUUID = gameUUID;
  }

  public String thumbnailPath() {
    return thumbnailPath;
  }

  public String gameName() {
    return gameName;
  }

  public String gameUUID() {
    return gameUUID;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (GameInfoData) obj;
    return Objects.equals(this.thumbnailPath, that.thumbnailPath) &&
      Objects.equals(this.gameName, that.gameName) &&
      Objects.equals(this.gameUUID, that.gameUUID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(thumbnailPath, gameName, gameUUID);
  }

  @Override
  public String toString() {
    return "Util.gamedata.GameInfoData[" +
      "thumbnailPath=" + thumbnailPath + ", " +
      "gameName=" + gameName + ", " +
      "gameUUID=" + gameUUID + ']';
  }

}
