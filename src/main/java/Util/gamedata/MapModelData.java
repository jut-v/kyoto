package Util.gamedata;

import Util.MapScrollDirection;

import java.util.ArrayList;
import java.util.Objects;

// MapModelDataの保存時のModel
public final class MapModelData {
  private final String mapName;
  private final String backgroundImageFileName;
  private final ArrayList<ObstacleModelData> obstacleModelData;
  private final PlayableCharacterModelData playableCharacterModelData;
  private final CameraModelData cameraModelData;
  private final float mapScrollValue;
  private final MapScrollDirection mapScrollDirection;
  private final String musicFileName;

  public MapModelData(String mapName, String backgroundImageFileName, ArrayList<ObstacleModelData> obstacleModelData, PlayableCharacterModelData playableCharacterModelData, CameraModelData cameraModelData, float mapScrollValue, MapScrollDirection mapScrollDirection, String musicFileName) {
    this.mapName = mapName;
    this.backgroundImageFileName = backgroundImageFileName;
    this.obstacleModelData = obstacleModelData;
    this.playableCharacterModelData = playableCharacterModelData;
    this.cameraModelData = cameraModelData;
    this.mapScrollValue = mapScrollValue;
    this.mapScrollDirection = mapScrollDirection;
    this.musicFileName = musicFileName;
  }

  public String mapName() {
    return mapName;
  }

  public String backgroundImageFileName() {
    return backgroundImageFileName;
  }

  public ArrayList<ObstacleModelData> obstacleModelData() {
    return obstacleModelData;
  }

  public PlayableCharacterModelData playableCharacterModelData() {
    return playableCharacterModelData;
  }

  public CameraModelData cameraModelData() {
    return cameraModelData;
  }

  public float mapScrollValue() {
    return mapScrollValue;
  }

  public MapScrollDirection mapScrollDirection() {
    return mapScrollDirection;
  }

  public String musicFileName() {
    return musicFileName;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (MapModelData) obj;
    return Objects.equals(this.mapName, that.mapName) &&
      Objects.equals(this.backgroundImageFileName, that.backgroundImageFileName) &&
      Objects.equals(this.obstacleModelData, that.obstacleModelData) &&
      Objects.equals(this.playableCharacterModelData, that.playableCharacterModelData) &&
      Objects.equals(this.cameraModelData, that.cameraModelData) &&
      Objects.equals(this.mapScrollValue, that.mapScrollValue) &&
      Objects.equals(this.mapScrollDirection, that.mapScrollDirection) &&
      Objects.equals(this.musicFileName, that.musicFileName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mapName, backgroundImageFileName, obstacleModelData, playableCharacterModelData, cameraModelData, mapScrollValue, mapScrollDirection, musicFileName);
  }

  @Override
  public String toString() {
    return "Util.gamedata.MapModelData[" +
      "mapName=" + mapName + ", " +
      "backgroundImageFileName=" + backgroundImageFileName + ", " +
      "obstacleModelData=" + obstacleModelData + ", " +
      "playableCharacterModelData=" + playableCharacterModelData + ", " +
      "cameraModelData=" + cameraModelData + ", " +
      "mapScrollValue=" + mapScrollValue + ", " +
      "mapScrollDirection" + mapScrollDirection +
      "musicPath" + musicFileName + ']';
  }

}
