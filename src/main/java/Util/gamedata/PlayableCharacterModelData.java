package Util.gamedata;

import Util.Point2;
import Util.Rect;
import Util.Vector2;

import java.util.Objects;

// PlayableCharacterModelDataの保存時のModel
public final class PlayableCharacterModelData {
  private final Rect body;
  private final Rect figBody;
  private final String backgroundFileName;
  private final Vector2 sensitivity;
  private final String generateFormulaX;
  private final String generateFormulaY;
  private final int hitPoint;
  private final ObstacleModelData servant;
  private final String supervisorFormula;
  private final ObstacleModelData obstacleModelReincarnation;
  private final PlayableCharacterModelData playableCharacterReincarnation;
  private final Point2 revivalPoint;

  public PlayableCharacterModelData(Rect body, Rect figBody, String backgroundFileName, Vector2 sensitivity, String generateFormulaX, String generateFormulaY, int hitPoint, ObstacleModelData servant, String supervisorFormula, ObstacleModelData obstacleModelReincarnation, PlayableCharacterModelData playableCharacterReincarnation, Point2 revivalPoint) {
    this.body = body;
    this.figBody = figBody;
    this.backgroundFileName = backgroundFileName;
    this.sensitivity = sensitivity;
    this.generateFormulaX = generateFormulaX;
    this.generateFormulaY = generateFormulaY;
    this.hitPoint = hitPoint;
    this.servant = servant;
    this.supervisorFormula = supervisorFormula;
    this.obstacleModelReincarnation = obstacleModelReincarnation;
    this.playableCharacterReincarnation = playableCharacterReincarnation;
    this.revivalPoint = revivalPoint;
  }

  public Rect body() {
    return body;
  }

  public Rect figBody() {
    return figBody;
  }

  public String backgroundFileName() {
    return backgroundFileName;
  }

  public Vector2 sensitivity() {
    return sensitivity;
  }

  public String generateFormulaX() {
    return generateFormulaX;
  }

  public String generateFormulaY() {
    return generateFormulaY;
  }

  public int hitPoint() {
    return hitPoint;
  }

  public ObstacleModelData servant() {
    return servant;
  }

  public String supervisorFormula() {
    return supervisorFormula;
  }

  public ObstacleModelData obstacleModelReincarnation() {
    return obstacleModelReincarnation;
  }

  public PlayableCharacterModelData playableCharacterReincarnation() {
    return playableCharacterReincarnation;
  }

  public Point2 revivalPoint() {
    return revivalPoint;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (PlayableCharacterModelData) obj;
    return Objects.equals(this.body, that.body) &&
      Objects.equals(this.figBody, that.figBody) &&
      Objects.equals(this.backgroundFileName, that.backgroundFileName) &&
      Objects.equals(this.sensitivity, that.sensitivity) &&
      Objects.equals(this.generateFormulaX, that.generateFormulaX) &&
      Objects.equals(this.generateFormulaY, that.generateFormulaY) &&
      this.hitPoint == that.hitPoint &&
      Objects.equals(this.servant, that.servant) &&
      Objects.equals(this.supervisorFormula, that.supervisorFormula) &&
      Objects.equals(this.obstacleModelReincarnation, that.obstacleModelReincarnation) &&
      Objects.equals(this.playableCharacterReincarnation, that.playableCharacterReincarnation) &&
      Objects.equals(this.revivalPoint, that.revivalPoint);
  }

  @Override
  public int hashCode() {
    return Objects.hash(body, figBody, backgroundFileName, sensitivity, generateFormulaX, generateFormulaY, hitPoint, servant, supervisorFormula, obstacleModelReincarnation, playableCharacterReincarnation, revivalPoint);
  }

  @Override
  public String toString() {
    return "Util.gamedata.PlayableCharacterModelData[" +
      "body=" + body + ", " +
      "figBody=" + figBody + ", " +
      "backgroundFileName=" + backgroundFileName + ", " +
      "sensitivity=" + sensitivity + ", " +
      "generateFormulaX=" + generateFormulaX + ", " +
      "generateFormulaY=" + generateFormulaY + ", " +
      "hitPoint" + hitPoint + ", " +
      "servant=" + servant + ", " +
      "supervisorFormula=" + supervisorFormula + ", " +
      "obstacleModelReincarnation=" + obstacleModelReincarnation + ", " +
      "playableCharacterReincarnation=" + playableCharacterReincarnation + ", " +
      "revivalPoint=" + revivalPoint + ']';
  }

}
