package Util.gamedata;

import Util.ObjectCamp;
import Util.Point2;
import Util.ReactionMode;
import Util.Rect;

import java.util.Objects;

// ObstacleModelDataの保存時のModel
public final class ObstacleModelData {
  private final Rect body;
  private final Rect figBody;
  private final String backgroundFileName;
  private final String bodyFormulaX;
  private final String bodyFormulaY;
  private final String generateFormulaX;
  private final String generateFormulaY;
  private final int hitPoint;
  private final int collisionDamage;
  private final ReactionMode reactionMode;
  private final ObjectCamp objectCamp;
  private final ObstacleModelData servant;
  private final String supervisorFormula;
  private final ObstacleModelData ObstacleReincarnation;
  private final PlayableCharacterModelData playableReincarnation;
  private final Point2 revivalPoint;
  private final boolean teleportKey;

  public ObstacleModelData(Rect body, Rect figBody, String backgroundFileName, String bodyFormulaX, String bodyFormulaY, String generateFormulaX, String generateFormulaY, int hitPoint, int collisionDamage, ReactionMode reactionMode, ObjectCamp objectCamp, ObstacleModelData servant, String supervisorFormula, ObstacleModelData ObstacleReincarnation, PlayableCharacterModelData playableReincarnation, Point2 revivalPoint, boolean teleportKey) {
    this.body = body;
    this.figBody = figBody;
    this.backgroundFileName = backgroundFileName;
    this.bodyFormulaX = bodyFormulaX;
    this.bodyFormulaY = bodyFormulaY;
    this.generateFormulaX = generateFormulaX;
    this.generateFormulaY = generateFormulaY;
    this.hitPoint = hitPoint;
    this.collisionDamage = collisionDamage;
    this.reactionMode = reactionMode;
    this.objectCamp = objectCamp;
    this.servant = servant;
    this.supervisorFormula = supervisorFormula;
    this.ObstacleReincarnation = ObstacleReincarnation;
    this.playableReincarnation = playableReincarnation;
    this.revivalPoint = revivalPoint;
    this.teleportKey = teleportKey;
  }

  public Rect body() {
    return body;
  }

  public Rect figBody() {
    return figBody;
  }

  public String backgroundFileName() {
    return backgroundFileName;
  }

  public String bodyFormulaX() {
    return bodyFormulaX;
  }

  public String bodyFormulaY() {
    return bodyFormulaY;
  }

  public String generateFormulaX() {
    return generateFormulaX;
  }

  public String generateFormulaY() {
    return generateFormulaY;
  }

  public int hitPoint() {
    return hitPoint;
  }

  public int collisionDamage() {
    return collisionDamage;
  }

  public ReactionMode reactionMode() {
    return reactionMode;
  }

  public ObjectCamp objectCamp() {
    return objectCamp;
  }

  public ObstacleModelData servant() {
    return servant;
  }

  public String supervisorFormula() {
    return supervisorFormula;
  }

  public ObstacleModelData ObstacleReincarnation() {
    return ObstacleReincarnation;
  }

  public PlayableCharacterModelData playableReincarnation() {
    return playableReincarnation;
  }

  public Point2 revivalPoint() {
    return revivalPoint;
  }

  public boolean teleportKey() {
    return teleportKey;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (ObstacleModelData) obj;
    return Objects.equals(this.body, that.body) &&
      Objects.equals(this.figBody, that.figBody) &&
      Objects.equals(this.backgroundFileName, that.backgroundFileName) &&
      Objects.equals(this.bodyFormulaX, that.bodyFormulaX) &&
      Objects.equals(this.bodyFormulaY, that.bodyFormulaY) &&
      Objects.equals(this.generateFormulaX, that.generateFormulaX) &&
      Objects.equals(this.generateFormulaY, that.generateFormulaY) &&
      this.hitPoint == that.hitPoint &&
      this.collisionDamage == that.collisionDamage &&
      Objects.equals(this.reactionMode, that.reactionMode) &&
      Objects.equals(this.objectCamp, that.objectCamp) &&
      Objects.equals(this.servant, that.servant) &&
      Objects.equals(this.supervisorFormula, that.supervisorFormula) &&
      Objects.equals(this.ObstacleReincarnation, that.ObstacleReincarnation) &&
      Objects.equals(this.playableReincarnation, that.playableReincarnation) &&
      Objects.equals(this.revivalPoint, that.revivalPoint) &&
      this.teleportKey == that.teleportKey;
  }

  @Override
  public int hashCode() {
    return Objects.hash(body, figBody, backgroundFileName, bodyFormulaX, bodyFormulaY, generateFormulaX, generateFormulaY, hitPoint, collisionDamage, reactionMode, objectCamp, servant, supervisorFormula, ObstacleReincarnation, playableReincarnation, revivalPoint, teleportKey);
  }

  @Override
  public String toString() {
    return "Util.gamedata.ObstacleModelData[" +
      "body=" + body + ", " +
      "figBody=" + figBody + ", " +
      "backgroundFileName=" + backgroundFileName + ", " +
      "bodyFormulaX=" + bodyFormulaX + ", " +
      "bodyFormulaY=" + bodyFormulaY + ", " +
      "generateFormulaX=" + generateFormulaX + ", " +
      "generateFormulaY=" + generateFormulaY + ", " +
      "hitPoint=" + hitPoint + ", " +
      "collisionDamage=" + collisionDamage + ", " +
      "reactionMode=" + reactionMode + ", " +
      "objectCamp=" + objectCamp + ", " +
      "servant=" + servant + ", " +
      "supervisorFormula=" + supervisorFormula + ", " +
      "ObstacleReincarnation=" + ObstacleReincarnation + ", " +
      "playableReincarnation=" + playableReincarnation + ", " +
      "revivalPoint=" + revivalPoint + ", " +
      "teleportKey=" + teleportKey + ']';
  }

}
