package Util.gamedata;

import Util.GameMode;

import java.util.ArrayList;
import java.util.Objects;

// GameMasterDataの保存時のModel
public final class GameMasterData {
  private final String gameName;
  private final GameMode gameMode;
  private final ArrayList<MapModelData> mapModel;

  public GameMasterData(String gameName, GameMode gameMode, ArrayList<MapModelData> mapModel) {
    this.gameName = gameName;
    this.gameMode = gameMode;
    this.mapModel = mapModel;
  }

  public String gameName() {
    return gameName;
  }

  public GameMode gameMode() {
    return gameMode;
  }

  public ArrayList<MapModelData> mapModel() {
    return mapModel;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (GameMasterData) obj;
    return Objects.equals(this.gameName, that.gameName) &&
      Objects.equals(this.gameMode, that.gameMode) &&
      Objects.equals(this.mapModel, that.mapModel);
  }

  @Override
  public int hashCode() {
    return Objects.hash(gameName, gameMode, mapModel);
  }

  @Override
  public String toString() {
    return "Util.gamedata.GameMasterData[" +
      "gameName=" + gameName + ", " +
      "gameMode=" + gameMode + ", " +
      "mapModel=" + mapModel + ']';
  }

}
