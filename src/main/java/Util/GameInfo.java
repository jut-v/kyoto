package Util;

import java.awt.*;

// Gameの概要を保持するclass
public class GameInfo {
  private final Image thumbnail;
  private final String title;
  private final String gameUUID;

  public GameInfo(Image thumbnail, String title, String gameUUID) {
    this.thumbnail = thumbnail;
    this.title = title;
    this.gameUUID = gameUUID;
  }

  public String getTitle() {
    return title;
  }

  public Image getThumbnail() {
    return thumbnail;
  }

  public String getGameUUID() {
    return gameUUID;
  }
}
