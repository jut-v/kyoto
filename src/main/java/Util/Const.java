package Util;

import java.awt.event.*;

public class Const {
  public static String gamePath;
  public static final int windowWidth = 1280;
  public static final int windowHeight = 720;
  public static final float FPS=30;
  public static final String defaultMapBackground = "/background.png";
  public static final String defaultGameName = "無題のゲーム";
  public static final String defaultSaveDataFolder = "saveData";
  public static final String defaultSaveDataFileExtension = "kyoto";
  public static final String defaultGameInfoFileName = "gameinfo.info";
  public static final String defaultGameDataFileName = "data.json";
  public static final String defaultMapName = "マップ";
  public static final String defaultServantPresetName = "召喚物プリセット";
  public static final int KEY_UP = KeyEvent.VK_UP;
  public static final int KEY_DOWN = KeyEvent.VK_DOWN;
  public static final int KEY_LEFT = KeyEvent.VK_LEFT;
  public static final int KEY_RIGHT = KeyEvent.VK_RIGHT;
  public static final int PRIMALY_KEY = KeyEvent.VK_SPACE;
  public static final int QUIT_KEY = KeyEvent.VK_ESCAPE;
  public static final String PRIMALY_KEY_NAME = "Space キー";
  public static final String QUIT_KEY_NAME = "Escape キー";
}
