package Util.io;

import Util.Const;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.Collection;

public final class KFileIO {
  public static Path getApplicationPath() throws URISyntaxException {
    ProtectionDomain protectionDomain = KFileIO.class.getProtectionDomain();
    CodeSource codeSource = protectionDomain.getCodeSource();
    URL url = codeSource.getLocation();
    URI uri = url.toURI();
    Path path = Paths.get(uri);
    return path.getParent();
  }

  public static Path getSaveDataDir() throws URISyntaxException, IOException {
    Path saveDir = getApplicationPath().resolve(Const.defaultSaveDataFolder);
    if (!Files.exists(saveDir)) {
      Files.createDirectory(saveDir);
    }
    return saveDir;
  }

  public static Path getSaveGameDataDir(String gameName) throws URISyntaxException, IOException {
    Path saveGameDataDir = getSaveDataDir().resolve(gameName);
    if (!Files.exists(saveGameDataDir)) {
      Files.createDirectory(saveGameDataDir);
    }
    return saveGameDataDir;
  }

  public static void deleteDir(File file) throws IOException {
    FileUtils.deleteDirectory(file);
  }

  public static void renameFileOrDir(File oldFile, String renameName) throws IOException {
    File newNameFile = oldFile.toPath().getParent().resolve(renameName).toFile();
    boolean isSuccess = oldFile.renameTo(newNameFile);
    if (!isSuccess) {
      throw new IOException("rename failed");
    }
  }

  public static void copyFile(Path originalFile, Path copyFile) throws IOException {
    Files.copy(originalFile, copyFile);
  }

  public static Collection<File> suffixFileSearch(Path searchDir, String suffix) {
    return FileUtils.listFiles(searchDir.toFile(), FileFilterUtils.suffixFileFilter(suffix), FileFilterUtils.trueFileFilter());
  }

  public static void compressFileOrDirWithZip(File file, String extension) throws IOException {
    ZipParameters zipParameters = new ZipParameters();
    if (file.isFile()) {
      int index = file.getName().lastIndexOf(".");
      if (index != -1) {
        try (ZipFile zipFile = new ZipFile(file.getParent() + file.getName().substring(0, index) + "." + extension)) {
          zipFile.addFile(file, zipParameters);
        }
      } else {
        try(ZipFile zipFile = new ZipFile(file.getAbsolutePath() + "." + extension)) {
          zipFile.addFile(file, zipParameters);
        }
      }
    } else {
      try(ZipFile zipFile = new ZipFile(file.getAbsolutePath() + "." + extension)) {
        zipFile.addFolder(file, zipParameters);
      }
    }
  }

  public static Path getSaveDataFile(String gameName) throws URISyntaxException, IOException {
    return getSaveDataDir().resolve(gameName);
  }
}
