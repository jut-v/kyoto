package Util.io;

import Util.DataBufferedImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.PixelGrabber;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.util.Hashtable;

public final class KImageIO {
  public static DataBufferedImage copyImage(DataBufferedImage bufferedImage) {
    DataBufferedImage copiedImage = new DataBufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), bufferedImage.getType());
    copiedImage.setData(bufferedImage.getData());
    return copiedImage;
  }

  public static void saveBufferedImage(BufferedImage bufferedImage, Path path) throws IOException {
    ImageIO.write(bufferedImage, "png", path.toFile());
  }

  public static boolean imageReadable(String path){
    return imageReadable(new File(path));
  }

  public static boolean imageReadable(Path filePath) {
    return imageReadable(filePath.toFile());
  }

  public static boolean imageReadable(File file) {
    try {
      imageRead(file);
    } catch (IOException | NullPointerException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  public static boolean imageReadable(URL url) {
    try {
      imageRead(url);
    } catch (IOException | NullPointerException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  public static DataBufferedImage imageRead(String filePath) throws IOException {
    return imageRead(new File(filePath));
  }

  public static DataBufferedImage imageRead(Path filePath) throws IOException {
    return imageRead(filePath.toFile());
  }

  public static DataBufferedImage imageRead(File filePath) throws IOException {
    return new DataBufferedImage(ImageIO.read(filePath), filePath.toPath());
  }

  public static DataBufferedImage imageRead(URL fileURL) throws IOException {
    return new DataBufferedImage(ImageIO.read(fileURL));
  }

  public static DataBufferedImage imageRead(InputStream inputStream) throws IOException {
    return new DataBufferedImage(ImageIO.read(inputStream));
  }

  public static BufferedImage trimWithAspectRatio(BufferedImage bufferedImage, int widthRatio, int heightRatio) {
    int imageWidth = bufferedImage.getWidth();
    int imageHeight = bufferedImage.getHeight();
    int widthCoefficient = imageWidth / widthRatio;
    int heightCoefficient = imageHeight / heightRatio;
    int coefficient = Math.min(widthCoefficient, heightCoefficient);
    return bufferedImage.getSubimage(0, 0, coefficient * widthRatio, coefficient * heightRatio);
  }

  public static DataBufferedImage convertDataBufferedImage(Image image) throws InterruptedException {
    if (image instanceof DataBufferedImage bufferedImage) {
      return bufferedImage;
    }
    MediaTracker tracker = new MediaTracker(new Component() {});
    tracker.addImage(image, 0);
    tracker.waitForAll();
    PixelGrabber pixelGrabber = new PixelGrabber(image, 0, 0, -1, -1, false);
    pixelGrabber.grabPixels();
    ColorModel colorModel = pixelGrabber.getColorModel();
    int width = pixelGrabber.getWidth();
    int height = pixelGrabber.getHeight();
    WritableRaster writableRaster = colorModel.createCompatibleWritableRaster(width, height);
    DataBufferedImage dataBufferedImage = new DataBufferedImage(
      colorModel,
      writableRaster,
      colorModel.isAlphaPremultiplied(),
      new Hashtable<>()
    );
    dataBufferedImage.getRaster().setDataElements(0, 0, width, height, pixelGrabber.getPixels());
    return dataBufferedImage;
  }

  public static DataBufferedImage copyDataBufferedImage(DataBufferedImage dataBufferedImage) {
    return new DataBufferedImage(dataBufferedImage, dataBufferedImage.getFilePath());
  }
}
