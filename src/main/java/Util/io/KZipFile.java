package Util.io;

import Util.DataBufferedImage;
import org.apache.commons.io.IOUtils;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class KZipFile implements Closeable {

  private final ZipFile zipFile;
  private final HashMap<String, ZipEntry> zipEntryHashMap = new HashMap<>();

  public KZipFile(ZipFile zipFile) {
    this.zipFile = zipFile;
    zipFile.stream().forEach(entry -> {
      String name = entry.getName();
      int index = name.lastIndexOf("/");
      if (index == -1) {
        zipEntryHashMap.put(entry.getName(), entry);
      } else {
        zipEntryHashMap.put(name.substring(index + 1), entry);
      }
    });
  }

  public String getText(String fileName) throws IOException {
    String text;
    try (InputStream inputStream = zipFile.getInputStream(zipEntryHashMap.get(fileName))) {
      text = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
    }
    return text;
  }

  public DataBufferedImage getDataBufferedImage(String fileName) throws IOException {
    DataBufferedImage dataBufferedImage;
    try (InputStream inputStream = zipFile.getInputStream(zipEntryHashMap.get(fileName))) {
      dataBufferedImage = KImageIO.imageRead(inputStream);
    }
    return dataBufferedImage;
  }

  public String getMusicPath(String fileName) throws IOException, UnsupportedAudioFileException {
    try (InputStream inputStream = zipFile.getInputStream(zipEntryHashMap.get(fileName))) {
      File tmpFile;
      try (BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)) {
        try (AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(bufferedInputStream)) {
          tmpFile = File.createTempFile("audio", ".tmp");
          AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, tmpFile);
        }
      }
      tmpFile.deleteOnExit();
      return tmpFile.getAbsolutePath();
    }
  }

  @Override
  public void close() throws IOException {
    zipFile.close();
  }
}
