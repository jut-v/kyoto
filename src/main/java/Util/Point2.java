package Util;

public class Point2 implements Cloneable {
  private float x;
  private float y;
  public Point2() {
    this.x = 0.0f;
    this.y = 0.0f;
  }

  public Point2(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public Point2(Point2 p) {
    this.x = p.getX();
    this.y = p.getY();
  }

  public float getX() {
    return x;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getY() {
    return y;
  }

  public void setY(float y) {
    this.y = y;
  }

  public void set(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public void move(float x, float y){
    this.x += x;
    this.y += y;
  }

  public float calcDistance() {
    return (float) Math.sqrt(x * x + y * y);
  }

  public float calcDistance(Point2 point2) {
    return (float) Math.sqrt((this.x - point2.x) * (this.x - point2.x) + (this.y * point2.y) * (this.y * point2.y));
  }

  public float calcSqrDistance() {
    return x * x + y * y;
  }

  public float calcSqrDistance(Point2 point2) {
    return (this.x - point2.x) * (this.x - point2.x) + (this.y - point2.y) + (this.y - point2.y);
  }

  public Point2 addX(float x) {
    return new Point2(this.x + x, this.y);
  }

  public Point2 addY(float y) {
    return new Point2(this.x, this.y + y);
  }

  public Point2 addXY(float x, float y) {
    return new Point2(this.x + x, this.y + y);
  }

  public Point2 add(Vector2 v) {
    return new Point2(this.x + v.getX(), this.y + v.getY());
  }

  @Override
  public Point2 clone() {
    try {
      return (Point2) super.clone();
    } catch (CloneNotSupportedException e) {
      throw new AssertionError();
    }
  }
}
