package Util;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.nio.file.Path;
import java.util.Hashtable;
import java.util.UUID;

public class DataBufferedImage extends BufferedImage {
  private final String uuid = UUID.randomUUID().toString();
  private Path filePath = null;

  public DataBufferedImage(BufferedImage bufferedImage, Path filePath) {
    super(bufferedImage.getWidth(), bufferedImage.getHeight(), bufferedImage.getType());
    this.setData(bufferedImage.getData());
    this.filePath = filePath;
  }

  public DataBufferedImage(BufferedImage bufferedImage) {
    super(bufferedImage.getWidth(), bufferedImage.getHeight(), bufferedImage.getType());
    this.setData(bufferedImage.getData());
  }

  public DataBufferedImage(int width, int height, int imageType) {
    super(width, height, imageType);
  }

  public DataBufferedImage(int width, int height, int imageType, Path filePath) {
    super(width, height, imageType);
    this.filePath = filePath;
  }

  public DataBufferedImage(int width, int height, int imageType, IndexColorModel cm) {
    super(width, height, imageType, cm);
  }

  public DataBufferedImage(ColorModel cm, WritableRaster raster, boolean isRasterPremultiplied, Hashtable<?, ?> properties) {
    super(cm, raster, isRasterPremultiplied, properties);
  }

  public String getUUID() {
    return uuid;
  }

  public Path getFilePath() {
    return filePath;
  }
}
