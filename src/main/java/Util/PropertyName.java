package Util;

public class PropertyName {
  public static final String GAME_MODE = "gameMode";
  public static final String ADDED_OBSTACLE_MODEL = "addedObstacleModel";
  public static final String REMOVED_OBSTACLE_MODEL = "removedObstacleModel";
  public static final String ADDED_ACTIVE_OBSTACLE_MODEL = "addedActiveObstacleModel";
  public static final String REMOVED_ACTIVE_OBSTACLE_MODEL = "removedActiveObstacleModel";
  public static final String SET_PLAYABLE_CHARACTER_MODEL = "setPlayableCharacterModel";
  public static final String CHANGE_MAP_BACKGROUND = "changeMapBackground";
  public static final String ADDED_MAP_MODEL = "addedMapModel";
  public static final String MOVED_PLAYABLE_CHARACTER_BODY = "movedPlayableCharacterBody";
  public static final String MOVED_PLAYABLE_CHARACTER_FIG_BODY = "movedPlayableCharacterFigBody";
  public static final String CHANGED_GAME_NAME = "changedGameName";
  public static final String MOVED_CAMERA_MODEL = "movedCameraModel";
  public static final String UPDATED_OBJECTS = "updatedObjects";
  public static final String GAME_STARTED = "gameStarted";
  public static final String GAME_FINISHED = "gameFinished";
  public static final String GAME_RESTARTED = "gameRestarted";
  public static final String GAME_OVERED = "gameOvered";
  public static final String MAP_CHANGED = "mapChanged";
  public static final String GAME_LOADED = "gameLoaded";
  public static final String GAME_ERROR = "gameError";
}
