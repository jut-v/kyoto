package Util;

public class Vector2 implements Cloneable {
  private float x;
  private float y;
  public Vector2() {
    this.x = 0;
    this.y = 0;
  }

  public Vector2(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public Vector2(Point2 from, Point2 to) {
    this.x = to.getX() - from.getX();
    this.y = to.getY() - from.getY();
  }

  public float getX() {
    return x;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getY() {
    return y;
  }

  public void setY(float y) {
    this.y = y;
  }

  public void set(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public float calcMagnitude() {
    return (float) Math.sqrt(x * x + y * y);
  }

  public float calcSqrMagnitude() {
    return x * x + y * y;
  }

  public Vector2 normalized() {
    float magnitude = calcMagnitude();
    if (magnitude == 0) {
      throw new ArithmeticException("長さが0のためnormalizeできません");
    }
    return new Vector2(x / magnitude, y / magnitude);
  }

  public Vector2 add(Vector2 vector2) {
    return new Vector2(this.x + vector2.x, this.y + vector2.y);
  }

  public Vector2 sub(Vector2 vector2) {
    return new Vector2(this.x - vector2.x, this.y - vector2.y);
  }

  public float dotProducts(Vector2 vector2) {
    return this.x * vector2.x + this.y * vector2.y;
  }

  public float arg(Vector2 vector2) {
    if (this.x == 0 && this.y == 0) {
      throw new ArithmeticException("thisのベクトルが0ベクトルです");
    }
    if (vector2.x == 0 && vector2.y == 0) {
      throw new ArithmeticException("引数のベクトルが0ベクトルです");
    }
    return (float) Math.acos(dotProducts(vector2) / this.calcMagnitude() * vector2.calcSqrMagnitude());
  }

  public Vector2 inverse() {
    return new Vector2(-this.x, -this.y);
  }

  @Override
  public Vector2 clone() {
    try {
      return (Vector2) super.clone();
    } catch (CloneNotSupportedException e) {
      throw new AssertionError();
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    if (obj instanceof Vector2 that) {
      return this.x == that.x && this.y == that.y;
    }
    return false;
  }
}
