package Util;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

//数式を用いてマップ上での動きを定義、処理するクラス
public class Behavior {
  private Expression[] exps=new Expression[2];//数式処理クラスを保持
  private float a=1,b=1,summonerT=0;//x, y軸に関して対称な動きに切り替える時はそれぞれa, bが-1となる。summonerTは召喚者の召喚時刻を保持するために使う。
  private final String formulaX,formulaY;//数式を保持
  public Behavior(String formulaX,String formulaY){
    ExpressionBuilder[] expBuilds=new ExpressionBuilder[2];
    expBuilds[0]=new ExpressionBuilder(formulaX);
    expBuilds[1]=new ExpressionBuilder(formulaY);
    exps[0]=expBuilds[0].variables("t","T").build();
    exps[1]=expBuilds[1].variables("t","T").build();
    this.formulaX=formulaX;
    this.formulaY=formulaY;
  }
  public Behavior(Vector2 vector,float time){
    float x=vector.getX(),y=vector.getY();
    formulaX=String.format("%gt", x/time);
    formulaY=String.format("%gt", y/time);
    new Behavior(formulaX,formulaY);
  }
  public Behavior(Behavior b){
    exps=b.exps;formulaX=b.formulaX;formulaY=b.formulaY;
  }
  //tの時にいるべき座標を返す
  public Point2 func(float t){
    exps[0].setVariable("T", summonerT);exps[1].setVariable("T", summonerT);
    exps[0].setVariable("t", t);exps[1].setVariable("t", t);
    return new Point2(a*(float)(exps[0].evaluate()),b*(float)(exps[1].evaluate()));
  }
  public void mirrorX(){
    a*=-1;
  }
  public void mirrorY(){
    b*=-1;
  }
  public void setSummonerT(float summonerT){
    this.summonerT=summonerT;
  }
  public String getFormulaX(){
    return formulaX;
  }
  public String getFormulaY(){
    return formulaY;
  }
}
