package Util;

public class Rect {
    private Point2 p1, p2;
    public static int XSMALL = 0;
    public static int XLARGE = 1;
    public static int YSMALL = 0;
    public static int YLARGE = 1;
    public static final int ABOVE=0;
    public static final int BELOW=1;
    public static final int RIGHT=2;
    public static final int LEFT=3;

  public Rect() {
    this.p1 = new Point2();
    this.p2 = new Point2();
    normalize();
  }

  public Rect(Point2 p1, Point2 p2) {
    this.p1 = p1;
    this.p2 = p2;
    normalize();
  }

  public Rect(Point2 p, float w, float h) {
    this.p1 = p;
    this.p2 = new Point2(p.getX() + w, p.getY() + h);
    normalize();
  }

  public Rect(Rect rt){
    this.p1=rt.p1.clone();this.p2=rt.p2.clone();
  }

  public Point2 getP1() {
    return p1;
  }

  public Point2 getP2() {
    return p2;
  }

  public void setPoint(Point2 p1,Point2 p2){
    this.p1=p1.clone();this.p2=p2.clone();
  }

  public void moveTo(Point2 p){
    Point2 m=this.getMedian();
    float dx=p.getX()-m.getX(),dy=p.getY()-m.getY();
    p1.set(p1.getX()+dx, p1.getY()+dy);p2.set(p2.getX()+dx, p2.getY()+dy);
  }

  public void move(float deltaX, float deltaY) {
    p1.set(p1.getX() + deltaX, p1.getY() + deltaY);
    p2.set(p2.getX() + deltaX, p2.getY() + deltaY);
  }

  public boolean hasPoint2(Point2 p) {
    return (p1.getX() <= p.getX())
        && (p.getX() <= p2.getX())
        && (p1.getY() <= p.getY())
        && (p.getY() <= p2.getY());
  }

  private void normalize() {
    float p1x = p1.getX(), p1y = p1.getY();
    float p2x = p2.getX(), p2y = p2.getY();
    if (p1x > p2x) {
      p1.setX(p2x);p2.setX(p1x);
    }
    if (p1y > p2y) {
      p1.setY(p2y);p2.setY(p1y);
    }
  }

  public boolean existOn(Rect r) {
    return Math.max(this.p1.getX(), r.p1.getX()) <= Math.min(this.p2.getX(), r.p2.getX())
      && Math.max(this.p1.getY(), r.p1.getY()) <= Math.min(this.p2.getY(), r.p2.getY());
  }

  public boolean wholeIn(Rect r) {
    return (p1.getX() <= r.p1.getX())
      && (p1.getY() <= r.p1.getY())
      && (p2.getX() >= r.p2.getX())
      && (p2.getY() >= r.p2.getY());
  }

  public Point2 getMedian(){
    return new Point2((p1.getX()+p2.getX())/2,(p1.getY()+p2.getY())/2);
  }

  public float getWidth() {
    return p2.getX() - p1.getX();
  }

  public float getHeight() {
    return p2.getY() - p1.getY();
  }

  public Point2 getPoint(int positionX, int positionY) {
    float px = p1.getX();
    float py = p1.getY();
    if(positionX == XLARGE) px = p2.getX();
    if(positionY == YLARGE) py = p2.getY();
    return new Point2(px, py);
  }

  public int positionalRelation(Rect rt){
    float thisX1=p1.getX(),thisX2=p2.getX(),thisY1=p1.getY(),thisY2=p2.getY(),
          rtX1=rt.p1.getX(),rtX2=rt.p2.getX(),rtY1=rt.p1.getY(),rtY2=rt.p2.getY();
    boolean diffX1=thisX1>rtX1,diffX2=thisX2<=rtX2,
            diffY1=thisY1>rtY1,diffY2=thisY2<=rtY2;
    int xRelation=((Boolean)(diffX1)).compareTo(diffX2),
        yRelation=((Boolean)(diffY1)).compareTo(diffY2);
    if((xRelation==0)^(yRelation==0)){
      if(xRelation==0){
        return yRelation>0?BELOW:ABOVE;
      }else{
        return xRelation>0?RIGHT:LEFT;
      }
    }
    float xMed=(thisX1+thisX2)/2,yMed=(thisY1+thisY2)/2;
    boolean belowSlash=(rtX2-rtX1)*(yMed-rtY1)>(rtY2-rtY1)*(xMed-rtX1),
            belowBackSlash=(rtX2-rtX1)*(yMed-rtY2)>(rtY1-rtY2)*(xMed-rtX1);
    if(belowSlash){
      return belowBackSlash?BELOW:LEFT;
    }else{
      return belowBackSlash?RIGHT:ABOVE;
    }
  }

  public float getBasisX() {
    return p1.getX();
  }

  public float getBasisY() {
    return p1.getY();
  }
}
