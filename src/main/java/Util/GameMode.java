package Util;

// GameのModeの一覧のenum
public enum GameMode {
  PLAY_MODE(0),
  EDIT_MODE(1),
  GAME_SELECT_MODE(2);

  private int mode;
  private GameMode(int mode) {
    this.mode = mode;
  }

  public int getMode() {
    return mode;
  }

  public void setMode(int mode) {
    this.mode = mode;
  }
}
