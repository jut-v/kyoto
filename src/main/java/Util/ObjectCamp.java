package Util;

public enum ObjectCamp {
  STAGE(0),
  PLAYER(1),
  ENEMY(2);
  private int side;
  private ObjectCamp(int side){
    this.side=side;
  }
  public int getCamp(){
    return side;
  }
}
