package Util;

import java.util.BitSet;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

//弾幕の召喚タイミングを教えてくれるクラス
public class GenerateSupervisor {
  private Expression exp;//数式処理クラス
  private BitSet bSet=new BitSet(FPS);//1秒分の判断を保持するクラス
  private int second=0;//現在保持されている判断が何秒なのかを保持するクラス
  private final String formula;//数式を保持するクラス
  private static final int FPS=(int)Const.FPS;
  public GenerateSupervisor(String formula){
    ExpressionBuilder expBuilder=new ExpressionBuilder(formula);
    exp=expBuilder.variable("T").build();
    this.formula=formula;
  }
  public GenerateSupervisor(GenerateSupervisor gs){
    exp=gs.exp;bSet=new BitSet(FPS);formula=gs.formula;
  }
  //一秒ごとに纏めて判断する
  private void setBitSet(){
    exp.setVariable("T", second);
    int i,index,n=Math.min(Math.max((int)exp.evaluate(),0),FPS),m=FPS-n;
    //nは1秒に召喚する個数であり、数式が[0,30]の値を取らなければ2つの値の内近い方となる。
    boolean swapped=false;
    bSet.clear();
    if(n<m){
      int temp=n;n=m;m=temp;
      swapped=true;
    }
    for(i=index=0;i<m;i++){
      bSet.set(index);
      index+=(n+i)/m+1;
    }
    if(!swapped)bSet.flip(0, FPS);
  }
  //弾幕を放つべきならばtrueを返す
  public boolean judge(int count){
    if(count/FPS!=second){//一秒ごとに纏めて判断する
      second=count/FPS;setBitSet();
    }
    return bSet.get(count%FPS);
  }
  public String getFormula(){
    return formula;
  }
}
