package Util;

public enum MapScrollDirection {
  verticalPlus(0),
  horizontalPlus(1),
  verticalMinus(2),
  horizontalMinus(3);

  public int getMapScrollDirection() {
    return mapScrollDirection;
  }

  public void setMapScrollDirection(int mapScrollDirection) {
    this.mapScrollDirection = mapScrollDirection;
  }

  private int mapScrollDirection;
  MapScrollDirection(int mapScrollDirection) {
    this.mapScrollDirection = mapScrollDirection;
  }

}
