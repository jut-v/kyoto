package Util;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;

//パスにあるwavファイルを再生するクラス
public class MusicPlayer{
  private final String path;//再生するファイルのパス
  private Clip music;//曲データを入れる
  private FloatControl controler;//musicのcontroller
  private float volume=1;//音量を保持する
  private boolean playing=false,staged=false;//それぞれ再生中、音声準備が完了しているならtrue
  public MusicPlayer(String path) throws javax.sound.sampled.UnsupportedAudioFileException,IOException,LineUnavailableException{
    this.path=path;
    stage();terminate();
  }
  private void stage() throws javax.sound.sampled.UnsupportedAudioFileException,IOException,LineUnavailableException{
    if(staged)return;
    AudioInputStream in;
    in=AudioSystem.getAudioInputStream(new File(path));
    AudioFormat base=in.getFormat();
    AudioFormat target=new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, base.getSampleRate(),
          16, base.getChannels(), base.getChannels()*2, base.getSampleRate(), false);
    AudioInputStream data=AudioSystem.getAudioInputStream(target, in);
    music=AudioSystem.getClip();
    music.open(data);
    controler=(FloatControl)music.getControl(FloatControl.Type.MASTER_GAIN);
    controler.setValue((float)Math.log10(volume) * 20);
    data.close();
    in.close();
    music.addLineListener(new LineListener() {
      public void update(LineEvent myLineEvent) {
        if(!music.isRunning()){
          playing=staged=false;music.close();
        }
      }
    });
  }
  private void clipPlay(int n) throws javax.sound.sampled.UnsupportedAudioFileException,IOException,LineUnavailableException{
    stage();
    if(playing){
      music.stop();music.flush();music.setFramePosition(0);
    }else{
      playing=true;
    }
    music.loop(n);
  }
  //n回再生する
  public void play(int n) throws javax.sound.sampled.UnsupportedAudioFileException,IOException,LineUnavailableException{
    if(n==0)return;
    if(n>0)n--;
    clipPlay(n);
  }
  public void loop() throws javax.sound.sampled.UnsupportedAudioFileException,IOException,LineUnavailableException{
    clipPlay(Clip.LOOP_CONTINUOUSLY);
  }
  public void stop(){
    music.stop();playing=false;
  }
  public void terminate(){
    playing=staged=false;music.close();
  }
  public void setVolume(float ratio){
    volume=ratio;
    controler.setValue((float)Math.log10(ratio) * 20);
  }
  public float getVolume(){
    return volume;
  }
  public String getPath(){
    return path;
  }
}
