package Util;

public enum ReactionMode{
  NEUTRAL(0),
  VANISH(1),
  REFLECT(2),
  PHANTOM(3);
  private int mode;
  private ReactionMode(int mode){
    this.mode=mode;
  }
  public int getMode(){
    return mode;
  }
}
