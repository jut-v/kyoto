import java.util.*;
import java.awt.*;
import java.awt.event.*;
import Util.*;
import gameUI.*;
import gameUI.button.BasicButton;
import gameUI.button.ButtonSupportable;
import gameUI.dropDownMenu.*;
import gameUI.style.*;
import gameUI.text.UITextWithBackground;

public class NavigationView implements Clickable, DropDownMenuViewSupportable, ButtonSupportable {
  private EditView ev; //このViewを呼び出したView
  private Rect r; //ナビゲーションメニューの描画範囲

  private DropDownMenuView mapMenuView; //マップ一覧を表示するView
  private ArrayList<BasicButton> buttonList; //表示するボタンのリスト
  private BasicButton createNewMapButton; //マップを作成するボタン
  private BasicButton mapSettingsButton; //マップ設定を表示するボタン
  private BasicButton gameSettingsButton; //ゲーム設定を表示するボタン
  private BasicButton saveButton; //ゲームを保存するボタン
  private BasicButton saveAndQuitButton; //ゲームを保存して終了するためのボタン
  
  public NavigationView (EditView ev, Rect r) {
    this.ev = ev;
    this.r = r;
    generateMapMenu();
    buttonList = new ArrayList<>();
    this.createNewMapButton = new BasicButton(this, new Rect(new Point2(220, 10), 90, 30), "マップ追加");
    buttonList.add(createNewMapButton);
    this.mapSettingsButton = new BasicButton(this, new Rect(new Point2(320, 10), 90, 30), "マップ設定");
    buttonList.add(mapSettingsButton);
    this.gameSettingsButton = new BasicButton(this, new Rect(new Point2(420, 10), 90, 30), "ゲーム設定");
    buttonList.add(gameSettingsButton);
    this.saveAndQuitButton = new BasicButton(this, new Rect(new Point2(1160, 10), 110, 30), "保存して終了");
    buttonList.add(saveAndQuitButton);
    this.saveButton = new BasicButton(this, new Rect(new Point2(1105, 10), 45, 30), "保存");
    buttonList.add(saveButton);
  }

  //マップのメニューを生成
  public void generateMapMenu() {
    ArrayList<MapModel> mapModelList = this.ev.getMapModelList();
    ArrayList<DropDownMenuItemContent> mapList = new ArrayList<DropDownMenuItemContent>();
    for (MapModel mm : mapModelList) {
      mapList.add(new DropDownMenuItemContent(mm.getName()));
    }
    mapMenuView = new DropDownMenuView(this, ev.getCurrentMapModel().getName(), mapList, new Rect(new Point2(10, 10), 200, 30));
  }

  //描画
  public void draw(Graphics g) {
    mapMenuView.draw(g);
    for (BasicButton b: buttonList) {
      b.draw(g);
    }
    new UITextWithBackground(
      new Rect(new Point2(520, 10), 100, 30),
      "(" + ev.getCameraPosition().getX() + ", " + ev.getCameraPosition().getY() + ")",
      StyleFont.defaultFont
    ).draw(g);
  }

  @Override
  public Rect getArea() {
    if(mapMenuView.getState()) return new Rect(r.getP1(), r.getWidth(), mapMenuView.getArea().getHeight());
    return new Rect(r);
  }

  @Override
  public boolean clicked(Point2 p, MouseEvent e) {
    boolean result;
    if(mapMenuView.getArea().hasPoint2(p)) {
      result = mapMenuView.clicked(p, e);
      if(result) return true;
    }
    for (BasicButton b: buttonList) {
      if(b.getArea().hasPoint2(p)) {
        result = b.clicked(p, e);
        if(result) return true;
      }
    }
    return false;
  }

  @Override
  public void menuSelected() {
    ev.updateView();
  }

  @Override
  public void menuItemSelected(DropDownMenuItemContent content) {
    ev.changeCurrentMap(content.getText());
    this.generateMapMenu();
    //Map読み込みで再描画がかかるため、updateViewの必要はない
  }

  @Override
  public void buttonSelected(BasicButton b) {
    if(b == createNewMapButton) ev.createNewMap();
    if(b == mapSettingsButton) ev.showMapSettings();
    if(b == gameSettingsButton) ev.showGameSettings();
    if(b == saveAndQuitButton) ev.saveAndQuit();
    if(b == saveButton) ev.save();
  }
}
