import java.awt.*;

import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;

import Util.*;
import Util.io.KImageIO;
import gameUI.*;
import gameUI.contextMenu.*;
import gameUI.style.StyleColor;
import settingsUI.SErrorDialog;

public class EditMapView extends MapView implements Clickable, Draggable, WheelAcceptable, ContextMenuViewSupportable {
  private EditView ev;
  private static final int SCROLL_LENGTH = 32;

  // ドラッグ関係
  private Point2 draggedStartedPosition; //ドラッグされ始めた位置
  private String draggedComponentType; //現在行われているドラッグの種類

  //ドラッグの種類
  private static final String DRAGGED_COMPONENT_TYPE_MAP = "draggedComponentTypeMap"; //マップをドラッグ中
  private static final String DRAGGED_COMPONENT_TYPE_OBJECT = "draggedComponentTypeObject"; //物体をドラッグ中

  private boolean isCreatingNewObject; //新しい物体が作成されているか
  private Rect newObjectArea; //その時点で作成される物体の範囲
  private ObstacleView draggedObstacleView; //ドラッグされているObstacleView

  //コンテキストメニュー関係
  private ContextMenuView contextMenuView; //コンテキストメニューを表示するクラス
  private ObstacleView contextMenuCallerObject; //コンテキストメニューを呼び出したクラス (存在しない場合はnull)
  private String contextMenuType; //現在表示中のメニューの種類

  //コンテキストメニューの種類
  private static final String NEW_OBJECT = "newObject"; //新しい物体を作成するときのメニュー
  private static final String OBJECT = "object"; //物体を編集するときのメニュー

  //コンテキストメニューの要素のテキスト
  private static final String NEW_OBJECT_CREATE_OBJECT = "オブジェクトの作成";
  private static final String NEW_OBJECT_CREATE_COLLIDER = "コライダーの作成";
  private static final String OBJECT_SETTINGS = "詳細設定";
  private static final String OBJECT_CLONE = "オブジェクトを複製";
  private static final String OBJECT_DELETE = "オブジェクトを削除";

  private static final int CLONE_SLIDE_X = 50; //複製された物体が、元の物体からずれるX座標の大きさ

  public EditMapView(EditView ev, MapModel mm, Rect r) throws IOException {
    super(mm, r, true);
    this.ev = ev;
    if(mm.getPlayableCharacterModel() == null) {
      mm.setPlayableCharacterModel(new Rect());
      mm.getPlayableCharacterModel().setImage(KImageIO.imageRead(Kyoto.class.getResourceAsStream("/playable.png")));
    }
  }

  @Override
  protected ObstacleView addObstacleModel(ObstacleModel om) {
    ObstacleView ov = super.addObstacleModel(om);
    ov.setEditMapView(this);
    return ov;
  }

  public void draw(Graphics g) {
    super.draw(g);
    if(newObjectArea != null) {
      g.setColor(StyleColor.objectGeneratedColor);
      g.drawRect((int)newObjectArea.getBasisX(), (int)newObjectArea.getBasisY(), (int)newObjectArea.getWidth(), (int)newObjectArea.getHeight());
    }
    if(contextMenuView != null) contextMenuView.draw(g);
  }

  // オブジェクトを作成したときのコンテキストメニューを表示
  public void generateNewObjectMenu(Point2 p) {
    contextMenuType = NEW_OBJECT;
    ArrayList<ContextMenuItemContent> contentList = new ArrayList<>();
    contentList.add(new ContextMenuItemContent(NEW_OBJECT_CREATE_OBJECT));
    contentList.add(new ContextMenuItemContent(NEW_OBJECT_CREATE_COLLIDER));
    contextMenuView = new ContextMenuView(this, contentList, new Point2(p), 160);
  }

  //オブジェクトから呼び出されたコンテキストメニューを表示
  public void generateObjectMenu(ObstacleView caller, Point2 p) {
    this.contextMenuCallerObject = caller;
    contextMenuType = OBJECT;
    ArrayList<ContextMenuItemContent> contentList = new ArrayList<>();
    contentList.add(new ContextMenuItemContent(OBJECT_SETTINGS));
    contentList.add(new ContextMenuItemContent(OBJECT_CLONE));
    contentList.add(new ContextMenuItemContent(OBJECT_DELETE));
    contextMenuView = new ContextMenuView(this, contentList, new Point2(p), 160);
  }

  //コンテキストメニューを閉じる
  public void closeContextMenu(ContextMenuView view) {
    if(contextMenuType == NEW_OBJECT) {
      newObjectArea = null;
    }
    contextMenuType = null;
    contextMenuView = null;
    ev.updateView();
  }

  @Override
  public void contextMenuItemSelected(ContextMenuItemContent content) {
    switch (contextMenuType) {
      case NEW_OBJECT:
        switch (content.getText()) {
          case NEW_OBJECT_CREATE_OBJECT: createNewObstacle(true); break;
          case NEW_OBJECT_CREATE_COLLIDER: createNewObstacle(false); break;
        }
        break;
      case OBJECT:
        switch (content.getText()) {
          case OBJECT_SETTINGS: showObstacleSettings(contextMenuCallerObject.getModel()); break;
          case OBJECT_CLONE: cloneObstacle(contextMenuCallerObject.getModel()); break;
          case OBJECT_DELETE: deleteObstacle(contextMenuCallerObject.getModel()); break;
        }
        break;
    }
  }

  //新しいオブジェクトを作成
  private void createNewObstacle(boolean hasImage) {
    if (hasImage) {
      try {
        DataBufferedImage backgroundImage = KImageIO.imageRead(Kyoto.class.getResourceAsStream("/object.png"));
        mm.addObstacleModelFromRelativeAxis(newObjectArea, backgroundImage);
      } catch (Exception e) {
        new SErrorDialog(e);
      }
    } else {
      mm.addObstacleModelFromRelativeAxis(newObjectArea);
    }
    newObjectArea = null;
  }

  //引数で受け取ったオブジェクトの設定を表示
  private void showObstacleSettings(ObstacleModel om) {
    new ObstacleSettingsFrameView(ev, om, mm);
  }

  //引数で受け取ったオブジェクトを複製
  private void cloneObstacle(ObstacleModel om) {
    try {
      mm.addObstacleModel(new ObstacleModel(om, om.getBody().getMedian().addX(CLONE_SLIDE_X)));
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  //引数で受け取ったオブジェクトを削除
  private void deleteObstacle(ObstacleModel om) {
    mm.removeObjectModel(om);
  }

  // Controllerから呼び出されるメソッド

  @Override
  public boolean clicked(Point2 p, MouseEvent e) {
    boolean result;
    if(contextMenuView != null) {
      if (contextMenuView.getArea().hasPoint2(p)) {
        contextMenuView.clicked(p, e);
      } else {
        closeContextMenu(contextMenuView);
      }
      return true;
    } else {
      for(ObstacleView view: obstacleViewList) {
        if(view.existPoint2OnVisibleArea(p)) {
          result = view.clicked(p, e);
          if(result) return true;
        }
      }
    }
    return false;
  }

  @Override
  public void draggedStarted(Point2 p, MouseEvent e) {
    // draggedStartedPosition = null;
    if(contextMenuView == null || !contextMenuView.getArea().hasPoint2(p)) {
      for (ObstacleView ov : obstacleViewList) {
        if(ov.existPoint2OnVisibleArea(p)) {
          ov.draggedStarted(p, e);
          draggedComponentType = DRAGGED_COMPONENT_TYPE_OBJECT;
          draggedObstacleView = ov;
        }
      }
      if(draggedComponentType == null) {
        draggedStartedPosition = p;
        draggedComponentType = DRAGGED_COMPONENT_TYPE_MAP;
      }
    }
  }

  @Override
  public void draggedChanged(Point2 p, MouseEvent e) {
    if(draggedComponentType == DRAGGED_COMPONENT_TYPE_OBJECT) {
      draggedObstacleView.draggedChanged(p, e);
    }
    if(draggedComponentType == DRAGGED_COMPONENT_TYPE_MAP) {
      if(!isCreatingNewObject) isCreatingNewObject = true;
      newObjectArea = new Rect(new Point2(draggedStartedPosition), new Point2(p));
    }
    ev.updateView();
  }

  @Override
  public void draggedEnded(Point2 p, MouseEvent e) {
    if(draggedComponentType == DRAGGED_COMPONENT_TYPE_OBJECT) {
      draggedObstacleView.draggedEnded(p, e);
      draggedObstacleView = null;
    }
    if(draggedComponentType == DRAGGED_COMPONENT_TYPE_MAP) {
      if(isCreatingNewObject) {
        isCreatingNewObject = false;
        generateNewObjectMenu(p);
      }
    }
    draggedComponentType = null;
    ev.updateView();
  }

  @Override
  public void wheelMoved(Point2 p, MouseWheelEvent e) {
    if(e.isShiftDown()) {
      if (e.getWheelRotation() > 0) mm.cameraMove(SCROLL_LENGTH, 0);
      if (e.getWheelRotation() < 0) mm.cameraMove(-SCROLL_LENGTH, 0);
    } else {
      if (e.getWheelRotation() > 0) mm.cameraMove(0, SCROLL_LENGTH);
      if (e.getWheelRotation() < 0) mm.cameraMove(0, -SCROLL_LENGTH);
    }
    ev.updateView();
  }
}
