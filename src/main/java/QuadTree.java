import Util.Rect;

import java.util.ArrayList;

// 4分木のモデル
public class QuadTree {
  private final int width;
  private final int height;
  private final int level;
  private final double unitWidth;
  private final double unitHeight;
  private final ArrayList<ArrayList<ObstacleModel>> quadTree = new ArrayList<>();
  public final static int[] addNum = {0, 1, 5, 21, 85, 341};

  QuadTree(int width, int height, int level) {
    this.width = width;
    this.height = height;
    this.level = level;
    for (int i = 0; i < addNum[level + 1]; i++) {
      quadTree.add(new ArrayList<>());
    }
    this.unitWidth = width / (double) (1 << this.level);
    this.unitHeight = height / (double) (1 << this.level);
  }

  public void clear() {
    for (int i = 0; i < addNum[this.level + 1]; i++) {
      quadTree.get(i).clear();
    }
  }

  // ObstacleModelを追加する
  public void addObstacleModel(ObstacleModel object, Rect body) {
    float leftBottomXPoint = body.getP1().getX();
    float leftBottomYPoint = body.getP1().getY();
    float rightUpXPoint = body.getP2().getX();
    float rightUpYPoint = body.getP2().getY();
    if (leftBottomXPoint <= 0 || leftBottomYPoint <= 0 || rightUpXPoint >= this.width || rightUpYPoint >= this.height) {
      quadTree.get(0).add(object);
      return;
    }
    short leftBottomX = (short) (leftBottomXPoint / this.unitWidth);
    short leftBottomY = (short) (leftBottomYPoint / this.unitHeight);
    short rightUpX = (short) (rightUpXPoint / this.unitWidth);
    short rightUpY = (short) (rightUpYPoint / this.unitHeight);
    int leftBottomMorton = getMortonNumber(leftBottomX, leftBottomY);
    int rightUpMorton = getMortonNumber(rightUpX, rightUpY);
    if (leftBottomMorton == rightUpMorton) {
      quadTree.get(addNum[level] + leftBottomMorton).add(object);
      return;
    }
    int shiftNum = getShiftNum(leftBottomMorton, rightUpMorton);
    int objLevel = this.level - (shiftNum / 2);
    int morton = (rightUpMorton >> shiftNum) & 0x00000003;
    quadTree.get(addNum[objLevel] + morton).add(object);
  }

  public ArrayList<ArrayList<ObstacleModel>> getQuadTree() {
    return quadTree;
  }

  private int getShiftNum(int morton1, int morton2) {
    int xor = morton1 ^ morton2;
    int num = 0;
    for (int i = 0; i < 32; i+=2) {
      if (((xor >> i) & 0x00000003) != 0) {
        num = i + 2;
      }
    }
    return num;
  }

  private int separate(int n) {
    n = (n | (n << 8)) & 0x00ff00ff;
    n = (n | (n << 4)) & 0x0f0f0f0f;
    n = (n | (n << 2)) & 0x33333333;
    n = (n | (n << 1)) & 0x55555555;
    return n;
  }

  private int getMortonNumber(short x, short y) {
    return (separate(x) | separate(y) << 1);
  }

  // 衝突判定をする
  public void checkCollision(int index , ArrayList<ObstacleModel> checkCollidedModels) {
    ArrayList<ObstacleModel> checkObstacleModels = quadTree.get(index);
    int length = checkObstacleModels.size();
    for (int i = 0; i < length; i++) {
      for (int j = 0; j < i - 1; j++) {
        ObstacleModel obstacleModel1 = checkObstacleModels.get(i);
        ObstacleModel obstacleModel2 = checkObstacleModels.get(j);
        if (obstacleModel1.getBody().existOn(obstacleModel2.getBody())) {
          obstacleModel1.reaction(obstacleModel2);
          obstacleModel2.reaction(obstacleModel1);
        }
      }
    }
    int collidedLength = checkCollidedModels.size();
    for (int i = 0; i < length; i++) {
      for (int j = 0; j < collidedLength; j++) {
        ObstacleModel obstacleModel1 = checkObstacleModels.get(i);
        ObstacleModel obstacleModel2 = checkCollidedModels.get(j);
        if (obstacleModel1.getBody().existOn(obstacleModel2.getBody())) {
          obstacleModel1.reaction(obstacleModel2);
          obstacleModel2.reaction(obstacleModel1);
        }
      }
    }

    checkCollidedModels.addAll(checkObstacleModels);
    if (quadTree.size() > index * 4 + 2) {
      for (int i = 0; i < 4; i++) {
        int nextIndex = index * 4 + 1 + i;
        checkCollision(nextIndex, checkCollidedModels);
      }
    }
  }
}
