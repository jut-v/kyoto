import java.io.IOException;

import Util.Rect;

public class ReincarnationServantSettingsView extends ObjectServantSettingsView {
  private ObstacleModel parent; //この障害物を次の形態の召喚物とする障害物
  public ReincarnationServantSettingsView(ObstacleModel parent, MapModel map) {
    super(parent.getReincarnation() != null ? parent.getReincarnation() : new ObstacleModel(new Rect(), map), map);
    this.parent = parent;
  }

  //設定を反映
  public void setValues() throws IOException {
    if(parent.getReincarnation() != null) {
      model = parent.getReincarnation();
      super.setValues();
    }
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    fp.setEnabled(b);
  }
}
