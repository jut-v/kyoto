import Util.*;
import Util.io.KImageIO;
import settingsUI.SErrorDialog;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

// Mapを管理するModel
public class MapModel implements PropertyChangeSupportable {
  private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  private final ArrayList<ObstacleModel> obstacleModels = new ArrayList<>();
  private String name;
  private DataBufferedImage background;
  private final GameMasterModel gameMasterModel;
  private PlayableCharacterModel playableCharacterModel;
  private CameraModel cameraModel;
  private MapScrollDirection mapScrollDirection = MapScrollDirection.horizontalPlus;
  private float mapScrollValue = 1.0f;
  private MusicPlayer musicPlayer;
  private int width;
  private int height;
  private ScheduledExecutorService scheduledExecutorService;
  private ScheduledFuture<?> scheduledFuture;
  private QuadTree quadTree;
  private boolean isCameraMoveFinished = false;
  private final Set<ObstacleModel> activeObstacleModels = new HashSet<>();
  private final ArrayList<ObstacleModel> playingObstacleModels = new ArrayList<>();
  private final List<ObstacleModel> expectedRemoveObstacleModels = new ArrayList<>();
  private final List<ObstacleModel> expectedAddObstacleModels = new ArrayList<>();

  public MapModel(String name, Image background, GameMasterModel gameMasterModel) throws IOException, InterruptedException {
    this.name = name;
    this.background = KImageIO.convertDataBufferedImage(background);
    this.gameMasterModel = gameMasterModel;
    this.width = this.background.getWidth();
    this.height = this.background.getHeight();
    cameraModel = new CameraModel(new Rect(new Point2(0, 0), new Point2(Const.windowWidth, Const.windowHeight)));
  }

  public MapModel(String name, String backgroundPath, GameMasterModel gameMasterModel) throws IOException, InterruptedException {
    this.name = name;
    this.background = KImageIO.imageRead(backgroundPath);
    this.gameMasterModel = gameMasterModel;
    this.width = this.background.getWidth();
    this.height = this.background.getHeight();
    cameraModel = new CameraModel(new Rect(new Point2(0, 0), new Point2(Const.windowWidth, Const.windowHeight)));
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
    this.propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
    this.propertyChangeSupport.removePropertyChangeListener(propertyChangeListener);
  }

  public ArrayList<ObstacleModel> getObstacleModels() {
    return obstacleModels;
  }

  public void addObstacleModel(ObstacleModel obstacleModel) {
    obstacleModels.add(obstacleModel);
    this.propertyChangeSupport.firePropertyChange(PropertyName.ADDED_OBSTACLE_MODEL, null, obstacleModel);
  }

  public void addObstacleModel(Rect rect) {
    ObstacleModel obstacleModel = new ObstacleModel(rect, this);
    obstacleModels.add(obstacleModel);
    this.propertyChangeSupport.firePropertyChange(PropertyName.ADDED_OBSTACLE_MODEL, null, obstacleModel);
  }

  public void removeObjectModel(ObjectModel removeObjectModel) {
    obstacleModels.remove((ObstacleModel) removeObjectModel);
    this.propertyChangeSupport.firePropertyChange(PropertyName.REMOVED_OBSTACLE_MODEL, null, removeObjectModel);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) throws InvalidNameException {
    if (this.gameMasterModel.isValidMapName(name)) {
      this.name = name;
    } else {
      throw new InvalidNameException(name + "はすでに使用されています。");
    }
  }

  public DataBufferedImage getBackground() {
    return background;
  }

  public void setBackground(Image background) throws InterruptedException {
    DataBufferedImage oldBackground = this.background;
    this.background = KImageIO.convertDataBufferedImage(background);
    this.width = this.background.getWidth();
    this.height = this.background.getHeight();
    this.propertyChangeSupport.firePropertyChange(PropertyName.CHANGE_MAP_BACKGROUND, oldBackground, this.background);
  }

  public void setBackground(String backgroundFilePath) throws IOException, InterruptedException {
    DataBufferedImage oldBackground = this.background;
    setBackground(KImageIO.imageRead(backgroundFilePath));
    this.propertyChangeSupport.firePropertyChange(PropertyName.CHANGE_MAP_BACKGROUND, oldBackground, this.background);
  }

  public void setPlayableCharacterModel(Rect rect) {
    this.playableCharacterModel = new PlayableCharacterModel(rect, this);
    this.propertyChangeSupport.firePropertyChange(PropertyName.SET_PLAYABLE_CHARACTER_MODEL, null, this.playableCharacterModel);
  }

  public PlayableCharacterModel getPlayableCharacterModel() {
    return this.playableCharacterModel;
  }

  public void setPlayableCharacterModel(PlayableCharacterModel playableCharacterModel) {
    this.playableCharacterModel = playableCharacterModel;
  }

  public CameraModel getCameraModel() {
    return cameraModel;
  }

  public void setCameraModel(CameraModel cameraModel) {
    this.cameraModel = cameraModel;
  }

  public MapScrollDirection getMapScrollDirection() {
    return mapScrollDirection;
  }

  public void setMapScrollDirection(MapScrollDirection mapScrollDirection) {
    this.mapScrollDirection = mapScrollDirection;
  }

  public float getMapScrollValue() {
    return mapScrollValue;
  }

  public void setMusic(String path) throws javax.sound.sampled.UnsupportedAudioFileException,IOException,LineUnavailableException {
    try {
      musicPlayer = new MusicPlayer(path);
    } catch (Exception e) {
      musicPlayer = null;
      throw e;
    }
  }

  public String getMusicPath() {
    if (musicPlayer != null) {
      return musicPlayer.getPath();
    } else {
      return null;
    }
  }

  public void setMapScrollValue(float mapScrollValue) {
    if (mapScrollValue < 0) {
      mapScrollValue = -mapScrollValue;
    }
    this.mapScrollValue = mapScrollValue;
  }

  public Set<ObstacleModel> getActiveObstacleModels() {
    return activeObstacleModels;
  }

  public ArrayList<ObstacleModel> getPlayingObstacleModels() {
    return playingObstacleModels;
  }

  // カメラからの相対座標でObstacleModelを追加する
  public void addObstacleModelFromRelativeAxis(Rect rect, DataBufferedImage backgroundImage) throws IOException {
    ObstacleModel obstacleModel = new ObstacleModel(new Rect(), this);
    obstacleModel.setBodyWithCamera(rect);
    // ここでsetImageによりfigBodyも相対座標で生成される。
    obstacleModel.setImage(backgroundImage);
    obstacleModels.add(obstacleModel);
    this.propertyChangeSupport.firePropertyChange(PropertyName.ADDED_OBSTACLE_MODEL, null, obstacleModel);
  }

  public void addObstacleModelFromRelativeAxis(Rect rect) {
    ObstacleModel obstacleModel = new ObstacleModel(new Rect(), this);
    obstacleModel.setBodyWithCamera(rect);
    obstacleModels.add(obstacleModel);
    this.propertyChangeSupport.firePropertyChange(PropertyName.ADDED_OBSTACLE_MODEL, null, obstacleModel);
  }

  // ゲームを開始する
  public void gameStarted() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
    prepareGame();
    gameLoop();
  }

  // ゲームを止める
  public void gameStop() {
    if (musicPlayer != null) {
      musicPlayer.terminate();
    }
    playingObstacleModels.clear();
    activeObstacleModels.clear();
    this.playableCharacterModel.restoreBackup();
    scheduledExecutorService.shutdown();
  }

  private void gameStop(Exception e) {
    gameStop();
    this.gameMasterModel.gameError(e);
  }

  // ゲームを一時停止する
  public void gamePause() {
    if (musicPlayer != null) {
      musicPlayer.stop();
    }
    scheduledFuture.cancel(false);
  }

  // ゲームを終了する
  public void gameOver() {
    gameStop();
    this.gameMasterModel.gameOver();
  }

  // 次のマップに進む
  public void nextMap() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
    gameStop();
    this.gameMasterModel.nextMap(this);
  }

  public void setIsCameraMoveFinished(boolean cameraMoveFinished) {
    this.isCameraMoveFinished = cameraMoveFinished;
  }

  public void addActiveObstacleModel(ObstacleModel obstacleModel) {
    expectedAddObstacleModels.add(obstacleModel);
  }

  public void removeActiveObjectModel(ObstacleModel obstacleModel) {
    expectedRemoveObstacleModels.add(obstacleModel);
  }

  // カメラを動かす
  public void cameraMove(float dx, float dy) {
    Rect oldCamPos = new Rect(cameraModel.getCamPos());
    cameraModel.getCamPos().move(dx, dy);
    this.propertyChangeSupport.firePropertyChange(PropertyName.MOVED_CAMERA_MODEL, oldCamPos, cameraModel.getCamPos());
  }

  // ゲームを開始する準備をする
  private void prepareGame() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
    if (playingObstacleModels.isEmpty()) {
      playableCharacterModel.doBackup();
      obstacleModels.forEach(om -> {
        ObstacleModel copyObstacleModel = new ObstacleModel(om);
        playingObstacleModels.add(copyObstacleModel);
        if (playableCharacterModel.getTeleportKey() == om) {
          playableCharacterModel.setTeleportKey(copyObstacleModel);
        }
      });
    }
    if (musicPlayer != null) {
      musicPlayer.play(Clip.LOOP_CONTINUOUSLY);
    }
    cameraModel.setFollowMove(playableCharacterModel, new Rect(new Point2(0, 0), new Point2(this.width, this.height)), false, false, mapScrollDirection);
    quadTree = new QuadTree((int) cameraModel.getCamPos().getWidth(), (int) cameraModel.getCamPos().getHeight(), 3);
    scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
  }

  // ゲームを開始したときに1/30ごとに繰り返す内容
  private void gameLoop() {
    scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(() -> {
      expectedRemoveObstacleModels.clear();
      expectedAddObstacleModels.clear();
      activeCheck();
      moveModels();
      playableCharacterModel.summonServant();
      try {
        checkCollisionModels();
      } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
        e.printStackTrace();
        gameStop(e);
      }
      applyActiveObstacleModel();
      this.propertyChangeSupport.firePropertyChange(PropertyName.UPDATED_OBJECTS, null, activeObstacleModels);
    }, 0, (long) (1000 / Const.FPS), TimeUnit.MILLISECONDS);
  }

  // カメラ内にオブジェクトが存在するか調べる
  private void activeCheck() {
    playingObstacleModels.forEach(o -> {
      if (o.live() && o.getBody().existOn(this.cameraModel.getCamPos())) {
        if (!activeObstacleModels.contains(o)) {
          activeObstacleModels.add(o);
          this.propertyChangeSupport.firePropertyChange(PropertyName.ADDED_ACTIVE_OBSTACLE_MODEL, null, o);
        }
      }
    });
  }

  // Objectを動かす
  private void moveModels() {
    if (isCameraMoveFinished) {
      mapScrollValue = 0;
    }
    playableCharacterModel.mapScroll(mapScrollValue, mapScrollDirection);
    activeObstacleModels.forEach(ObstacleModel::move);
    cameraModel.move();
  }

  // 衝突判定をする
  private void checkCollisionModels() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
    quadTree.clear();
    activeObstacleModels.forEach(o -> {
      quadTree.addObstacleModel(o, o.getBodyFromCamera());
    });
    quadTree.checkCollision(0, new ArrayList<>());
    for (ObstacleModel o : activeObstacleModels) {
      if (o.getBody().existOn(playableCharacterModel.getBody())) {
        o.reaction(playableCharacterModel);
        playableCharacterModel.reaction(o);
      }
    }
  }

  // Objectから依頼されていた保留している処理を実行する
  private void applyActiveObstacleModel() {
    expectedRemoveObstacleModels.forEach(o -> {
      activeObstacleModels.remove(o);
      this.propertyChangeSupport.firePropertyChange(PropertyName.REMOVED_ACTIVE_OBSTACLE_MODEL, null, o);
    });
    expectedAddObstacleModels.forEach(o -> {
      if (!activeObstacleModels.contains(o)) {
        activeObstacleModels.add(o);
        this.propertyChangeSupport.firePropertyChange(PropertyName.ADDED_ACTIVE_OBSTACLE_MODEL, null, o);
      }
    });
  }
}
