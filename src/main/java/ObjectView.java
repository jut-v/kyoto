import java.awt.*;

import Util.*;
import gameUI.style.StyleColor;

public class ObjectView {
  private ObjectModel model; //このViewで表示する物体
  private boolean isColliderVisible; //コライダーを表示するか

  public ObjectView(ObjectModel model, boolean isColliderVisible) {
    this.model = model;
    this.isColliderVisible = isColliderVisible;
  }

  //画像が設定されている場合は画像の範囲、設定されていない場合はコライダーの範囲を返す
  public Rect getArea() {
    return model.getImage() != null ? model.getFigBodyFromCamera() : model.getBodyFromCamera();
  }

  //物体の範囲内に引数で与えられた点が入っているかを返す
  public boolean existPoint2OnVisibleArea(Point2 p) {
    if(model.getImage() == null) {
      return model.getBodyFromCamera().hasPoint2(p);
    }
    return model.getBodyFromCamera().hasPoint2(p) || model.getFigBodyFromCamera().hasPoint2(p);
  }

  //この物体を描画
  public void draw(Graphics g) {
    Image img = model.getImage();
    Rect r = model.getBodyFromCamera();
    if(img != null) {
      Rect imgArea = model.getFigBodyFromCamera();
      g.drawImage(img, (int)imgArea.getBasisX(), (int)imgArea.getBasisY(), (int)imgArea.getWidth(), (int)imgArea.getHeight(), null);
    }
    g.setColor(StyleColor.colliderColor);
    if(isColliderVisible) g.drawRect((int)r.getBasisX(), (int)r.getBasisY(), (int)r.getWidth(), (int)r.getHeight());
  }

  //このViewで表示しているモデルを返す
  public ObjectModel getModel() {
    return this.model;
  }
}
