import java.awt.*;
import java.io.IOException;

import javax.swing.JPanel;

import Util.Rect;
import settingsUI.object.*;

public class ObstacleReincarnationSettingsView extends JPanel {
  ObstacleModel model; //この障害物を次の形態とする障害物
  MapModel map; //この障害物が設定されているマップ
  SFieldsPanel fp; //設定項目を表示するパネル
  ObjectPreviewPanelView previewPanel; //この障害物のプレビューをするパネル
  boolean isTeleportable; //この障害物にテレポートキーが設定されているか

  public ObstacleReincarnationSettingsView(ObstacleModel model, MapModel map) {
    this.model = model;
    this.map = map;
    isTeleportable = model == map.getPlayableCharacterModel().getTeleportKey();
    ObstacleModel rm = model.getReincarnation() != null ? model.getReincarnation() : new ObstacleModel(new Rect(), map);
    fp = new SFieldsPanel();
    fp.setEnabledCheckBox(model.getReincarnation() != null, "次の形態", "次の形態を有効にする");
    fp.setImageSelector(rm.getImage());
    fp.setBodySize(rm.getBody());
    fp.setHitPointField(rm.getHitPoint());
    fp.setDamageField(rm.getCollisionDamage());
    fp.setCampSelector(rm.getObjectCamp());
    fp.setReactionSelector(rm.getReactionMode());
    fp.setBodyBehaviorFields(rm.getBodyBehavior());
    fp.setTeleportableCheckBox(isTeleportable);
    fp.setFieldsEnabledByCheckBox();
    previewPanel = new ObjectPreviewPanelView(rm);
    this.setLayout(new GridLayout(1, 2));
    this.add(fp);
    this.add(previewPanel);
  }

  //設定を反映
  public void setValues() throws IOException {
    if(fp.isCheckBoxEnabled()) {
      if(model.getReincarnation() == null) model.setReincarnation(new ObstacleModel(fp.getBodyRect(), map));
      if(fp.isImagePathChanged()) {
        model.getReincarnation().setImage(fp.getImagePath());
        previewPanel.setImageFromPathString(fp.getImagePath());
      }
      model.getReincarnation().setBodyWithCamera(fp.getBodyRect());
      previewPanel.setBody(fp.getBodyRect());
      model.getReincarnation().setHitPoint(fp.getHitPoint());
      model.getReincarnation().setCollisionDamage(fp.getDamage());
      model.getReincarnation().setObjectCamp(fp.getCamp());
      model.getReincarnation().setReactionMode(fp.getReaction());
      model.getReincarnation().setBodyBehavior(fp.getBodyBehavior());
      if (fp.getTeleportable()) {
        map.getPlayableCharacterModel().setTeleportKey(model.getReincarnation());
      } else if (isTeleportable && !fp.getTeleportable()) {
        map.getPlayableCharacterModel().setTeleportKey(null);
        isTeleportable = false;
      }
      previewPanel.updateView();
    } else {
      model.setReincarnation(null);
    }
  }
}
