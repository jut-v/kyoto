import java.awt.*;

import Util.*;
import gameUI.style.*;
import gameUI.text.UITextWithBackground;

public class PlayStatusView {
    PlayableCharacterModel pm; //プレイヤーが動かしているキャラクター

    public PlayStatusView(PlayableCharacterModel pm) {
        this.pm = pm;
    }

    //ステータスを描画
    public void draw(Graphics g) {
        new UITextWithBackground(
            new Rect(new Point2(10, 10), 70, 20),
            "HP: " + (pm.getHitPoint() != Integer.MIN_VALUE ? pm.getHitPoint() : "なし"),
            StyleFont.defaultFont
        ).draw(g);
    }
}
