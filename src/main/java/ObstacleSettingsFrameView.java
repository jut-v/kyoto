import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;

public class ObstacleSettingsFrameView extends JFrame implements ActionListener {
  private EditView ev; //このフレームを呼び出したView
  private ObstacleModel model; //設定対象の障害物
  private ObstacleSettingsView obstaclePanel; //この障害物の設定を行うパネル
  private ObjectServantSettingsView servantPanel; //召喚物の設定を行うパネル
  private ObstacleReincarnationSettingsView reincarnationPanel; //次の形態の設定を行うパネル
  private ReincarnationServantSettingsView reincarnationServantPanel; //次の形態の召喚物を設定するパネル
  private JTabbedPane tabbedPane; //パネルを切り替えるためのタブ
  private JButton saveButton; //保存ボタン

  public ObstacleSettingsFrameView(EditView ev, ObstacleModel model, MapModel map) {
    this.ev = ev;
    this.model = model;
    this.setTitle("オブジェクトの設定");
    this.setSize(640, 480);
    this.setLayout(new BorderLayout());
    obstaclePanel = new ObstacleSettingsView(model, map);
    servantPanel = new ObjectServantSettingsView(model, map);
    reincarnationPanel = new ObstacleReincarnationSettingsView(model, map);
    reincarnationServantPanel = new ReincarnationServantSettingsView(model, map);
    reincarnationServantPanel.setEnabled(false);
    tabbedPane = new JTabbedPane();
    saveButton = new JButton("保存");
    saveButton.addActionListener(this);
    tabbedPane.add("オブジェクト本体", obstaclePanel);
    tabbedPane.add("召喚物", servantPanel);
    tabbedPane.add("次の形態", reincarnationPanel);
    tabbedPane.add("次の形態の召喚物", reincarnationServantPanel);
    reincarnationServantPanel.setEnabled(model.getReincarnation() != null);
    this.add(tabbedPane, BorderLayout.CENTER);
    this.add(saveButton, BorderLayout.SOUTH);
    this.setVisible(true);
  }
  
  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == saveButton) {
      try {
        obstaclePanel.setValues();
        servantPanel.setValues();
        reincarnationPanel.setValues();
        reincarnationServantPanel.setValues();
        reincarnationServantPanel.setEnabled(model.getReincarnation() != null);
        ev.updateView();
      } catch(IOException ex) {
        ex.printStackTrace();
      }
    }
  }
}
