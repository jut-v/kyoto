import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import settingsUI.SErrorDialog;

public class GameSettingsFrameView extends JFrame implements ActionListener {
  GameMasterModel gmm; //このゲームを管理しているモデル
  GameSettingsView gameSettingsView; //ゲームの設定を変更するためのView
  JButton saveButton; //保存ボタン

  public GameSettingsFrameView(GameMasterModel gmm) {
    super();
    this.gmm = gmm;
    this.setSize(300, 200);
    this.setLayout(new BorderLayout());
    try {
      gameSettingsView = new GameSettingsView(gmm);
    } catch (Exception e) {
      new SErrorDialog(e);
    }
    saveButton = new JButton("保存");
    saveButton.addActionListener(this);
    this.add(gameSettingsView, BorderLayout.CENTER);
    this.add(saveButton, BorderLayout.SOUTH);
    this.setVisible(true);
  }
  
  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == saveButton) { //保存ボタンがクリックされたとき
      try {
        gameSettingsView.setValues();
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }
}
