import java.awt.*;
import java.io.*;

import javax.swing.*;

import Util.*;
import Util.io.KImageIO;
import gameUI.style.StyleColor;

public class ObjectPreviewPanelView extends JPanel {
  private ObjectModel model; //プレビューを描画するモデル
  private Image image; //プレビューの画像
  private Rect body, figBody; //コライダーの範囲と、画像の範囲

  public ObjectPreviewPanelView(ObjectModel model) {
    super();
    this.model = model;
    setValuesFromModel();
  }

  //プレビューを描画
  @Override
  public void paintComponent(Graphics g) {
    g.fillRect(0, 0, (int)this.getSize().getWidth(), (int)this.getSize().getHeight());
    Point2 center = new Point2((float)this.getSize().getWidth() / 2, (float)this.getSize().getHeight() / 2);
    if(image != null) {
      g.drawImage(
        image,
        (int)(center.getX() - figBody.getWidth() / 2), (int)(center.getY() - figBody.getHeight() / 2),
        (int)figBody.getWidth(), (int)figBody.getHeight(), null
      );
    }
    g.setColor(StyleColor.colliderColor);
    g.drawRect(
      (int)(center.getX() - body.getWidth() / 2), (int)(center.getY() - body.getHeight() / 2),
      (int)body.getWidth(), (int)body.getHeight()
    );
  }

  //プレビューする画像を設定
  public void setImageFromPathString(String path) throws IOException {
    image = new DataBufferedImage(KImageIO.imageRead(path));
    figBody = new Rect(new Point2(), image.getWidth(null), image.getHeight(null));
  }

  //コライダーの範囲を取得
  public void setBody(Rect r) {
    body = r;
  }

  //プレビューを表示するモデルから、画像と、コライダーと画像の範囲を設定
  public void setValuesFromModel() {
    image = model.getImage();
    if(image != null) figBody = model.getFigBody();
    body = model.getBody();
  }

  //他のクラスからこのクラスを再描画するためのメソッド
  public void updateView() {
    this.repaint();
  }
}
