import java.awt.Graphics;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;

import Util.*;
import gameUI.KeyAcceptable;

public class PlayMapView extends MapView implements KeyAcceptable {
  PlayableCharacterModel cm; //プレイヤーが動かしているキャラクター
  ObjectView playableView; //プレイヤーが動かしているキャラクターを表示するクラス
  PlayView pv; //このクラスを表示しているView
  
  public PlayMapView(MapModel mm, PlayView pv, Rect r) {
    super(mm, r, false);
    this.pv = pv;
    cm = mm.getPlayableCharacterModel();
    playableView = new ObjectView(mm.getPlayableCharacterModel(), true);
  }

  @Override
  protected void addAllObstacleModels() {
    for(ObstacleModel om: mm.getActiveObstacleModels()) {
      addObstacleModel(om);
    }
  }

  @Override
  public void draw(Graphics g) {
    super.draw(g);
    playableView.draw(g);
  }

  @Override
  public void keyInput(KeyEvent e) {}

  @Override
  public void keyEntered(KeyEvent e) {
    if (e.getKeyCode() == Const.KEY_UP) cm.moveUp(true);
    if (e.getKeyCode() == Const.KEY_RIGHT) cm.moveRight(true);
    if (e.getKeyCode() == Const.KEY_DOWN) cm.moveDown(true);
    if (e.getKeyCode() == Const.KEY_LEFT) cm.moveLeft(true);
    if (e.getKeyCode() == Const.PRIMALY_KEY) cm.setSummoning(true);
  }

  @Override
  public void keyLeaved(KeyEvent e) {
    if (e.getKeyCode() == Const.KEY_UP) cm.moveUp(false);
    if (e.getKeyCode() == Const.KEY_RIGHT) cm.moveRight(false);
    if (e.getKeyCode() == Const.KEY_DOWN) cm.moveDown(false);
    if (e.getKeyCode() == Const.KEY_LEFT) cm.moveLeft(false);
    if (e.getKeyCode() == Const.PRIMALY_KEY) cm.setSummoning(false);
  }

  @Override
  public void propertyChange(PropertyChangeEvent e) {
    switch (e.getPropertyName()) {
      case PropertyName.ADDED_ACTIVE_OBSTACLE_MODEL:
        addObstacleModel((ObstacleModel) e.getNewValue());
        break;
      case PropertyName.REMOVED_ACTIVE_OBSTACLE_MODEL:
        removeObstacleModel((ObstacleModel)e.getNewValue());
        break;
      case PropertyName.UPDATED_OBJECTS:
        pv.updateView();
    }
  }
}
