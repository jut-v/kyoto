import java.awt.*;
import java.io.IOException;

import javax.swing.*;

import settingsUI.object.*;

public class MapPlayableSettingsView extends JPanel {
  PlayableCharacterModel model; //設定を行うプレイヤーのモデル
  SFieldsPanel fp; //設定項目を表示するパネル
  ObjectPreviewPanelView previewPanel; //キャラクターのプレビューを行うパネル
  public MapPlayableSettingsView(PlayableCharacterModel model) {
    super();
    this.model = model;
    fp = new SFieldsPanel();
    fp.setImageSelector(model.getImage());
    fp.setBodySize(model.getBody());
    fp.setPositionFields(model.getBody().getMedian());
    fp.setPlayableSensitivityFields(model.getSensitivity());
    fp.setHitPointField(model.getHitPoint());
    previewPanel = new ObjectPreviewPanelView(model);
    this.setLayout(new GridLayout(1, 2));
    this.add(fp);
    this.add(previewPanel);
  }

  //設定をモデルに反映
  public void setValues() throws IOException {
    if(fp.isImagePathChanged()) model.setImage(fp.getImagePath());
    model.setBodyWithCamera(fp.getBodyRect());
    model.moveBodiesTo(fp.getPosition());
    model.setHitPoint(fp.getHitPoint());
    model.setSensitivity(fp.getPlayableSensitivities());
    previewPanel.setValuesFromModel();
    previewPanel.updateView();
  }
}
