import java.awt.event.*;
import Util.*;
import gameUI.*;

public class ObstacleView extends ObjectView implements Clickable, Draggable {
  private ObstacleModel obstacleModel; //この障害物のモデル
  private EditMapView editMapView; //このViewを表示しているView

  //ドラッグの種類
  private String draggedType; //ドラッグの種類を表す変数
  private static String DRAGGED_MOVE = "draggedMove"; //障害物を移動中
  private static String DRAGGED_RESIZE_TOP = "draggedResizeTop"; //障害物の上部の位置を変更中
  private static String DRAGGED_RESIZE_BOTTOM = "draggedResizeBottom"; //障害物の下部の位置を変更中
  private static String DRAGGED_RESIZE_LEFT = "draggedResizeLeft"; //障害物の左側の位置を変更中
  private static String DRAGGED_RESIZE_RIGHT = "draggedResizeRight"; //障害物の右側の位置を変更中

  private Vector2 draggedPositionToMedian; // 位置変更用
  private Point2 draggedStartedPosition; // サイズ変更用
  private Rect draggedStartedFigBody; // サイズ変更用

  private static int EDGE_SIZE = 4; //障害物の端をドラッグしたと判定される幅

  public ObstacleView(ObstacleModel obstacleModel, boolean isColliderVisible) {
    super(obstacleModel, isColliderVisible);
    this.obstacleModel = obstacleModel;
  }

  //この障害物のモデルを返す
  public ObstacleModel getModel() {
    return obstacleModel;
  }

  //引数で与えられた位置からドラッグの種類を判定
  private String getDraggedType(Point2 p) {
    Rect r = obstacleModel.getBodyFromCamera();
    if (obstacleModel.getImage() != null) {
      if (obstacleModel.getFigBodyFromCamera().hasPoint2(p) && !obstacleModel.getBodyFromCamera().hasPoint2(p))
        return DRAGGED_MOVE;
    }
    if (p.getX() <= r.getBasisX() + EDGE_SIZE)
      return DRAGGED_RESIZE_LEFT;
    if (p.getY() <= r.getBasisY() + EDGE_SIZE)
      return DRAGGED_RESIZE_TOP;
    if (p.getX() >= r.getBasisX() + r.getWidth() - EDGE_SIZE)
      return DRAGGED_RESIZE_RIGHT;
    if (p.getY() >= r.getBasisY() + r.getHeight() - EDGE_SIZE)
      return DRAGGED_RESIZE_BOTTOM;
    return DRAGGED_MOVE;
  }

  //この障害物を表示するViewを設定
  public void setEditMapView(EditMapView editMapView) {
    this.editMapView = editMapView;
  }

  @Override
  public boolean clicked(Point2 p, MouseEvent e) {
    if (e.getButton() == MouseEvent.BUTTON3 && editMapView != null) {
      editMapView.generateObjectMenu(this, p);
      return true;
    }
    return false;
  }

  @Override
  public void draggedStarted(Point2 p, MouseEvent e) {
    draggedType = getDraggedType(p);
    if (draggedType == DRAGGED_MOVE) {
      draggedPositionToMedian = new Vector2(p, obstacleModel.getBodyFromCamera().getMedian());
    } else {
      draggedStartedPosition = p;
      draggedStartedFigBody = new Rect(obstacleModel.getBodyFromCamera());
    }
  }

  @Override
  public void draggedChanged(Point2 p, MouseEvent e) {
    if (draggedType == DRAGGED_MOVE) {
      obstacleModel.moveBodiesFromCameraTo(p.add(draggedPositionToMedian));
    } else {
      Point2 p1 = new Point2(draggedStartedFigBody.getP1());
      Point2 p2 = new Point2(draggedStartedFigBody.getP2());
      if (draggedType == DRAGGED_RESIZE_LEFT)
        p1 = p1.addX(p.getX() - draggedStartedPosition.getX());
      if (draggedType == DRAGGED_RESIZE_TOP)
        p1 = p1.addY(p.getY() - draggedStartedPosition.getY());
      if (draggedType == DRAGGED_RESIZE_RIGHT)
        p2 = p2.addX(p.getX() - draggedStartedPosition.getX());
      if (draggedType == DRAGGED_RESIZE_BOTTOM)
        p2 = p2.addY(p.getY() - draggedStartedPosition.getY());
      obstacleModel.setBodyWithCamera(new Rect(p1, p2));
    }
  }

  @Override
  public void draggedEnded(Point2 p, MouseEvent e) {
    if (draggedType == DRAGGED_MOVE) {
      draggedPositionToMedian = null;
    } else {
      draggedStartedPosition = null;
      draggedStartedFigBody = null;
    }
    draggedType = null;
  }
}
