import Util.*;

import java.io.IOException;

//プレイヤー以外のマップ上の物体を担うクラス
public class ObstacleModel extends ObjectModel{
  private Behavior bodyBehavior=new Behavior("0", "0");//自分の動き方を定義する変数とその初期値
  private Point2 basePoint;//Behaviorを使う上での基準点
  private boolean live=true;//自分がゲーム進行中のマップ上に存在しているかを表わす
  private ReactionMode mode=ReactionMode.NEUTRAL;//反応の種類を定義する
  ObstacleModel(Rect rt,MapModel map){
    super(rt, map);basePoint=new Point2(initPoint);
  }
  ObstacleModel(Rect rt,String path,MapModel map) throws IOException{
    super(rt, path, map);basePoint=new Point2(initPoint);
  }
  ObstacleModel(ObstacleModel ob){
    super(ob);copyEssentials(ob);
  }
  //initPointにobと座標以外同じものを生成する。
  ObstacleModel(ObstacleModel ob,Point2 initPoint){
    super(ob, initPoint);copyEssentials(ob);
  }
  private void copyEssentials(ObstacleModel ob){
    this.bodyBehavior=new Behavior(ob.bodyBehavior);
    basePoint=new Point2(initPoint);
    mode=ob.mode;
  }
  public void setBodyBehavior(Behavior behavior){
    this.bodyBehavior=behavior;
  }
  public void setBodyBehavior(String formulaX,String formulaY){
    this.bodyBehavior=new Behavior(formulaX,formulaY);
  }
  public void setBodyBehavior(Vector2 vector,float time){
    this.bodyBehavior=new Behavior(vector, time);
  }
  public Behavior getBodyBehavior(){
    return this.bodyBehavior;
  }
  public boolean live(){
    return live;
  }
  public void setReactionMode(ReactionMode mode){
    this.mode=mode;
  }
  public ReactionMode getReactionMode(){
    return mode;
  }
  private void reflect(ObjectModel o){
    int relation=body.positionalRelation(o.body);
    Point2 wallPoint=(relation==Rect.ABOVE||relation==Rect.LEFT?o.body.getP1():o.body.getP2());
    if(relation==Rect.ABOVE||relation==Rect.BELOW){
      bodyBehavior.mirrorY();
      basePoint.move(0,(wallPoint.getY()-basePoint.getY())*2);
    }else{
      bodyBehavior.mirrorX();
      basePoint.move((wallPoint.getX()-basePoint.getX())*2,0);
    }
  }
  //衝突時の位置調整
  @Override
  protected void adjust(ObjectModel o) {
    Point2 delta=solveAdjust(o);
    moveBodies(delta.getX(),delta.getY());
    basePoint.move(delta.getX(),delta.getY());
  }
  private void vanish(){
    if(live){
      map.removeActiveObjectModel(this);live=false;
      if(reincarnation!=null){
        ObstacleModel reincarnation=new ObstacleModel((ObstacleModel)this.reincarnation,revivalPoint==null?body.getMedian():revivalPoint);
        reincarnation.getBodyBehavior().setSummonerT(count/Const.FPS);
        if(this.reincarnation==map.getPlayableCharacterModel().getTeleportKey()){
          map.getPlayableCharacterModel().setTeleportKey(reincarnation);
        }
        map.addActiveObstacleModel(reincarnation);
      }
    }
  }
  @Override
  public void setHitPoint(int hitPoint) {
    super.setHitPoint(hitPoint);
    mode=(hitPoint!=NON_CHARACTER?ReactionMode.VANISH:ReactionMode.NEUTRAL);
  }
  @Override
  public void damage(ObjectModel o) {
    super.damage(o);
    if(hitPoint==0)vanish();
  }
  //ダメージ処理をした上で、どの反応を取るべきか返す。
  private ReactionMode solveReaction(ObjectModel o){
    damage(o);
    if(camp==o.camp||camp==ObjectCamp.STAGE)
      return ReactionMode.PHANTOM;
    if(o instanceof PlayableCharacterModel)
      return mode==ReactionMode.NEUTRAL?ReactionMode.PHANTOM:mode;//押し出されないようにするため
    ObstacleModel ob=(ObstacleModel)o;
    if(o.camp==ObjectCamp.STAGE)
      return ob.mode==ReactionMode.NEUTRAL?mode:ob.mode;
    return ob.hitPoint==NON_CHARACTER||(camp==ObjectCamp.ENEMY&&hitPoint!=NON_CHARACTER)?ReactionMode.PHANTOM:mode;
  }
  //対象のObjectModelに対して反応をする。
  @Override
  public void reaction(ObjectModel o) {
    ReactionMode outcome=solveReaction(o);
    switch (outcome) {
      case NEUTRAL:
        adjust(o);
        break;
      case VANISH:
        vanish();
        break;
      case REFLECT:
        reflect(o);
        break;
      case PHANTOM:
        break;
    }
  }
  //動いた上で、弾幕も放つ。
  protected void move(){
    if(bodyBehavior!=null){
      moveBodiesTo(bodyBehavior.func(count/Const.FPS).addXY(basePoint.getX(), basePoint.getY()));
    }
    if(!inCamera()){
      vanish();return;
    }
    summonServant();
    count++;
  }
}
