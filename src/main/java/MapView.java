import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.util.*;
import Util.*;
import gameUI.style.StyleColor;

public class MapView implements PropertyChangeListener {
  protected MapModel mm; //このViewで表示するマップのモデル
  protected Rect r; //Viewの表示範囲
  protected ArrayList<ObstacleView> obstacleViewList; //表示する障害物
  private boolean isEditable; //マップが編集可能か

  public MapView(MapModel mm, Rect r, boolean isEditable) {
    this.mm = mm;
    this.r = r;
    this.isEditable = isEditable;
    this.obstacleViewList = new ArrayList<ObstacleView>();
    addAllObstacleModels();
    mm.addPropertyChangeListener(this);
  }

  //mapModelから取得した障害物を追加
  protected void addAllObstacleModels() {
    for(ObstacleModel om: mm.getObstacleModels()) {
      addObstacleModel(om);
    }
  }

  //引数で受け取った障害物を追加
  protected ObstacleView addObstacleModel(ObstacleModel om) {
    ObstacleView ov = new ObstacleView(om, isEditable);
    this.obstacleViewList.add(ov);
    return ov;
  }

  //引数で受け取った障害物を削除
  protected ObstacleView removeObstacleModel(ObstacleModel om) {
    ObstacleView removedObstacleView = null;
    for(ObstacleView ov: this.obstacleViewList) {
      if(ov.getModel() == om) removedObstacleView = ov;
    }
    if(removedObstacleView != null) this.obstacleViewList.remove(removedObstacleView);
    return removedObstacleView;
  }

  public void draw(Graphics g) {
    Rect cameraPos = this.mm.getCameraModel().getCamPos();
    Rect sourceArea = new Rect(
      new Point2(cameraPos.getP1()),
      (int)Math.min(cameraPos.getWidth(), mm.getBackground().getWidth() - cameraPos.getBasisX()),
      (int)Math.min(cameraPos.getHeight(), mm.getBackground().getHeight() - cameraPos.getBasisY())
    );
    g.setColor(StyleColor.background);
    g.fillRect((int)r.getBasisX(), (int)r.getBasisY(), (int)r.getWidth(), (int)r.getHeight());
    g.drawImage(
      this.mm.getBackground(),
      (int)r.getBasisX(), (int)r.getBasisY(),
      (int)(r.getBasisX() + sourceArea.getWidth()), (int)(r.getBasisY() + sourceArea.getHeight()),
      (int)sourceArea.getP1().getX(), (int)sourceArea.getP1().getY(),
      (int)sourceArea.getP2().getX(), (int)sourceArea.getP2().getY(),
      null
    );
    for(ObstacleView ov: obstacleViewList) {
      ov.draw(g);
    }
  }

  //このクラスが描画される範囲を返す
  public Rect getArea() {
    return new Rect(r);
  }

  @Override
  public void propertyChange(PropertyChangeEvent e) {
    switch (e.getPropertyName()) {
      case PropertyName.ADDED_OBSTACLE_MODEL:
        this.addObstacleModel((ObstacleModel) e.getNewValue());
        break;
      case PropertyName.REMOVED_OBSTACLE_MODEL:
        this.removeObstacleModel((ObstacleModel) e.getNewValue());
        break;
    }
  }
}
