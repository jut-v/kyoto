import javax.swing.*;

import Util.*;
import gameUI.KeyAcceptable;
import gameUI.UIController;
import gameUI.message.UIMessage;
import settingsUI.SErrorDialog;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

public class PlayView extends JPanel implements PropertyChangeListener, ActionListener, KeyAcceptable {
  private GameMasterModel gmm; //現在のゲームを管理するモデル
  private UIController c; //このViewで用いるController
  private PlayStatusView statusView; //プレイヤーのHPを表示するView
  private PlayMapView mapView; //現在プレイ中のマップを表示するView

  //ゲームの実行状況を保持するためのフィールド
  private String gameStatus;
  private static final String PREPARED = "prepared";
  private static final String PROCESSING = "processing";
  private static final String PAUSED = "paused";
  private static final String FINISHED = "finished";
  private static final String OVERED = "overed";

  private UIMessage information; //メッセージ画面を表示するためのクラス

  public PlayView(GameMasterModel gmm) {
    this.gmm = gmm;
    c = new UIController();
    this.setFocusable(true);
    this.addMouseListener(c);
    this.addMouseMotionListener(c);
    this.addMouseWheelListener(c);
    this.addKeyListener(c);
    c.addKeyAcceptable(this);
    gmm.addPropertyChangeListener(this);
    showPreparedMessage();
    gameStatus = PREPARED;
  }

  public void actionPerformed(ActionEvent e) {
    repaint();
  }

  public void changeMapView() {
    mapView = new PlayMapView(gmm.getCurrentMapModel(), this, new Rect(new Point2(), Const.windowWidth, Const.windowHeight));
    statusView = new PlayStatusView(gmm.getCurrentMapModel().getPlayableCharacterModel());
  }

  public void paintComponent(Graphics g) {
    if(information != null) {
      information.draw(g);
    } else {
      mapView.draw(g);
      statusView.draw(g);
    }
  }

  public void updateView() {
    repaint();
  }

  public void startGame() {
    System.out.println("STARTED");
    try {
      information = null;
      gmm.gameStart();
      changeMapView();
      gameStatus = PROCESSING;
    } catch(Exception e) {
      new SErrorDialog(e);
    }
  }

  public void pauseGame() {
    System.out.println("PAUSED");
    showPausedMessage();
    gmm.getCurrentMapModel().gamePause();
    gameStatus = PAUSED;
  }

  public void restartGame() {
    System.out.println("RESTARTED");
    try {
      information = null;
      gmm.gameRestart();
      changeMapView();
      gameStatus = PROCESSING;
    } catch(Exception e) {
      new SErrorDialog(e);
    }
  }

  public void quitGame() {
    System.out.println("QUIT");
    gmm.setMode(GameMode.GAME_SELECT_MODE);
  }

  public void showPreparedMessage() {
    try {
      ArrayList<String> textList = new ArrayList<>();
      textList.add("ゲームを開始するには " + Const.PRIMALY_KEY_NAME + " を押してください。");
      textList.add(Const.QUIT_KEY_NAME + " で一覧画面へ戻ります。");
      information = new UIMessage("ゲームを開始する準備ができました。", gmm.getGameThumbnail(), textList);
      repaint();
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  public void showPausedMessage() {
    try {
      ArrayList<String> textList = new ArrayList<>();
      textList.add(Const.PRIMALY_KEY_NAME + " でゲームを再開します。");
      textList.add(Const.QUIT_KEY_NAME + " でゲームを終了し、一覧画面へ戻ります。");
      information = new UIMessage("ゲームを一時停止しました。", gmm.getGameThumbnail(), textList);
      repaint();
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  public void showFinishedMessage() {
    gameStatus = FINISHED;
    try {
      ArrayList<String> textList = new ArrayList<>();
      textList.add(Const.PRIMALY_KEY_NAME + " でゲーム選択画面に戻ります。");
      information = new UIMessage("ゲームが終了しました。", gmm.getGameThumbnail(), textList);
      repaint();
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  public void showOveredMessage() {
    gameStatus = OVERED;
    try {
      System.out.println("GAME OVERED");
      ArrayList<String> textList = new ArrayList<>();
      textList.add(Const.PRIMALY_KEY_NAME + " でゲームをリスタートします。");
      textList.add(Const.QUIT_KEY_NAME + " でゲーム選択画面に戻ります。");
      information = new UIMessage("ゲームオーバーしました。", gmm.getGameThumbnail(), textList);
      repaint();
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  @Override
  public void keyInput(KeyEvent e) {
    if(e.getKeyCode() == Const.PRIMALY_KEY) {
      if(gameStatus == PREPARED) startGame();
      else if(gameStatus == PAUSED) startGame();
      else if(gameStatus == FINISHED) quitGame();
      else if(gameStatus == OVERED) restartGame();
    } else if(e.getKeyCode() == Const.QUIT_KEY) {
      if(gameStatus == PREPARED) quitGame();
      else if(gameStatus == PROCESSING) pauseGame();
      else if(gameStatus == PAUSED) quitGame();
      else if(gameStatus == OVERED) quitGame();
    }
  }

  @Override
  public void keyEntered(KeyEvent e) {
    if(mapView != null) mapView.keyEntered(e);
  }

  @Override
  public void keyLeaved(KeyEvent e) {
    if(mapView != null) mapView.keyLeaved(e);
  }

  @Override
  public void propertyChange(PropertyChangeEvent e) {
    switch (e.getPropertyName()) {
      case PropertyName.MAP_CHANGED:
        changeMapView();
        break;
      case PropertyName.UPDATED_OBJECTS:
        repaint();
        break;
      case PropertyName.GAME_FINISHED:
        showFinishedMessage();
        break;
      case PropertyName.GAME_OVERED:
        showOveredMessage();
        break;
    }
  }
}
