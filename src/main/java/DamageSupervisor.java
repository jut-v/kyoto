import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;

import Util.Const;
import Util.PropertyName;

//1秒以内に対象オブジェクトからダメージを受けたことがあるか教えてくれるクラス
public class DamageSupervisor implements PropertyChangeListener{
  private HashMap<ObjectModel,Integer> hashMap=new HashMap<ObjectModel,Integer>();//最後に対象オブジェクトにぶつかった時間を保存する
  private static int damageCycle=(int)Const.FPS;//ダメージを与えられる周期。既定で1秒
  public static void setCoolTime(float coolTime){
    coolTime=Math.round(Const.FPS*coolTime);
  }
  public static float getCoolTime(){
    return damageCycle/Const.FPS;
  }
  public boolean judge(ObjectModel o,int count){
    Integer lastCount=hashMap.get(o);
    boolean ret=((lastCount==null?0:lastCount)+damageCycle<=count);
    if(ret)hashMap.put(o, count);
    return ret;
  }
  //マップ上から消去された物体は二度とぶつかってくることはないのでHashMapから追い出す。
  @Override
  public void propertyChange(PropertyChangeEvent e) {
    if(e.getPropertyName()==PropertyName.REMOVED_ACTIVE_OBSTACLE_MODEL){
      hashMap.remove((ObjectModel)e.getNewValue());
    }
  }
}
