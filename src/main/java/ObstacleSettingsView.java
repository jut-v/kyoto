import java.io.IOException;

import javax.swing.*;

import settingsUI.object.*;

public class ObstacleSettingsView extends JPanel {
  MapModel mm; //この障害物が設置されているマップ
  ObstacleModel model; //設定を反映するモデル
  SFieldsPanel fp; //設定を行うパネル
  boolean isTeleportable; //この障害物にテレポートキーが設定されているか

  public ObstacleSettingsView(ObstacleModel model, MapModel mm) {
    this.mm = mm;
    this.model = model;
    isTeleportable = model == mm.getPlayableCharacterModel().getTeleportKey();
    //フィールド作成
    fp = new SFieldsPanel();
    fp.setImageSelector(model.getImage());
    fp.setHitPointField(model.getHitPoint());
    fp.setDamageField(model.getCollisionDamage());
    fp.setCampSelector(model.getObjectCamp());
    fp.setReactionSelector(model.getReactionMode());
    fp.setBodyBehaviorFields(model.getBodyBehavior());
    fp.setTeleportableCheckBox(isTeleportable);
    this.add(fp);
  }

  //設定内容を反映
  public void setValues() throws IOException {
    if(fp.isImagePathChanged()) {
      model.setImage(fp.getImagePath());
    }
    model.setHitPoint(fp.getHitPoint());
    model.setCollisionDamage(fp.getDamage());
    model.setObjectCamp(fp.getCamp());
    model.setReactionMode(fp.getReaction());
    model.setBodyBehavior(fp.getBodyBehavior());
    if(fp.getTeleportable()) {
      mm.getPlayableCharacterModel().setTeleportKey(model);
    } else if(isTeleportable && !fp.getTeleportable()) {
      mm.getPlayableCharacterModel().setTeleportKey(null);
      isTeleportable = false;
    }
  }
}
