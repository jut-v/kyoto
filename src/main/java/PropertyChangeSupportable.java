import java.beans.PropertyChangeListener;

public interface PropertyChangeSupportable {
  public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener);
  public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener);
}
