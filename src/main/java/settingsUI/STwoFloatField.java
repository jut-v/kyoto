package settingsUI;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class STwoFloatField extends JPanel implements KeyListener {
  private SFloatField field1, field2; //表示するテキストフィールド
  private ArrayList<SFieldListener> sFieldListenerList;
  public STwoFloatField(String labelText1, String labelText2, float defaultValue1, float defaultValue2) {
    super();
    this.setLayout(new GridLayout(1, 4));
    field1 = new SFloatField(defaultValue1);
    field2 = new SFloatField(defaultValue2);
    this.add(new Label(labelText1));
    this.add(field1);
    this.add(new Label(labelText2));
    this.add(field2);
    sFieldListenerList = new ArrayList<>();
  }

  public void addSFieldListener(SFieldListener listener) {
    sFieldListenerList.add(listener);
  }

  private void notifyToListener() {
    for(SFieldListener listener: sFieldListenerList) {
      listener.sFieldInput();
    }
  }

  public float getValue1() {
    return field1.getValue();
  }

  public float getValue2() {
    return field2.getValue();
  }

  public void keyTyped(KeyEvent e) {
    notifyToListener();
  }

  public void keyPressed(KeyEvent e) {}

  public void keyReleased(KeyEvent e) {}

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    field1.setEnabled(b);
    field2.setEnabled(b);
  }
}
