package settingsUI.object;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import settingsUI.SIntField;

public class SHitPoint extends JPanel implements ActionListener {
  private JCheckBox toggleButton;
  private SIntField field;

  public SHitPoint(int defaultValue) {
    this.setLayout(new BorderLayout());
    toggleButton = new JCheckBox("HPを有効にする", defaultValue != Integer.MIN_VALUE);
    toggleButton.addActionListener(this);
    field = new SIntField(defaultValue);
    if(defaultValue == Integer.MIN_VALUE) field.setEnabled(false);
    this.add(toggleButton, BorderLayout.WEST);
    this.add(field, BorderLayout.CENTER);
  }

  //HPを返す
  public int getHitPoint() {
    if(!toggleButton.isSelected()) return Integer.MIN_VALUE;
    return field.getValue();
  }

  public void actionPerformed(ActionEvent e) {
    if(toggleButton.isSelected()) {
      field.setEnabled(true);
      field.setText("1");
    } else {
      field.setEnabled(false);
    }
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    toggleButton.setEnabled(b);
    if(b && toggleButton.isSelected()) {
      field.setEnabled(true);
    } else {
      field.setEnabled(false);
    }
  }
}
