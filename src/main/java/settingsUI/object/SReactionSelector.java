package settingsUI.object;

import java.util.*;
import java.awt.*;
import javax.swing.*;

import Util.*;
import settingsUI.*;

public class SReactionSelector extends JPanel {
  private SRadioButtons buttons; //反応のボタンを表示するクラス
  private String NEUTRAL = "通常";
  private String VANISH = "消滅";
  private String REFLECT = "反射";
  private String PHANTOM = "なし";
  public SReactionSelector(ReactionMode defaultReaction) {
    ArrayList<String> list = new ArrayList<>();
    list.add(NEUTRAL);
    list.add(VANISH);
    list.add(REFLECT);
    list.add(PHANTOM);
    String defaultText = NEUTRAL;
    if(defaultReaction == ReactionMode.NEUTRAL) defaultText = NEUTRAL;
    if(defaultReaction == ReactionMode.VANISH) defaultText = VANISH;
    if(defaultReaction == ReactionMode.REFLECT) defaultText = REFLECT;
    if(defaultReaction == ReactionMode.PHANTOM) defaultText = PHANTOM;
    buttons = new SRadioButtons(list, defaultText);
    this.setLayout(new BorderLayout());
    this.add(buttons);
  }

  //反応を返す
  public ReactionMode getReaction() {
    String text = buttons.getText();
    if(text == NEUTRAL) return ReactionMode.NEUTRAL;
    if(text == VANISH) return ReactionMode.VANISH;
    if(text == REFLECT) return ReactionMode.REFLECT;
    if(text == PHANTOM) return ReactionMode.PHANTOM;
    return null;
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    buttons.setEnabled(b);
  }
}
