package settingsUI.object;

import java.awt.*;
import javax.swing.*;

import Util.Behavior;
import settingsUI.SLabeledField;

public class SBehavior extends JPanel{
  private SLabeledField xField, yField; //x方向とy方向の動き方を入力するフィールド
  public SBehavior(String defaultXBehavior, String defaultYBehavior) {
    this.setLayout(new BorderLayout());
    xField = new SLabeledField("x(t) = ", defaultXBehavior);
    yField = new SLabeledField("y(t) = ", defaultYBehavior);
    this.add(xField, BorderLayout.CENTER);
    this.add(yField, BorderLayout.SOUTH);
  }

  public SBehavior(Behavior behavior) {
    this(behavior.getFormulaX(), behavior.getFormulaY());
  }

  public SBehavior() {
    this("", "");
  }

  //x方向の動き方を返す
  public String getXBehavior() {
    return xField.getText();
  }

  //y方向の動き方を返す
  public String getYBehavior() {
    return yField.getText();
  }

  //動き方をBehaviorクラスで返す
  public Behavior getBehavior() {
    return new Behavior(getXBehavior(), getYBehavior());
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    xField.setEnabled(b);
    yField.setEnabled(b);
  }
}
