package settingsUI.object;

import Util.GenerateSupervisor;
import settingsUI.SLabeledField;

public class SGenerateSupervisor extends SLabeledField {
  public SGenerateSupervisor(GenerateSupervisor gs) {
    super("f(T)=", gs.getFormula());
  }

  public GenerateSupervisor getGenerateSupervisor() {
    return new GenerateSupervisor(this.getText());
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
  }
}
