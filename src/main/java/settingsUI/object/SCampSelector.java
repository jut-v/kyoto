package settingsUI.object;

import java.util.*;
import java.awt.*;
import javax.swing.*;

import Util.ObjectCamp;
import settingsUI.*;

public class SCampSelector extends JPanel {
  private SRadioButtons campButtons; //陣営の複数のボタンを表示するクラス
  private String STAGE = "Stage";
  private String PLAYER = "Player";
  private String ENEMY = "Enemy";
  public SCampSelector(ObjectCamp defaultCamp) {
    ArrayList<String> campTextList;
    campTextList = new ArrayList<>();
    campTextList.add(STAGE);
    campTextList.add(PLAYER);
    campTextList.add(ENEMY);
    String defaultText = STAGE;
    if(defaultCamp == ObjectCamp.STAGE) defaultText = STAGE;
    if(defaultCamp == ObjectCamp.PLAYER) defaultText = PLAYER;
    if(defaultCamp == ObjectCamp.ENEMY) defaultText = ENEMY;
    campButtons = new SRadioButtons(campTextList, defaultText);
    this.setLayout(new BorderLayout());
    this.add(campButtons, BorderLayout.CENTER);
  }

  //陣営を返す
  public ObjectCamp getCamp() {
    String buttonText = campButtons.getText();
    if(buttonText == STAGE) return ObjectCamp.STAGE;
    if(buttonText == PLAYER) return ObjectCamp.PLAYER;
    if(buttonText == ENEMY) return ObjectCamp.ENEMY;
    return null;
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    campButtons.setEnabled(b);
  }
}
