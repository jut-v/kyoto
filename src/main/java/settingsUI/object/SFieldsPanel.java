package settingsUI.object;

import java.awt.event.*;
import javax.swing.*;

import Util.Behavior;
import Util.DataBufferedImage;
import Util.GenerateSupervisor;
import Util.ObjectCamp;
import Util.Point2;
import Util.ReactionMode;
import Util.Rect;
import Util.Vector2;
import settingsUI.SFloatField;
import settingsUI.SFileSelector;
import settingsUI.SIntField;
import settingsUI.SLayoutGenerator;
import settingsUI.STwoFloatField;

public class SFieldsPanel extends JPanel implements ActionListener {
  private SLayoutGenerator lg; //レイアウト管理
  private JCheckBox enabledCheckBox, teleportCheckBox; //このパネルを有効にするチェックボックス、テレポートキーのチェックボックス
  private JTextField nameField; //名前を設定するためのフィールド
  private SFileSelector imageSelector, musicSelector; //画像と音楽を設定するためのボタン
  private SHitPoint hitPointField; //HPを設定するフィールド
  private SIntField damageField; //与えるダメージを設定するフィールド
  private STwoFloatField bodySizeField, positionFields, playableSensitivityFields; //大きさ、位置、プレイヤーの移動速度の設定フィールド
  private SCampSelector campSelector; //陣営を選択するボタン
  private SReactionSelector reactionSelector; //反応を選択するボタン
  private SBehavior bodyBehavior, generateBehavior; //動き方、生成方法
  private SGenerateSupervisor generateSupervisor; //生成頻度
  private SFloatField mapScrollVauleField; //マップの自動スクロール速度
  private String modelName; //名前が変更されたかを確認するために、名前を代入しておくフィールド

  public SFieldsPanel() {
    lg = new SLayoutGenerator(this, 4);
  }

  public void setEnabledCheckBox(boolean defaultBoolean, String labelText, String checkBoxtext) {
    enabledCheckBox = new JCheckBox(checkBoxtext, defaultBoolean);
    enabledCheckBox.addActionListener(this);
    lg.addLabel(labelText);
    lg.add(enabledCheckBox);
  }

  public void setModelNameFields(String name, String labelText) {
    this.modelName = name;
    nameField = new JTextField(name);
    lg.addLabel(labelText);
    lg.add(nameField);
  }

  public String getModelName() {
    return nameField.getText();
  }

  public boolean isModelNameChanged() {
    return !modelName.equals(getModelName());
  }

  public boolean isCheckBoxEnabled() {
    return enabledCheckBox.isSelected();
  }

  public void setImageSelector(DataBufferedImage image, String labelText, String buttonText) {
    String path = (image != null && image.getFilePath() != null) ? image.getFilePath().toString() : "";
    imageSelector = new SFileSelector(buttonText, path, SFileSelector.IMAGE);
    lg.addLabel(labelText);
    lg.add(imageSelector);
  }

  public void setImageSelector(DataBufferedImage image) {
    setImageSelector(image, "画像", "画像を設定");
  }

  public String getImagePath() {
    return imageSelector.getPath();
  }

  public boolean isImagePathChanged() {
    return !getImagePath().equals("");
  }

  public void setHitPointField(int hp) {
    hitPointField = new SHitPoint(hp);
    lg.addLabel("HP");
    lg.add(hitPointField);
  }

  public int getHitPoint() {
    return hitPointField.getHitPoint();
  }

  public void setDamageField(int damage) {
    damageField = new SIntField(damage);
    lg.addLabel("与えるダメージ");
    lg.add(damageField);
  }

  public int getDamage() {
    return damageField.getValue();
  }

  public void setBodySize(Rect r) {
    bodySizeField = new STwoFloatField("幅", "高さ", r.getWidth(), r.getHeight());
    lg.addLabel("大きさ");
    lg.add(bodySizeField);
  }

  public Rect getBodyRect() {
    return new Rect(new Point2(), bodySizeField.getValue1(), bodySizeField.getValue2());
  }

  public void setCampSelector(ObjectCamp camp) {
    campSelector = new SCampSelector(camp);
    lg.addLabel("陣営");
    lg.add(campSelector);
  }

  public ObjectCamp getCamp() {
    return campSelector.getCamp();
  }

  public void setReactionSelector(ReactionMode mode) {
    reactionSelector = new SReactionSelector(mode);
    lg.addLabel("反応");
    lg.add(reactionSelector);
  }

  public ReactionMode getReaction() {
    return reactionSelector.getReaction();
  }

  public void setBodyBehaviorFields(Behavior behavior) {
    bodyBehavior = new SBehavior(behavior);
    lg.addLabel("動き方");
    lg.add(bodyBehavior);
  }

  public Behavior getBodyBehavior() {
    return bodyBehavior.getBehavior();
  }

  public void setGenerateBehaviorFields(Behavior behavior) {
    generateBehavior = new SBehavior(behavior);
    lg.addLabel("生成位置");
    lg.add(generateBehavior);
  }

  public Behavior getGenerateBehavior() {
    return generateBehavior.getBehavior();
  }

  public void setGenerateSupervisorField(GenerateSupervisor supervisor) {
    generateSupervisor = new SGenerateSupervisor(supervisor);
    lg.addLabel("生成頻度 (個/s)");
    lg.add(generateSupervisor);
  }

  public GenerateSupervisor getGenerateSupervisor() {
    return generateSupervisor.getGenerateSupervisor();
  }

  public void setTeleportableCheckBox(boolean isTeleportable) {
    teleportCheckBox = new JCheckBox("テレポートキーを設定する", isTeleportable);
    lg.addLabel("テレポート");
    lg.add(teleportCheckBox);
  }

  public boolean getTeleportable() {
    return teleportCheckBox.isSelected();
  }

  public void setPlayableSensitivityFields(Vector2 v) {
    playableSensitivityFields = new STwoFloatField("x方向", "y方向", v.getX(), v.getY());
    lg.addLabel("移動速度");
    lg.add(playableSensitivityFields);
  }

  public Vector2 getPlayableSensitivities() {
    return new Vector2(playableSensitivityFields.getValue1(), playableSensitivityFields.getValue2());
  }

  public void setPositionFields(Point2 p) {
    positionFields = new STwoFloatField("x", "y", p.getX(), p.getY());
    lg.addLabel("初期位置");
    lg.add(positionFields);
  }

  public Point2 getPosition() {
    return new Point2(positionFields.getValue1(), positionFields.getValue2());
  }

  public void setMapScrollValueField(float mapScrollValue) {
    mapScrollVauleField = new SFloatField(mapScrollValue);
    lg.addLabel("スクロール速度");
    lg.add(mapScrollVauleField);
  }

  public float getMapScrollValue() {
    return mapScrollVauleField.getValue();
  }

  public void setMusicSelector(String path, String labelText, String buttonText) {
    if (path == null) {
      path = "";
    }
    musicSelector = new SFileSelector(buttonText, path, SFileSelector.MUSIC);
    lg.addLabel(labelText);
    lg.add(musicSelector);
  }

  public String getMusicPath() {
    return musicSelector.getPath();
  }

  public boolean isMusicPathChanged() {
    return !getMusicPath().equals("");
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    if(enabledCheckBox != null) {
      enabledCheckBox.setEnabled(b);
      if(b && enabledCheckBox.isSelected()) {
        setFieldsEnabled(true);
      } else {
        setFieldsEnabled(false);
      }
    } else {
      setFieldsEnabled(b);
    }
  }

  public void setFieldsEnabled(boolean b) {
    if(teleportCheckBox != null) teleportCheckBox.setEnabled(b);
    if(nameField != null) nameField.setEnabled(b);
    if(imageSelector != null) imageSelector.setEnabled(b);
    if(hitPointField != null) hitPointField.setEnabled(b);
    if(damageField != null) damageField.setEnabled(b);
    if(bodySizeField != null) bodySizeField.setEnabled(b);
    if(positionFields != null) positionFields.setEnabled(b);
    if(playableSensitivityFields != null) playableSensitivityFields.setEnabled(b);
    if(campSelector != null) campSelector.setEnabled(b);
    if(reactionSelector != null) reactionSelector.setEnabled(b);
    if(bodyBehavior != null) bodyBehavior.setEnabled(b);
    if(generateBehavior != null) generateBehavior.setEnabled(b);
    if(generateSupervisor != null) generateSupervisor.setEnabled(b);
    if(mapScrollVauleField != null) mapScrollVauleField.setEnabled(b);
  }

  public void setFieldsEnabledByCheckBox() {
    setFieldsEnabled(isCheckBoxEnabled());
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == enabledCheckBox) {
      setFieldsEnabledByCheckBox();
    }
  }
}
