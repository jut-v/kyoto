package settingsUI;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SRadioButtons extends JPanel implements ActionListener {
  private ArrayList<SRadioButtonsSupportable> supportableList;
  private ButtonGroup group; //選択肢を管理するButtonGroup
  private ArrayList<JRadioButton> radioButtonList; //選択肢
  public SRadioButtons(ArrayList<String> buttonTextList, String defaultText) {
    this.setLayout(new GridLayout(1, buttonTextList.size()));
    group = new ButtonGroup();
    radioButtonList = new ArrayList<>();
    for (String text : buttonTextList) {
      JRadioButton button = new JRadioButton(text, text.equals(defaultText));
      this.add(button);
      group.add(button);
      radioButtonList.add(button);
      button.addActionListener(this);
    }
    supportableList = new ArrayList<>();
  }

  public void addSuportable(SRadioButtonsSupportable supportable) {
    supportableList.add(supportable);
  }

  public void removeSupportable(SRadioButtonsSupportable supportable) {
    supportableList.remove(supportable);
  }

  public String getText() {
    for (JRadioButton button : radioButtonList) {
      if(button.isSelected()) return button.getText();
    }
    return null;
  }

  public void actionPerformed(ActionEvent e) {
    for (SRadioButtonsSupportable supportable: supportableList) {
      supportable.buttonsSelected();
    }
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    for (JRadioButton button : radioButtonList) {
      button.setEnabled(b);
    }
  }
}
