package settingsUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//エラーウィンドウを表示
public class SErrorDialog extends JDialog implements ActionListener {
  public SErrorDialog(Exception e) {
    super((JFrame)null, e.getMessage(), true);
    this.setLayout(new BorderLayout());
    this.add(new JLabel(e.getMessage()), BorderLayout.CENTER);
    JButton button = new JButton("OK");
    button.addActionListener(this);
    this.add(button, BorderLayout.SOUTH);
    this.setSize(300, 150);
    this.setVisible(true);
  }

  public void actionPerformed(ActionEvent e) {
    this.setVisible(false);
  }
}
