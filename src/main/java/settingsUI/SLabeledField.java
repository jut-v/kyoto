package settingsUI;

import java.awt.*;
import javax.swing.*;

public class SLabeledField extends JPanel {
  private JLabel label; //テキストフィールドにつけるラベル
  private JTextField field; //テキストフィールド
  public SLabeledField(String labelText, String defaultText) {
    this.setLayout(new BorderLayout());
    label = new JLabel(labelText);
    field = new JTextField(defaultText, 10);
    this.add(label, BorderLayout.WEST);
    this.add(field, BorderLayout.CENTER);
  }

  public SLabeledField(String labelText) {
    this(labelText, "");
  }

  public String getText() {
    return field.getText();
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    field.setEnabled(b);
  }
}
