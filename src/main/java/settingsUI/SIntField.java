package settingsUI;

import javax.swing.*;

public class SIntField extends JTextField {
  public SIntField(int defaultValue) {
    super(Integer.toString(defaultValue), 4);
  }

  //値をint型で返し、テキストフィールドも正しい値になるように変更
  public int getValue() {
    int value = Integer.parseInt(this.getText());
    this.setText(Integer.toString(value));
    return value;
  }
}