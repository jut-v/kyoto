package settingsUI;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.filechooser.*;

//ファイルの選択画面を表示
public class SFileSelector extends JPanel implements ActionListener {
  private String imagePath;
  private JButton button;
  private ArrayList<SButtonListener> sButtonListenerList;
  private FileNameExtensionFilter filter;
  public static final FileNameExtensionFilter IMAGE = new FileNameExtensionFilter("画像", "png", "jpg");
  public static final FileNameExtensionFilter MUSIC = new FileNameExtensionFilter("音声", "wav");

  public SFileSelector(String buttonText, String defaultPath, FileNameExtensionFilter filter) {
    imagePath = defaultPath;
    this.filter = filter;
    button = new JButton(buttonText);
    button.addActionListener(this);
    this.setLayout(new BorderLayout());
    this.add(button, BorderLayout.CENTER);
    sButtonListenerList = new ArrayList<>();
  }

  public void addSButtonListener(SButtonListener listener) {
    sButtonListenerList.add(listener);
  }

  private void notifyToListener() {
    for (SButtonListener listener : sButtonListenerList) {
      listener.sButtonClicked();
    }
  }

  public void actionPerformed(ActionEvent e) {
    //第8回講義資料のFAQを参考に作成
    JFileChooser chooser = new JFileChooser();
    if(filter != null) chooser.setFileFilter(filter);
    int result = chooser.showOpenDialog(null);
    if(result == JFileChooser.APPROVE_OPTION) {
      imagePath = chooser.getSelectedFile().getPath();
      notifyToListener();
    }
  }

  public void setDefaultPath(String defaultPath) {
    imagePath = defaultPath;
    notifyToListener();
  }

  public String getPath() {
    return imagePath;
  }

  @Override
  public void setEnabled(boolean b) {
    super.setEnabled(b);
    button.setEnabled(b);
  }
}
