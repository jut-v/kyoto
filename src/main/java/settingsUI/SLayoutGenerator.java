package settingsUI;

import java.awt.*;
import javax.swing.*;

public class SLayoutGenerator {
  private JPanel panel; //レイアウトを設定するパネル
  private GridBagConstraints labelConst; //ラベルの表示設定
  private GridBagConstraints fieldConst; //設定項目の表示設定
  private static int insetsValue = 5;
  public SLayoutGenerator(JPanel panel, double fieldWeight) {
    this.panel = panel;
    panel.setLayout(new GridBagLayout());
    labelConst = new GridBagConstraints();
    labelConst.weightx = 1;
    labelConst.fill = GridBagConstraints.HORIZONTAL;
    labelConst.insets = new Insets(insetsValue, insetsValue, insetsValue, 0);
    fieldConst = new GridBagConstraints();
    fieldConst.weightx = 3;
    fieldConst.fill = GridBagConstraints.HORIZONTAL;
    fieldConst.gridwidth = GridBagConstraints.REMAINDER;
    fieldConst.insets = new Insets(insetsValue, 0, insetsValue, insetsValue);
  }

  //ラベルを追加
  public void addLabel(String labelText) {
    this.panel.add(new JLabel(labelText), labelConst.clone());
  }

  //パネルを項目として追加
  public void add(JPanel panel) {
    this.panel.add(panel, fieldConst.clone());
  }
  
  //テキストフィールドを項目として追加
  public void add(JTextField field) {
    this.panel.add(field, fieldConst.clone());
  }

  //チェックボックスを項目として追加
  public void add(JCheckBox checkBox) {
    this.panel.add(checkBox, fieldConst.clone());
  }
}
