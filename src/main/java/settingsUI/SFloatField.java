package settingsUI;

import javax.swing.JTextField;

public class SFloatField extends JTextField {
  public SFloatField(float defaultValue) {
    super(Float.toString(defaultValue), 4);
  }

  //値をfloat型で返し、テキストフィールドの文字列を正しい値に変更
  public float getValue() {
    float value = Float.parseFloat(this.getText());
    this.setText(Float.toString(value));
    return value;
  }
}
