import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;

import Util.*;
import gameUI.UIController;
import gameUI.button.*;
import gameUI.select.*;
import gameUI.style.*;
import gameUI.text.*;
import settingsUI.SErrorDialog;

public class GameSelectView extends JPanel implements GameInfoListViewSupportable, ButtonSupportable {
  GameMasterModel gmm; //このゲームを管理しているモデル
  UIController c; //このViewで使うController
  UIText uiText;
  BasicButton infoButton, createButton;
  GameInfoListView gameInfoListView;

  public GameSelectView(GameMasterModel gmm) throws IOException, URISyntaxException {
    this.gmm = gmm;
    c = new UIController();
    this.addMouseListener(c);
    this.addMouseWheelListener(c);
    this.setFocusable(true);

    //UIを作成
    this.uiText = new UIText(new Point2(80, 45), "ゲームを選択してください。", StyleFont.headingFont, StyleColor.text);
    this.infoButton = new BasicButton(this, new Rect(new Point2(990, 40), 40, 40), "情報");
    c.addClickable(infoButton);
    this.createButton = new BasicButton(this, new Rect(new Point2(1040, 40), 160, 40), "新しいゲームを作成");
    c.addClickable(createButton);
    this.gameInfoListView = new GameInfoListView(this, new Rect(new Point2(80, 100), 1120, 600), gmm.getGameInfoList(Const.gamePath));
    c.addClickable(gameInfoListView);
    c.addWheelAcceptable(gameInfoListView);
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.setColor(StyleColor.background);
    g.fillRect(0, 0, Const.windowWidth, Const.windowHeight);
    uiText.draw(g);
    gameInfoListView.draw(g);
    infoButton.draw(g);
    createButton.draw(g);
  }

  //他のクラスからViewをアップデートするために用いられる
  public void updateView() {
    repaint();
  }

  @Override
  public void gameInfoSelected(GameInfo gi, GameMode mode) {
    try {
      gmm.openGame(gi, mode);
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  @Override
  public void buttonSelected(BasicButton b) {
    if(b == createButton) {
      try {
        gmm.createNewGame();
      } catch(Exception e) {
        new SErrorDialog(e);
      };
    }
    if(b == infoButton) {
      try {
        new LicenseFrame();
      } catch (IOException e) {
        new SErrorDialog(e);
      }
    }
  }
}
