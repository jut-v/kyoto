import java.awt.*;
import java.io.IOException;

import javax.swing.*;

import Util.*;
import settingsUI.object.*;

public class ObjectServantSettingsView extends JPanel {
  ObjectModel model; //設定を反映するモデル
  MapModel map; //この召喚物が設定されるマップ
  SFieldsPanel fp; //設定項目を表示するパネル
  ObjectPreviewPanelView previewPanel; //召喚物のプレビューを行うパネル

  public ObjectServantSettingsView(ObjectModel model, MapModel map) {
    this.model = model;
    this.map = map;
    ObstacleModel servant = model.getServant() != null ? model.getServant() : new ObstacleModel(new Rect(), map);
    //フィールドを追加
    fp = new SFieldsPanel();
    fp.setEnabledCheckBox(model.getServant() != null, "召喚物", "召喚物を有効にする");
    fp.setImageSelector(servant.getImage());
    fp.setBodySize(servant.getBody());
    fp.setHitPointField(servant.getHitPoint());
    fp.setDamageField(servant.getCollisionDamage());
    fp.setCampSelector(servant.getObjectCamp());
    fp.setReactionSelector(servant.getReactionMode());
    fp.setBodyBehaviorFields(servant.getBodyBehavior());
    fp.setGenerateBehaviorFields(model.getGenerateBehavior());
    fp.setGenerateSupervisorField(model.getGenerateSupervisor());
    fp.setFieldsEnabledByCheckBox();
    previewPanel = new ObjectPreviewPanelView(servant);
    //パネルを追加
    this.setLayout(new GridLayout(1, 2));
    this.add(fp);
    this.add(previewPanel);
  }

  //設定内容をモデルに反映
  public void setValues() throws IOException {
    if (fp.isCheckBoxEnabled()) {
      if (model.getServant() == null) model.setServant(new ObstacleModel(fp.getBodyRect(), map));
      model.getServant().setHitPoint(fp.getHitPoint());
      model.getServant().setCollisionDamage(fp.getDamage());
      model.getServant().setBodyBehavior(fp.getBodyBehavior());
      model.setGenerateBehavior(fp.getGenerateBehavior());
      model.getServant().setBodyWithCamera(fp.getBodyRect());
      previewPanel.setBody(fp.getBodyRect());
      if(fp.isImagePathChanged()) {
        model.getServant().setImage(fp.getImagePath());
        previewPanel.setImageFromPathString(fp.getImagePath());
      }
      model.getServant().setObjectCamp(fp.getCamp());
      model.getServant().setReactionMode(fp.getReaction());
      model.setGenerateSupervisor(fp.getGenerateSupervisor());
      previewPanel.updateView();
    } else {
      model.setServant(null);
    }
  }
}
