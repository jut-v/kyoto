package gameUI;
import java.awt.*;

public interface UIComponent {
    void draw(Graphics g); //UIの描画を行う
}
