package gameUI;

import java.awt.event.*;

import Util.*;

public interface Hoverable {
  Rect getArea(); //UIの表示範囲
  boolean hoveredStarted(Point2 p, MouseEvent e); //ホバーが始まったときの処理
  boolean hoveredEnded(Point2 p, MouseEvent e); //ホバーが終わったときの処理
}
