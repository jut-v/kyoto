package gameUI.message;

import java.awt.*;
import java.util.ArrayList;

import Util.DataBufferedImage;
import Util.Point2;
import gameUI.background.UIBackgroundWithImage;
import gameUI.style.StyleColor;
import gameUI.style.StyleFont;
import gameUI.text.UIText;

//ゲーム開始前などのメニュー画面を表示
public class UIMessage {
  UIBackgroundWithImage bg; //背景
  UIText titleView; //タイトル
  ArrayList<UIText> textViewList; //文章を表示するViewのリスト
  public UIMessage(String title, DataBufferedImage image, ArrayList<String> textList) {
    Point2 p = new Point2(80, 100);
    bg = new UIBackgroundWithImage(image);
    titleView = new UIText(p, title, StyleFont.headingFont, StyleColor.text);
    textViewList = new ArrayList<>();
    p = new Point2(80, 200);
    for(String text: textList) {
      textViewList.add(new UIText(p, text, StyleFont.defaultFont, StyleColor.text));
      p = p.addY(50);
    }
  }

  public void draw(Graphics g) {
    bg.draw(g);
    titleView.draw(g);
    for(UIText textView: textViewList) {
      textView.draw(g);
    }
  }
}
