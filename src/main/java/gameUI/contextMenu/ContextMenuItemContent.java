package gameUI.contextMenu;

//コンテキストメニューの項目の内容
public class ContextMenuItemContent {
  private String text; //メニューの項目名
  public ContextMenuItemContent(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }
}
