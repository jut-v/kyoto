package gameUI.contextMenu;

public interface ContextMenuViewSupportable {
  void contextMenuItemSelected(ContextMenuItemContent content); //項目が選択されたときに呼び出されるメソッド
  void closeContextMenu(ContextMenuView view); //メニューを閉じるためのメソッド
}
