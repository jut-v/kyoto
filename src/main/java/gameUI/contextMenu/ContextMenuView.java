package gameUI.contextMenu;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import Util.*;
import gameUI.*;
import gameUI.style.StyleColor;
import gameUI.style.StyleRound;

//コンテキストメニュー
public class ContextMenuView implements Clickable {
  private ContextMenuViewSupportable parent; //呼び出し元
  private ArrayList<ContextMenuItemView> viewList; //項目のView
  private Rect r; //描画範囲
  private static int PADDING = 5;
  private static int ITEM_HEIGHT = 30;
  public ContextMenuView (ContextMenuViewSupportable parent, ArrayList<ContextMenuItemContent> contentList, Point2 p, int width) {
    this.parent = parent;
    this.viewList = new ArrayList<ContextMenuItemView>();
    this.r = new Rect(new Point2(p), width, ITEM_HEIGHT * contentList.size() + PADDING * 2);
    //rの位置を調整
    if(r.getP2().getX() > Const.windowWidth) r.move(-r.getWidth(), 0);
    if(r.getP2().getY() > Const.windowHeight) r.move(0, -r.getHeight());
    Point2 nextPosition = new Point2(r.getBasisX() + PADDING, r.getBasisY() + PADDING);
    for(ContextMenuItemContent content: contentList) {
      this.viewList.add(new ContextMenuItemView(this, content, new Rect(new Point2(nextPosition), width, ITEM_HEIGHT)));
      nextPosition = nextPosition.addY(ITEM_HEIGHT);
    }
  }

  public void draw(Graphics g) {
    g.setColor(StyleColor.background);
    g.fillRoundRect(
      (int)r.getBasisX(), (int)r.getBasisY(), (int)r.getWidth(), (int)r.getHeight(),
      StyleRound.defaultRound, StyleRound.defaultRound
    );
    for(ContextMenuItemView view: viewList) view.draw(g);
  }

  public Rect getArea() {
    return new Rect(r);
  }

  public boolean clicked(Point2 p, MouseEvent e) {
    boolean result;
    for(ContextMenuItemView view: viewList) {
      if(view.getArea().hasPoint2(p)) {
        result = view.clicked(p, e);
        if(result) {
          parent.closeContextMenu(this);
          return true;
        }
      }
    }
    return false;
  }

  public void close(Point2 p, MouseEvent e) {
    parent.closeContextMenu(this);
  }

  public void itemSelected(ContextMenuItemContent content) {
    parent.contextMenuItemSelected(content);
  }
}
