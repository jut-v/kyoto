package gameUI.contextMenu;

import java.awt.*;
import java.awt.event.*;

import Util.*;
import gameUI.Clickable;
import gameUI.style.StyleColor;
import gameUI.style.StyleFont;
import gameUI.text.UIText;

//コンテキストメニューの項目
public class ContextMenuItemView implements Clickable {
  public ContextMenuView parent; //メニュー本体
  public ContextMenuItemContent content; //このViewで表示する項目の内容
  public Rect r; //表示範囲
  public ContextMenuItemView(ContextMenuView parent, ContextMenuItemContent content, Rect r) {
    this.parent = parent;
    this.content = content;
    this.r = r;
  }

  public void draw(Graphics g) {
    new UIText(new Rect(r), content.getText(), StyleFont.defaultFont, StyleColor.text).draw(g);
  }

  public Rect getArea() {
    return new Rect(r);
  }

  public boolean clicked(Point2 p, MouseEvent e) {
    parent.itemSelected(content);
    return true;
  }
}
