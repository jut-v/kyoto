package gameUI;

import java.awt.event.*;
import Util.*;

public interface Clickable {
  Rect getArea(); //UIの表示範囲
  boolean clicked(Point2 p, MouseEvent e); //クリックされたときの処理
}
