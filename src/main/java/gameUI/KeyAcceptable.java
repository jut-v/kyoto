package gameUI;

import java.awt.event.*;

public interface KeyAcceptable {
  void keyInput(KeyEvent e); //キーが入力されたとき
  void keyEntered(KeyEvent e); //キーが押されたとき
  void keyLeaved(KeyEvent e); //キーが離されたとき
}
