package gameUI;

import java.awt.event.*;
import Util.*;

public interface WheelAcceptable {
  Rect getArea(); //UIの表示範囲
  void wheelMoved(Point2 p, MouseWheelEvent e); //マウスホイールが動いたとき
}
