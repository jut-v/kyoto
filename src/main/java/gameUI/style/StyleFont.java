package gameUI.style;
import java.awt.*;

public class StyleFont {
  //フォント名
  public static String defaultName = "Noto Sans CJK JP Regular"; //標準フォント
  public static String boldName = "Noto Sans CJK JP Bold"; //太字のフォント
  //フォントの大きさ
  public static int defaultSize = 16; //フォントの標準サイズ
  public static int extraSmallSize = (int)(defaultSize * 0.8); //最も小さいサイズ
  public static int smallSize = (int)(defaultSize * 0.9);  //小さいサイズ
  public static int largeSize = (int)(defaultSize * 1.2); //大きいサイズ
  public static int extraLargeSize = (int)(defaultSize * 1.5); //最も大きいサイズ
  //フォントの太さ
  public static int regularStyle = Font.PLAIN; //標準のフォント
  public static int boldStyle = Font.BOLD; //太字のフォント
  
  public static Font defaultFont = new Font(defaultName, regularStyle, defaultSize); //通常サイズのフォント
  public static Font defaultBoldFont = new Font(boldName, regularStyle, defaultSize); //太字の標準サイズのフォント
  public static Font headingFont = new Font(boldName, regularStyle, extraLargeSize); //見出しのフォント
}
