package gameUI.style;

import java.awt.*;

public class StyleColor {
  public static Color background = new Color(48, 48, 56); //背景
  public static Color backgroundAlpha = new Color(48, 48, 56, 128); //半透明の背景
  public static Color backgroundLight = new Color(60, 60 ,68); //少し明るい背景
  public static Color accentColor = new Color(80, 208 ,255); //ゲームのメインカラー
  public static Color text = new Color(240, 240, 240); //文字列の色
  public static Color darkText = new Color(56, 56, 56); //明るい色の上に表示するときの文字列の色
  public static Color colliderColor = new Color(0, 255, 128); //当たり判定の色
  public static Color objectGeneratedColor = new Color(255, 128, 0); //オブジェクトが作成される範囲を示す色
}