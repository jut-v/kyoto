package gameUI;

import java.awt.event.*;
import Util.*;

public interface Draggable {
  Rect getArea(); //UIの表示範囲
  void draggedStarted(Point2 p, MouseEvent e); //ドラッグが開始されたときの処理
  void draggedChanged(Point2 p, MouseEvent e); //ドラッグの座標が変わったときの処理
  void draggedEnded(Point2 p, MouseEvent e); //ドラッグが終わったときの処理
}
