package gameUI.button;

public interface ButtonSupportable {
    void buttonSelected(BasicButton b); //ボタンが選択されたとき
}
