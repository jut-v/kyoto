package gameUI.button;

import java.awt.*;
import java.awt.event.*;

import Util.*;
import gameUI.Clickable;
import gameUI.UIComponent;
import gameUI.style.StyleColor;
import gameUI.style.StyleFont;
import gameUI.text.UIText;

//ボタンを表示
public class BasicButton implements UIComponent, Clickable {
    ButtonSupportable parent; //ボタンを持つ要素
    Rect r; //範囲
    String text; //表示するテキスト
    private static int ROUND = 10;
    private static int TEXT_PADDING = 5;
    public BasicButton (ButtonSupportable parent, Rect r, String text) {
        this.parent = parent;
        this.r = r;
        this.text = text;
    }

    public Rect getArea() {
        return new Rect(r);
    }

    public boolean clicked(Point2 p, MouseEvent e) {
        parent.buttonSelected(this);
        return true;
    }

    public void draw(Graphics g) {
        g.setColor(StyleColor.accentColor);
        g.fillRoundRect((int)r.getBasisX(), (int)r.getBasisY(), (int)r.getWidth(), (int)r.getHeight(), ROUND, ROUND);
        new UIText(
            new Rect(
                new Point2(r.getBasisX() + TEXT_PADDING, r.getBasisY() + TEXT_PADDING),
                r.getWidth() - 2 * TEXT_PADDING, r.getHeight() - 2 * TEXT_PADDING
            ),
            text, StyleFont.defaultFont, StyleColor.darkText
        ).draw(g);
    }
}
