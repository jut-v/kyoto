package gameUI.select;

import java.awt.*;
import java.awt.event.*;
import Util.*;
import gameUI.Clickable;
import gameUI.button.BasicButton;
import gameUI.button.ButtonSupportable;
import gameUI.style.StyleColor;
import gameUI.style.StyleFont;
import gameUI.text.UIText;

//ゲーム一覧の各ゲームの情報を表示
public class GameInfoView implements Clickable, ButtonSupportable {
    private GameInfoListView gameInfoListView; //一覧の本体
    private GameInfo gi; //ゲームの情報
    private BasicButton editButton; //ゲームの編集画面に移動するためのボタン
    private Point2 p; //表示位置の左上の座標
    public static int WIDTH = 320;
    public static int HEIGHT = 220;
    public static double THUMBNAIL_ASPECT_RATIO = 9.0 / 16.0;
  
    public GameInfoView(GameInfoListView gameInfoListView, GameInfo gi, Point2 p) {
        this.gameInfoListView = gameInfoListView;
        this.gi = gi;
        this.p = p;
    }
  
    public void draw(Graphics g) {
        // thumbnail
        Point2 nextPos = new Point2((int)p.getX(), (int)p.getY());
        g.drawImage(gi.getThumbnail(), (int)nextPos.getX(), (int)nextPos.getY(), WIDTH, (int)(WIDTH * THUMBNAIL_ASPECT_RATIO), null);
        // title
        nextPos = nextPos.addY((int)(WIDTH * THUMBNAIL_ASPECT_RATIO));
        new UIText(new Rect(nextPos, WIDTH - 50, 40), gi.getTitle(), StyleFont.headingFont, StyleColor.text).draw(g);
        editButton = new BasicButton(this, new Rect(nextPos.addXY(WIDTH - 45, 10), 45, 30), "編集");
        editButton.draw(g);
    }

    public Rect getArea() {
        return new Rect(p, WIDTH, HEIGHT);
    }

    public boolean clicked(Point2 p, MouseEvent e) {
        if(editButton.getArea().hasPoint2(p)) {
            editButton.clicked(p, e);
            return true;
        }
        gameInfoListView.gameSelected(this.gi, GameMode.PLAY_MODE);
        return true;
    }

    
    public void buttonSelected(BasicButton b) {
        if(b == editButton) gameInfoListView.gameSelected(this.gi, GameMode.EDIT_MODE);
    }
}
