package gameUI.select;

import Util.*;

public interface GameInfoListViewSupportable {
    void gameInfoSelected (GameInfo gi, GameMode mode); //ゲームが選択されたときに呼び出されるメソッド
    void updateView(); //リストの一覧の表示を更新するためのメソッド
}
