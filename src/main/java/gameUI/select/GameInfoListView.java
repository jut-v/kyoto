package gameUI.select;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import Util.*;
import gameUI.*;

//ゲーム選択画面の一覧を表示
public class GameInfoListView implements Clickable, WheelAcceptable {
    public GameInfoListViewSupportable parent; //ゲーム選択画面を表示しているクラス
    private Rect listSize; //一覧の表示範囲
    private ArrayList<GameInfo> gameInfoList; //表示するゲームのリスト
    private ArrayList<GameInfoView> gameInfoViewList = new ArrayList<>(); //表示するゲームのViewのリスト
    private int row; //表示している列が何列目か
    private static final int ROW_ITEM_NUM = 3;
    private static final int MAX_ROW = 2;
    private final int DISTANCE;

    public GameInfoListView(GameInfoListViewSupportable parent, Rect listSize, ArrayList<GameInfo> gameInfoList) {
        this.parent = parent;
        this.listSize = listSize;
        this.gameInfoList = gameInfoList;
        this.DISTANCE = (int) (listSize.getWidth() - GameInfoView.WIDTH * ROW_ITEM_NUM) / (ROW_ITEM_NUM - 1);
        this.row = 0;
        generateListView();
    }

    public void generateListView() {
        gameInfoViewList.clear();
        Point2 itemPos = new Point2(listSize.getP1());
        for (int i = ROW_ITEM_NUM * row; i < ROW_ITEM_NUM * (row + MAX_ROW) && i < gameInfoList.size(); i++) {
            gameInfoViewList.add(new GameInfoView(this, gameInfoList.get(i), itemPos));
            if ((i + 1) % 3 == 0) {
                itemPos = new Point2(listSize.getBasisX(), itemPos.getY() + GameInfoView.HEIGHT + DISTANCE);
            } else {
                itemPos = itemPos.addX(GameInfoView.WIDTH + DISTANCE);
            }
        }
    }

    public void draw(Graphics g) {
        for(GameInfoView giv: gameInfoViewList) {
            giv.draw(g);
        }
    }

    public Rect getArea () {
        return new Rect(listSize);
    }

    public boolean clicked(Point2 p, MouseEvent e) {
        boolean result = false;
        for(GameInfoView giv: gameInfoViewList) {
            if(giv.getArea().hasPoint2(p)) {
                result = giv.clicked(p, e);
                if (result) return true;
            }
        }
        return false;
    }

    protected void gameSelected(GameInfo gi, GameMode mode) {
        this.parent.gameInfoSelected(gi, mode);
    }

    //リストをスクロール
    public void wheelMoved(Point2 p, MouseWheelEvent e) {
        if(e.getWheelRotation() > 0 && row < gameInfoList.size() / ROW_ITEM_NUM - 1) row++;
        if(e.getWheelRotation() < 0 && row > 0) row--;
        generateListView();
        parent.updateView();
    }
}