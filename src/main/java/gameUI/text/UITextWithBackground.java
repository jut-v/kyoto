package gameUI.text;

import java.awt.*;
import Util.*;
import gameUI.style.StyleColor;

public class UITextWithBackground {
  private UIText uiText; //テキストを表示するView
  private Rect r; //表示範囲

  public UITextWithBackground(Rect r, String text, Font f) {
    uiText = new UIText(new Point2(r.getBasisX(), r.getBasisY() + (r.getHeight() - f.getSize()) / 2), text, f, StyleColor.text);
    this.r = r;
  }

  public void draw(Graphics g) {
    g.setColor(StyleColor.backgroundAlpha);
    g.fillRect((int)r.getBasisX(), (int)r.getBasisY(), (int)r.getWidth(), (int)r.getHeight());
    uiText.draw(g);
  }
}
