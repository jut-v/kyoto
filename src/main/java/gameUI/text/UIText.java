package gameUI.text;

import java.awt.*;

import Util.*;
import gameUI.*;

public class UIText implements UIComponent {
  private Rect r; //表示範囲
  private String text; //表示するテキスト
  private Font f; //フォント
  private Color c; //文字列の色

  public UIText (Rect r, String text, Font f, Color c) {
    this.r = r;
    this.text = text;
    this.f = f;
    this.c = c;
  }
  
  public UIText(Point2 p, String text, Font f, Color c) {
    this.r = new Rect(p, 0, f.getSize());
    this.text = text;
    this.f = f;
    this.c = c;
  }

  public void draw(Graphics g) {
    g.setColor(c);
    g.setFont(f);
    g.drawString(text, (int)r.getBasisX(), (int)(r.getBasisY() + f.getSize() + (r.getHeight() - f.getSize()) / 2));
  }
}
