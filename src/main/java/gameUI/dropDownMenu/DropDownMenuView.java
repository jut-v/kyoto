package gameUI.dropDownMenu;

import java.util.*;

import Util.*;
import gameUI.*;
import gameUI.style.*;
import gameUI.text.UIText;

import java.awt.*;
import java.awt.event.*;

//ドロップダウンメニューを表示
public class DropDownMenuView implements UIComponent, Clickable {
  private DropDownMenuViewSupportable parent; //メニューを表示しているクラス
  private String text; //メニュー名に表示するテキスト
  private Rect r; //メニューが折りたたまれたときの範囲
  private Rect extendedSize; //メニューが開いた状態の範囲
  private boolean isExtended = false; //メニューが開いているか
  private ArrayList<DropDownMenuItemView> viewList; //表示する項目
  private static int ITEM_HEIGHT = 30;

  public DropDownMenuView(DropDownMenuViewSupportable parent, String text, ArrayList<DropDownMenuItemContent> contentList, Rect r) {
    this.parent = parent;
    this.text = text;
    this.r = r;
    this.viewList = new ArrayList<DropDownMenuItemView>();
    Point2 nextPosition = new Point2((int)r.getBasisX(), (int)r.getBasisY() + r.getHeight());
    for(DropDownMenuItemContent content: contentList) {
      this.viewList.add(
        new DropDownMenuItemView(this, content, new Rect(nextPosition, r.getWidth(), ITEM_HEIGHT))
      );
      nextPosition = nextPosition.addY(ITEM_HEIGHT);
    }
    this.extendedSize = new Rect(new Point2(r.getBasisX(), r.getBasisY()), r.getWidth(), nextPosition.getY() - r.getBasisY());
  }

  public boolean getState() {
    return isExtended;
  }

  public void draw(Graphics g) {
    g.setColor(StyleColor.accentColor);
    g.fillRoundRect(
      (int)r.getBasisX(), (int)r.getBasisY(), (int)r.getWidth(), (int)r.getHeight(),
      StyleRound.defaultRound, StyleRound.defaultRound
    );
    new UIText(r, text, StyleFont.defaultFont, StyleColor.darkText).draw(g);
    g.setColor(StyleColor.darkText);
    g.drawLine(
      (int)(r.getBasisX() + r.getWidth() - 30), (int)r.getBasisY() + 10,
      (int)(r.getBasisX() + r.getWidth() - 20), (int)r.getBasisY() + 20
    );
    g.drawLine(
      (int)(r.getBasisX() + r.getWidth() - 10), (int)r.getBasisY() + 10,
      (int)(r.getBasisX() + r.getWidth() - 20), (int)r.getBasisY() + 20
    );
    if(isExtended) {
      g.setColor(StyleColor.background);
      g.fillRoundRect(
        (int)extendedSize.getBasisX(), (int)extendedSize.getBasisY() + (int)r.getHeight(),
        (int)extendedSize.getWidth(), (int)extendedSize.getHeight() - (int)r.getHeight(),
        StyleRound.defaultRound, StyleRound.defaultRound
      );
      for(DropDownMenuItemView view: viewList) {
        view.draw(g);
      }
    }
  }

  public Rect getArea() {
    return isExtended ? extendedSize : r;
  }

  public boolean clicked(Point2 p, MouseEvent e) {
    boolean result;
    if(r.hasPoint2(p)) {
      isExtended = !isExtended;
      parent.menuSelected();
      return true;
    }
    for (DropDownMenuItemView view : viewList) {
      if(view.getArea().hasPoint2(p)) {
        result = view.clicked(p, e);
        if(result) return true;
      }
    }
    return false;
  }

  public void itemSelected(DropDownMenuItemContent content) {
    parent.menuItemSelected(content);
    isExtended = false;
  }

  public void setMenuText(String text) {
    this.text = text;
  }
}
