package gameUI.dropDownMenu;

public interface DropDownMenuViewSupportable {
  void menuSelected(); //メニュー自体が選択されたとき
  void menuItemSelected(DropDownMenuItemContent content); //メニューの項目が選択されたとき
}
