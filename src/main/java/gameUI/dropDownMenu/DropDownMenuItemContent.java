package gameUI.dropDownMenu;

//ドロップダウンメニューの内容
public class DropDownMenuItemContent {
  private String text; //メニューの項目
  public DropDownMenuItemContent(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }
}
