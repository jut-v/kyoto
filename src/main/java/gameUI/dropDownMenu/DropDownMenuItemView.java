package gameUI.dropDownMenu;

import java.awt.*;
import java.awt.event.*;
import Util.*;
import gameUI.*;
import gameUI.style.*;
import gameUI.text.UIText;

//ドロップダウンメニューの項目
public class DropDownMenuItemView implements UIComponent, Clickable{
  private DropDownMenuView menu; //メニュー本体
  private DropDownMenuItemContent content; //表示する内容
  private Rect r; //表示範囲
  public DropDownMenuItemView(DropDownMenuView menu, DropDownMenuItemContent content, Rect r) {
    this.menu = menu;
    this.content = content;
    this.r = r;
  }

  public void draw(Graphics g) {
    new UIText(r, content.getText(), StyleFont.defaultFont, StyleColor.text).draw(g);
  }

  public Rect getArea() {
    return new Rect(r);
  }

  public boolean clicked(Point2 p, MouseEvent e) {
    menu.itemSelected(content);
    return true;
  }
}
