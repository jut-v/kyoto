package gameUI.background;

import java.awt.*;
import Util.DataBufferedImage;
import Util.Rect;
import gameUI.style.StyleColor;

//画像の付いた背景を表示する
public class UIBackgroundWithImage extends UIBackground {
  DataBufferedImage image; //背景に表示する画像
  public UIBackgroundWithImage(DataBufferedImage image) {
    super();
    this.image = image;
  }

  public UIBackgroundWithImage(DataBufferedImage image, Rect r) {
    super(r);
    this.image = image;
  }

  public void draw(Graphics g) {
    g.drawImage(image,
      (int)r.getP1().getX(), (int)r.getP1().getY(), (int)r.getP2().getX(), (int)r.getP2().getY(),
      0, 0, (int)image.getWidth(), (int)image.getHeight(), null
    );
    g.setColor(StyleColor.backgroundAlpha);
    g.fillRect((int)r.getBasisX(), (int)r.getBasisY(), (int)r.getWidth(), (int)r.getHeight());
  }
}
