package gameUI.background;

import java.awt.Graphics;

import Util.Const;
import Util.Point2;
import Util.Rect;
import gameUI.UIComponent;
import gameUI.style.StyleColor;

//背景を表示する
public class UIBackground implements UIComponent {
  Rect r; //表示範囲
  public UIBackground () {
    this.r = new Rect(new Point2(0, 0), Const.windowWidth, Const.windowHeight);
  }

  public UIBackground (Rect r) {
    this.r = r;
  }

  public void draw(Graphics g) {
    g.setColor(StyleColor.background);
    g.fillRect((int)r.getBasisX(), (int)r.getBasisY(), (int)r.getWidth(), (int)r.getHeight());
  }
}
