package gameUI;

import java.awt.event.*;
import java.util.*;
import Util.*;

public class UIController implements MouseListener, MouseMotionListener, MouseWheelListener, KeyListener {
  private ArrayList<Clickable> clickableList;
  private ArrayList<Draggable> draggableList;
  private ArrayList<Hoverable> hoverableList;
  private ArrayList<WheelAcceptable> wheelAcceptableList;
  private ArrayList<KeyAcceptable> keyAcceptableList;
  private Draggable draggedComponent;
  private ArrayList<Hoverable> hoveredComponentList;

  public UIController() {
    clickableList = new ArrayList<>();
    draggableList = new ArrayList<>();
    hoverableList = new ArrayList<>();
    wheelAcceptableList = new ArrayList<>();
    keyAcceptableList = new ArrayList<>();
    hoveredComponentList = new ArrayList<>();
  }

  public void addClickable(Clickable component) {
    clickableList.add(component);
  }

  public void addDraggable(Draggable component) {
    draggableList.add(component);
  }

  public void addHoverable(Hoverable component) {
    hoverableList.add(component);
  }

  public void addWheelAcceptable(WheelAcceptable component) {
    wheelAcceptableList.add(component);
  }

  public void addKeyAcceptable(KeyAcceptable component) {
    keyAcceptableList.add(component);
  }

  public void removeClickable(Clickable component) {
    clickableList.remove(component);
  }

  public void removeDraggable(Draggable component) {
    draggableList.remove(component);
  }

  public void removeHoverable(Hoverable component) {
    hoverableList.remove(component);
  }

  public void removeWheelAcceptable(WheelAcceptable component) {
    wheelAcceptableList.remove(component);
  }

  public void removeKeyAcceptable(KeyAcceptable component) {
    keyAcceptableList.remove(component);
  }

  private Point2 getPoint2FromMouseEvent(MouseEvent e) {
    return new Point2(e.getX(), e.getY());
  }

  public void mouseClicked(MouseEvent e) {
    Point2 p = getPoint2FromMouseEvent(e);
    boolean result;
    for(Clickable component: clickableList) {
      if(component.getArea().hasPoint2(p)) {
        result = component.clicked(p, e);
        if(result) break;
      }
    }
  }

  public void mousePressed(MouseEvent e) {
    Point2 p = getPoint2FromMouseEvent(e);
    for (Draggable component : draggableList) {
      if(component.getArea().hasPoint2(p)) {
        draggedComponent = component;
        component.draggedStarted(p, e);
      }
    }
  }

  public void mouseDragged(MouseEvent e) {
    if(draggedComponent != null) {
      Point2 p = getPoint2FromMouseEvent(e);
      draggedComponent.draggedChanged(p, e);
    }
  }

  public void mouseReleased(MouseEvent e) {
    if(draggedComponent != null) {
      Point2 p = getPoint2FromMouseEvent(e);
      draggedComponent.draggedEnded(p, e);
      draggedComponent = null;
    }
  }

  public void mouseMoved(MouseEvent e) {
    Point2 p = getPoint2FromMouseEvent(e);
    for (Hoverable component : hoverableList) {
      if(component.getArea().hasPoint2(p)) {
        if(!hoveredComponentList.contains(component)) {
          component.hoveredStarted(p, e);
          hoveredComponentList.add(component);
        }
      }
    }
    for(Hoverable component: hoveredComponentList) {
      if(!component.getArea().hasPoint2(p)) {
        hoveredComponentList.remove(component);
      }
    }
  }

  public void mouseWheelMoved(MouseWheelEvent e) {
    Point2 p = getPoint2FromMouseEvent(e);
    for (WheelAcceptable component : wheelAcceptableList) {
      if(component.getArea().hasPoint2(p)) component.wheelMoved(p, e);
    }
  }

  public void keyTyped(KeyEvent e) {}

  public void mouseEntered(MouseEvent e) {}
  public void mouseExited(MouseEvent e) {}
  
  public void keyPressed(KeyEvent e) {
    for (KeyAcceptable component : keyAcceptableList) {
      component.keyEntered(e);
      component.keyInput(e);
    }
  }

  public void keyReleased(KeyEvent e) {
    for (KeyAcceptable component : keyAcceptableList) {
      component.keyLeaved(e);
    }
  }
}
