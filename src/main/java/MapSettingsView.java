import javax.swing.*;

import settingsUI.object.SFieldsPanel;

public class MapSettingsView extends JPanel {
  MapModel model;
  private SFieldsPanel fp;

  public MapSettingsView(MapModel model) {
    this.model = model;
    fp = new SFieldsPanel();
    fp.setModelNameFields(model.getName(), "マップ名");
    fp.setImageSelector(model.getBackground(), "マップ背景", "背景を設定");
    fp.setMapScrollValueField(model.getMapScrollValue());
    fp.setMusicSelector(model.getMusicPath(), "BGM", "BGMを設定");
    this.add(fp);
  }

  public void setValues() throws Exception {
    if(fp.isModelNameChanged()) {
      model.setName(fp.getModelName());
    }
    if(fp.isImagePathChanged()) {
      model.setBackground(fp.getImagePath());
    }
    model.setMapScrollValue(fp.getMapScrollValue());
    if(fp.isMusicPathChanged()) {
      model.setMusic(fp.getMusicPath());
    }
  }
}
