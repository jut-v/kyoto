import javax.swing.*;
import Util.*;
import settingsUI.SErrorDialog;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class GameMasterView extends JFrame implements PropertyChangeListener {
  private GameMasterModel gmm; //このゲームを管理するモデル
  private GameSelectView gameSelectView; //このViewから表示する選択画面
  private PlayView playView; //このViewから表示するプレイ画面
  private EditView editView; //このViewから表示する編集画面

  public GameMasterView() {
    this.gmm = new GameMasterModel();
    this.gmm.addPropertyChangeListener(this);
    this.setTitle("Kyoto");
    this.setSize(Const.windowWidth, Const.windowHeight);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //最初は選択画面を表示
    try {
      gameSelectView = new GameSelectView(gmm);
      this.add(gameSelectView);
      this.setVisible(true);
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  private void changeToPlayMode() {
    playView = new PlayView(gmm);
    this.remove(gameSelectView);
    this.setVisible(false);
    this.add(playView);
    this.setVisible(true);
  }

  private void changeToEditMode() {
    editView = new EditView(gmm);
    this.remove(gameSelectView);
    this.setVisible(false);
    this.add(editView);
    this.setVisible(true);
  }

  private void backToGameSelectMode(GameMode oldGameMode) {
    try {
      if(oldGameMode == GameMode.EDIT_MODE) this.remove(editView);
      if(oldGameMode == GameMode.PLAY_MODE) this.remove(playView);
      this.setVisible(false);
      gameSelectView = new GameSelectView(gmm);
      this.add(gameSelectView);
      this.setVisible(true);
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  public void propertyChange(PropertyChangeEvent e) {
    switch (e.getPropertyName()) {
      case PropertyName.GAME_MODE:
        switch((GameMode)e.getNewValue()) {
          case PLAY_MODE: changeToPlayMode(); break;
          case EDIT_MODE: changeToEditMode(); break;
          case GAME_SELECT_MODE: backToGameSelectMode((GameMode)e.getOldValue()); break;
        }
        break;
    }
  }
}
