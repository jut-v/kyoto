import java.io.IOException;

import javax.swing.*;

import settingsUI.object.SFieldsPanel;

public class GameSettingsView extends JPanel {
  GameMasterModel gmm; //このゲームを管理しているモデル
  SFieldsPanel fp; //設定項目を表示するパネル
  public GameSettingsView(GameMasterModel gmm) throws InterruptedException {
    this.gmm = gmm;
    fp = new SFieldsPanel();
    fp.setModelNameFields(gmm.getGameName(), "ゲーム名");
    fp.setImageSelector(gmm.getGameThumbnail(), "サムネイル", "サムネイルを設定");
    this.add(fp);
  }

  //変更をモデルに反映
  public void setValues() throws IOException {
    if(fp.isModelNameChanged()) gmm.setGameName(fp.getModelName());
    if(fp.isImagePathChanged()) gmm.setGameThumbnail(fp.getImagePath());
  }
}
