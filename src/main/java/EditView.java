import Util.*;
import gameUI.UIController;
import gameUI.background.UIBackground;
import settingsUI.SErrorDialog;

import java.awt.*;
import java.util.*;
import java.beans.*;
import javax.swing.*;

public class EditView extends JPanel implements PropertyChangeListener {
  private GameMasterModel gmm;
  private MapModel currentMapModel; //編集中のマップ
  private EditMapView mapView; //編集中のマップを表示するView
  private NavigationView navView; //設定ウィンドウを開くボタンや、保存などのボタンを表示し、操作を受け付けるView
  private UIController c; //このViewのコントローラ
  private static final int NAVIGATION_HEIGHT = 50; //ナビゲーションメニューの高さ

  public EditView(GameMasterModel gmm) {
    this.gmm = gmm;
    this.currentMapModel = getMapModelList().get(0);
    this.navView = new NavigationView(this, new Rect(new Point2(0, 0), Const.windowWidth, NAVIGATION_HEIGHT));
    this.c = new UIController();
    this.addMouseListener(c);
    this.addMouseMotionListener(c);
    this.addMouseWheelListener(c);
    this.addKeyListener(c);
    c.addClickable(navView);
    generateMapView();
    this.setFocusable(true);
  }

  //現在のマップに対応するViewを生成
  private void generateMapView() {
    if(mapView != null) {
      c.removeClickable(mapView);
      c.removeDraggable(mapView);
      c.removeWheelAcceptable(mapView);
    }
    try {
      this.mapView = new EditMapView(this, currentMapModel, new Rect(new Point2(0, 0), Const.windowWidth, Const.windowHeight));
      c.addClickable(mapView);
      c.addDraggable(mapView);
      c.addWheelAcceptable(mapView);
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  @Override
  public void paintComponent(Graphics g) {
    new UIBackground().draw(g);
    mapView.draw(g);
    navView.draw(g);
  }

  //外部から再描画するためのメソッド
  public void updateView() {
    this.repaint();
  }

  //NavigationViewから呼び出す用

  //必要なデータを取得

  public ArrayList<MapModel> getMapModelList() {
    return this.gmm.getMapModels();
  }

  public MapModel getCurrentMapModel() {
    return currentMapModel;
  }

  public Point2 getCameraPosition() {
    return new Point2(currentMapModel.getCameraModel().getCamPos().getP1());
  }

  //設定フレームを開く

  public void showGameSettings() {
    new GameSettingsFrameView(gmm);
  }

  public void showMapSettings() {
    new MapSettingsFrameView(this, currentMapModel);
  }

  //マップを変更
  public void changeCurrentMap(String mapName) {
    if(mapName != currentMapModel.getName()) {
      for(MapModel mm: getMapModelList()) {
        if(mapName == mm.getName()) {
          currentMapModel = mm;
          break;
        }
      }
      generateMapView();
      updateView();
    }
  }

  //マップを作成
  public void createNewMap() {
    try {
      gmm.addMapModel();
      navView.generateMapMenu();
      updateView();
    } catch(Exception e) {
      new SErrorDialog(e);
    }
  }

  //保存

  public void save() {
    try {
      gmm.saveGame();
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  public void saveAndQuit() {
    try {
      gmm.saveGame();
      gmm.setMode(GameMode.GAME_SELECT_MODE);
    } catch (Exception e) {
      new SErrorDialog(e);
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent e) {
    if(e.getPropertyName() == PropertyName.ADDED_MAP_MODEL) {
      navView.generateMapMenu();
    }
  }
}
