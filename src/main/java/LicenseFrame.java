import gameUI.style.StyleColor;
import gameUI.style.StyleFont;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.*;

// ライセンスを表示するFrame
public class LicenseFrame extends JFrame {
  public LicenseFrame() throws IOException {
    this.setSize(300, 400);

    JList<LicenseListItem> jList = new JList<>(getLicenseListItems());
    jList.setBackground(StyleColor.background);
    jList.setCellRenderer(new LicenseListRenderer());
    LicenseFrame licenseFrame = this;
    jList.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
          int index = jList.locationToIndex(e.getPoint());
          LicenseListItem licenseListItem = jList.getModel().getElementAt(index);
          new LicenseSentenceDialog(licenseFrame, licenseListItem.productName, Dialog.ModalityType.DOCUMENT_MODAL, licenseListItem.licenseSentence);
        }
      }
    });

    JScrollPane jScrollPane = new JScrollPane(jList);
    jScrollPane.setBackground(StyleColor.background);
    this.add(jScrollPane);

    this.setVisible(true);
  }

  private LicenseListItem[] getLicenseListItems() throws IOException {
    ArrayList<LicenseListItem> licenseListItems = new ArrayList<>();
    try (InputStreamReader csvInputStreamReader = new InputStreamReader(Objects.requireNonNull(Kyoto.class.getResourceAsStream("LICENSE/list.csv")), StandardCharsets.UTF_8)) {
      try (BufferedReader csvBufferedReader = new BufferedReader(csvInputStreamReader)) {
        String line;
        String[] data;
        while ((line = csvBufferedReader.readLine()) != null) {
          data = line.split(",");
          String productName = data[0];
          String licenseName = data[1];
          String licenseSentence = getLicenseSentence(productName);
          licenseListItems.add(new LicenseListItem(productName, licenseName, licenseSentence));
        }
      }
    }
    return licenseListItems.toArray(new LicenseListItem[0]);
  }

  private String getLicenseSentence(String productName) throws IOException {
    StringBuilder licenseSentenceBuilder = new StringBuilder();
    try (InputStreamReader licenseInputStreamReader = new InputStreamReader(Objects.requireNonNull(Kyoto.class.getResourceAsStream("LICENSE/" + productName + "/LICENSE")), StandardCharsets.UTF_8)) {
      try (BufferedReader licenseBufferedReader = new BufferedReader(licenseInputStreamReader)) {
        String licenseRow;
        while ((licenseRow = licenseBufferedReader.readLine()) != null) {
          licenseSentenceBuilder.append(licenseRow);
          licenseSentenceBuilder.append("\n");
        }
      }
    }
    return licenseSentenceBuilder.toString();
  }

  private static class LicenseListRenderer extends JPanel implements ListCellRenderer<LicenseListItem> {
    private final JLabel productNameLabel;
    private final JLabel licenseNameLabel;

    public LicenseListRenderer() {
      this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
      this.setBackground(StyleColor.background);
      productNameLabel = new JLabel();
      productNameLabel.setBackground(StyleColor.background);
      productNameLabel.setForeground(StyleColor.text);
      productNameLabel.setFont(StyleFont.headingFont);
      this.add(productNameLabel);

      licenseNameLabel = new JLabel();
      licenseNameLabel.setBackground(StyleColor.background);
      licenseNameLabel.setForeground(StyleColor.text);
      licenseNameLabel.setFont(StyleFont.defaultBoldFont);
      this.add(licenseNameLabel);
    }
    @Override
    public Component getListCellRendererComponent(JList<? extends LicenseListItem> list, LicenseListItem value, int index, boolean isSelected, boolean cellHasFocus) {
      this.productNameLabel.setText(value.productName);
      this.licenseNameLabel.setText(value.licenseName);

      if (cellHasFocus) {
        this.setBackground(StyleColor.backgroundLight);
        productNameLabel.setBackground(StyleColor.backgroundLight);
        licenseNameLabel.setBackground(StyleColor.backgroundLight);
      } else {
        this.setBackground(StyleColor.background);
        productNameLabel.setBackground(StyleColor.background);
        licenseNameLabel.setBackground(StyleColor.background);
      }

      return this;
    }
  }

  private record LicenseListItem(String productName, String licenseName, String licenseSentence){}

  private static class LicenseSentenceDialog extends JDialog {
    public LicenseSentenceDialog(Window owner, String title, Dialog.ModalityType modalityType, String licenseSentence) {
      super(owner, title, modalityType);
      this.setSize(300, 400);
      this.setBackground(StyleColor.background);
      JTextArea jTextArea = new JTextArea();
      jTextArea.setText(licenseSentence);
      jTextArea.setLineWrap(true);
      jTextArea.setEditable(false);
      jTextArea.setBackground(StyleColor.background);
      jTextArea.setForeground(StyleColor.text);
      jTextArea.setFont(StyleFont.defaultFont);
      JScrollPane jScrollPane = new JScrollPane(jTextArea);
      jScrollPane.setBackground(StyleColor.background);
      this.add(jScrollPane);
      this.setVisible(true);
    }
  }
}
