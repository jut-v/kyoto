import Util.*;
import Util.io.*;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

//マップ上で動くModelの抽象クラス
abstract class ObjectModel{
  protected Rect body,figBody;//当たり判定と画像の範囲
  protected final Point2 initPoint;//初期位置
  protected Point2 revivalPoint=null;//次の形態の生成位置
  protected DataBufferedImage img=null,originalImage=null;//現在の画像と元画像
  protected MapModel map;//インスタンスの所属するマップ
  protected ObstacleModel servant=null,reincarnation=null;//召喚物と次の形態
  protected Behavior generateBehavior=new Behavior("0","0");//生成位置の振る舞い
  protected GenerateSupervisor generateSupervisor=new GenerateSupervisor(""+Const.FPS);//弾幕の召喚タイミングを教えてくれるクラス
  protected int count=0,hitPoint=NON_CHARACTER,collisionDamage=0;//生成されてからのフレーム数、HP、衝突相手に与えるダメージ
  protected ObjectCamp camp=ObjectCamp.STAGE;//所属陣営
  private DamageSupervisor damageSupervisor=new DamageSupervisor();//1秒以内に対象オブジェクトからダメージを受けたことがあるか教えてくれるクラス
  public static final int NON_CHARACTER=Integer.MIN_VALUE;//HPを持たないキャラクターの場合、hitPoint==NON_CHARACTER
  public ObjectModel(Rect rt,MapModel map){
    setEssentials(rt, map);initPoint=rt.getMedian();
  }
  public ObjectModel(Rect rt,String path,MapModel map) throws IOException {
    setEssentials(rt, map);initPoint=rt.getMedian();
    setImage(path);
  }
  public ObjectModel(ObjectModel o){
    copyEssentials(o);initPoint=new Point2(o.initPoint);
  }
  public ObjectModel(ObjectModel o,Point2 initPoint){
    copyEssentials(o);moveBodiesTo(initPoint);this.initPoint=new Point2(initPoint);
  }
  private void setEssentials(Rect rt,MapModel map){
    body=figBody=rt;
    this.map=map;
  }
  private void copyEssentials(ObjectModel o){
    body=o.getBody();
    map=o.map;
    originalImage=o.originalImage;
    img=o.img;
    figBody=(img!=null?new Rect(o.figBody):body);
    servant=o.servant;
    generateBehavior=new Behavior(o.generateBehavior);
    generateSupervisor=new GenerateSupervisor(o.generateSupervisor);
    hitPoint=o.hitPoint;collisionDamage=o.collisionDamage;
    camp=o.camp;
    reincarnation=o.reincarnation;
    if(o.revivalPoint!=null){
      revivalPoint=new Point2(o.revivalPoint);
    }
  }

  public void setBody(Rect rt){
    body=rt;
    if(figBody!=body){
      figBody.moveTo(body.getMedian());
    }
  }
  public Rect getBody(){
    return new Rect(body);
  }
  public boolean setFigBody(Rect rt){
    if(img==null)return false;
    img=new DataBufferedImage((int)rt.getWidth(),(int)rt.getHeight(),BufferedImage.TYPE_3BYTE_BGR,originalImage.getFilePath());
    img.createGraphics().drawImage(originalImage.getScaledInstance(
      (int)rt.getWidth(), (int)rt.getHeight(), Image.SCALE_AREA_AVERAGING)
      ,0,0,(int)rt.getWidth(),(int)rt.getHeight(),null);
    figBody=rt;
    body.moveTo(figBody.getMedian());
    return true;
  }
  public Rect getFigBody(){
    return figBody;
  }
  public void setBodyWithCamera(Rect rt){
    Point2 cameraPosition=map.getCameraModel().getCamPos().getP1();
    body=rt;
    if(figBody!=body){
      figBody.moveTo(body.getMedian());
    }
    moveBodies(cameraPosition.getX(), cameraPosition.getY());
  }
  public void setFigBodyWithCamera(Rect rt) throws NullPointerException{
    Point2 cameraPosition=map.getCameraModel().getCamPos().getP1();
    img=new DataBufferedImage((int)rt.getWidth(),(int)rt.getHeight(),BufferedImage.TYPE_3BYTE_BGR, originalImage.getFilePath());
    img.createGraphics().drawImage(originalImage.getScaledInstance(
      (int)rt.getWidth(), (int)rt.getHeight(), Image.SCALE_AREA_AVERAGING)
      ,0,0,(int)rt.getWidth(),(int)rt.getHeight(),null);
    figBody=rt;
    body.moveTo(figBody.getMedian());
    moveBodies(cameraPosition.getX(), cameraPosition.getY());
  }
  public void setFigBodyWithCameraInKeepingRatioToBody(Rect rt) throws NullPointerException{
    float ratioW=body.getWidth()/figBody.getWidth(),ratioH=body.getHeight()/figBody.getHeight();
    Point2 cameraPosition=map.getCameraModel().getCamPos().getP1();
    try {
      setFigBodyWithCamera(rt);
    } catch (NullPointerException e) {
      throw e;
    }
    body.setPoint(new Point2(), new Point2(ratioW*rt.getWidth(), ratioH*rt.getHeight()));
    moveBodies(cameraPosition.getX(), cameraPosition.getY());
  }
  private Rect getRectFromCamera(Rect rt){
    Point2 cameraPosition=map.getCameraModel().getCamPos().getP1();
    Rect copy=new Rect(rt);
    copy.move(-cameraPosition.getX(),-cameraPosition.getY());
    return copy;
  }
  public Rect getBodyFromCamera(){
    return getRectFromCamera(body);
  }
  public Rect getFigBodyFromCamera(){
    return getRectFromCamera(figBody);
  }
  public void moveBodies(float deltaX,float deltaY){
    body.move(deltaX, deltaY);
    if(figBody!=body){
      figBody.move(deltaX, deltaY);
    }
  }
  public void moveBodiesTo(Point2 pt){
    body.moveTo(pt);
    if(figBody!=body){
      figBody.moveTo(pt);
    }
  }
  public void moveBodiesFromCameraTo(Point2 pt){
    moveBodiesTo(map.getCameraModel().getCamPos().getP1());
    moveBodies(pt.getX(), pt.getY());
  }
  private void adjustSetImage(){
    figBody=new Rect(new Point2(),img.getWidth(),img.getHeight());
    figBody.moveTo(body.getMedian());
  }
  public void setImage(String path) throws IOException {
    try {
      originalImage=img=KImageIO.imageRead(new File(path));
    } catch (IOException e) {
      throw new IOException("'"+path+"'が読み込めません。");
    }
    adjustSetImage();
  }
  public void setImage(DataBufferedImage img){
    originalImage=this.img=img;
    adjustSetImage();
  }
  public void setImageAndBodies(String path) throws IOException{
    try {
      originalImage=img=KImageIO.imageRead(new File(path));
    } catch (IOException e) {
      throw new IOException("'"+path+"'が読み込めません。");
    }
    adjustSetImage();body=figBody;
  }
  public void unsetImage(){
    img=originalImage=null;figBody=body;
  }
  public DataBufferedImage getImage(){
    return img;
  }
  public boolean hasImage(){
    return img!=null;
  }
  public void setServant(ObstacleModel ob){
    if(ob==null){
      servant=null;
    }else{
      servant=new ObstacleModel(ob);
      servant.camp=this.camp;
    }
  }
  public ObstacleModel getServant(){
    return servant;
  }
  public void setGenerateBehavior(Behavior behavior){
    this.generateBehavior=behavior;
  }
  public void setGenerateBehavior(String formulaX,String formulaY){
    this.generateBehavior=new Behavior(formulaX,formulaY);
  }
  public void setGenerateBehavior(Vector2 vector,float time){
    this.generateBehavior=new Behavior(vector, time);
  }
  public Behavior getGenerateBehavior(){
    return this.generateBehavior;
  }
  public void setGenerateSupervisor(String formula){
    generateSupervisor=new GenerateSupervisor(formula);
  }
  public void setGenerateSupervisor(GenerateSupervisor gs){
    generateSupervisor=gs;
  }
  public GenerateSupervisor getGenerateSupervisor(){
    return generateSupervisor;
  }
  public void setReincarnation(ObstacleModel ob){
    reincarnation=ob;
  }
  public ObstacleModel getReincarnation(){
    return reincarnation;
  }
  public void setRevivalPoint(Point2 pt){
    revivalPoint=pt;
  }
  public Point2 getRevivalPoint(){
    return revivalPoint;
  }
  public void setHitPoint(int hitPoint){
    this.hitPoint=hitPoint;
  }
  public int getHitPoint(){
    return hitPoint;
  }
  public void setCollisionDamage(int collisionDamage){
    this.collisionDamage=collisionDamage;
  }
  public int getCollisionDamage(){
    return collisionDamage;
  }
  public void setObjectCamp(ObjectCamp camp){
    this.camp=camp;
  }
  public ObjectCamp getObjectCamp(){
    return camp;
  }
  public void damage(ObjectModel o){
    if(camp==o.camp||hitPoint==NON_CHARACTER||!damageSupervisor.judge(o, count))return;
    hitPoint=Math.max(hitPoint-o.collisionDamage,0);
  }
  public boolean inCamera(){
    return map.getCameraModel().inCamera(this);
  }
  //子クラスにおける反応の定義を強制
  abstract void reaction(ObjectModel o) throws UnsupportedAudioFileException, LineUnavailableException, IOException;
  //現在のcount等から所定の位置にservantのコピーを召喚
  protected void summonServant(){
    if(servant==null||generateBehavior==null||!generateSupervisor.judge(count))return;
    Point2 med=body.getMedian(),
           generatePoint=generateBehavior.func(count/Const.FPS).addXY(med.getX(), med.getY());
    ObstacleModel servant=new ObstacleModel(this.servant,generatePoint);
    servant.getBodyBehavior().setSummonerT(count/Const.FPS);
    map.addActiveObstacleModel(servant);
  }
  protected Point2 solveAdjust(ObjectModel o){
    int relation=body.positionalRelation(o.body);
    float deltaX,deltaY;
    switch (relation) {
      case Rect.ABOVE:
        deltaX=0;deltaY=o.body.getP1().getY()-body.getP2().getY();
        break;
      case Rect.BELOW:
        deltaX=0;deltaY=o.body.getP2().getY()-body.getP1().getY();
        break;
      case Rect.LEFT:
        deltaX=o.body.getP1().getX()-body.getP2().getX();deltaY=0;
        break;
      case Rect.RIGHT:
        deltaX=o.body.getP2().getX()-body.getP1().getX();deltaY=0;
        break;
      default:
        deltaX=deltaY=0;
    }
    return new Point2(deltaX, deltaY);
  }
  protected void adjust(ObjectModel o){
    Point2 delta=solveAdjust(o);
    moveBodies(delta.getX(), delta.getY());
  }
}
