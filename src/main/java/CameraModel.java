import Util.MapScrollDirection;
import Util.Point2;
import Util.PropertyName;
import Util.Rect;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

// CameraのModel
public class CameraModel implements PropertyChangeSupportable {
  private Rect camPos; // cameraの位置
  private ObjectModel targetModel; // cameraが追いかけるもの
  private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  private Rect limitRect; // カメラの動ける範囲
  private boolean horizontalLock; // x方向に動くかどうか
  private boolean verticalLock; // y方向に動くかどうか
  private MapScrollDirection mapScrollDirection; // カメラの動く方向

  CameraModel(Rect camPos) {
    this.camPos = camPos;
  }

  public Rect getCamPos() {
    return camPos;
  }


  // cameraがついていくのに必要な情報をセットする
  public void setFollowMove(ObjectModel targetModel, Rect limitRect, boolean horizontalLock, boolean verticalLock, MapScrollDirection mapScrollDirection) {
    this.limitRect = limitRect;
    this.targetModel = targetModel;
    this.horizontalLock = horizontalLock;
    this.verticalLock = verticalLock;
    this.mapScrollDirection = mapScrollDirection;
  }

  // カメラに含まれているかどうか
  public boolean inCamera(ObjectModel objectModel) {
    return camPos.existOn(objectModel.getBody());
  }

  // カメラを移動させる
  public void move() {
    Rect oldCamPos = new Rect(camPos);
    if (limitRect != null) {
      Point2 point2 = new Point2();
      if (!horizontalLock) {
        point2.setX(targetModel.getBody().getMedian().getX());
      } else {
        point2.setX(this.camPos.getMedian().getX());
      }
      if (!verticalLock) {
        point2.setY(targetModel.getBody().getMedian().getY());
      } else {
        point2.setY(this.camPos.getMedian().getY());
      }
      float halfWidth = this.camPos.getWidth() / 2;
      float halfHeight = this.camPos.getHeight() / 2;
      if (point2.getX() - halfWidth <= limitRect.getP1().getX()) {
        point2.setX(limitRect.getP1().getX() + halfWidth);
        if (mapScrollDirection == MapScrollDirection.horizontalMinus) {
          targetModel.map.setIsCameraMoveFinished(true);
        }
      }
      if (limitRect.getP2().getX() <= point2.getX() + halfWidth) {
        point2.setX(limitRect.getP2().getX() - halfWidth);
        if (mapScrollDirection == MapScrollDirection.horizontalPlus) {
          targetModel.map.setIsCameraMoveFinished(true);
        }
      }
      if (point2.getY() - halfHeight <= limitRect.getP1().getY()) {
        point2.setY(limitRect.getP1().getY() + halfHeight);
        if (mapScrollDirection == MapScrollDirection.verticalMinus) {
          targetModel.map.setIsCameraMoveFinished(true);
        }
      }
      if (limitRect.getP2().getY() <= point2.getY() + halfHeight) {
        point2.setY(limitRect.getP2().getY() - halfHeight);
        if (mapScrollDirection == MapScrollDirection.verticalPlus) {
          targetModel.map.setIsCameraMoveFinished(true);
        }
      }
      float currentX = this.camPos.getMedian().getX();
      float currentY = this.camPos.getMedian().getY();
      if (mapScrollDirection == MapScrollDirection.horizontalPlus) {
        if (currentX > point2.getX()) {
          point2.setX(currentX);
        }
      } else if (mapScrollDirection == MapScrollDirection.horizontalMinus) {
        if (currentX < point2.getX()) {
          point2.setX(currentX);
        }
      } else if (mapScrollDirection == MapScrollDirection.verticalPlus) {
        if (currentY > point2.getY()) {
          point2.setY(currentY);
        }
      } else if (mapScrollDirection == MapScrollDirection.verticalMinus) {
        if (currentY < point2.getY()) {
          point2.setY(currentY);
        }
      }
      this.camPos.moveTo(point2);
    }
    propertyChangeSupport.firePropertyChange(PropertyName.MOVED_CAMERA_MODEL, oldCamPos, this.camPos);
  }


  @Override
  public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
    propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
    propertyChangeSupport.removePropertyChangeListener(propertyChangeListener);
  }
}
