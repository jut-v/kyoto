import Util.*;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.lang.reflect.Field;

//プレイヤーとなるクラス
public class PlayableCharacterModel extends ObjectModel implements PropertyChangeSupportable {
  private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);//外部との通信をする
  private Vector2 sensitivity = new Vector2(1.0f, 1.0f);//感度を保持する
  private boolean left = false;//左に移動する時にtrueになる
  private boolean right = false;//右に移動する時にtrueになる
  private boolean up = false;//上に移動する時にtrueになる
  private boolean down = false;//下に移動する時にtrueになる
  private float dx = 0.0f;//現在x軸方向にどれだけ移動すれば良いのかを保存する
  private float dy = 0.0f;//現在y軸方向にどれだけ移動すれば良いのかを保存する
  private ObstacleModel teleportKey;//これとぶつかると次のマップへ
  private boolean fire=false;//弾幕を放つボタンが押されるとtrue

  public PlayableCharacterModel(Rect rt, MapModel map) {
    super(rt, map);camp=ObjectCamp.PLAYER;
  }

  public PlayableCharacterModel(Rect rt, String path, MapModel map) throws IOException {
    super(rt, path, map);camp=ObjectCamp.PLAYER;
  }

  public Vector2 getSensitivity() {
    return sensitivity;
  }

  public void setSensitivity(Vector2 sensitivity) {
    this.sensitivity = sensitivity;
  }

  public void setTeleportKey(ObstacleModel ob){
    teleportKey=ob;
  }

  public ObstacleModel getTeleportKey() {
    return teleportKey;
  }

  public void setSummoning(boolean fire){
    this.fire=fire;
  }

  public boolean getSummoning(){
    return fire;
  }

  public void move(float top, float right, float bottom, float left) {
    Rect oldBody = new Rect(body);
    Rect oldFigBody = new Rect(figBody);
    float dx = right - left;
    float dy = bottom - top;
    moveBodies(dx * sensitivity.getX(), dy * sensitivity.getY());
    propertyChangeSupport.firePropertyChange(PropertyName.MOVED_PLAYABLE_CHARACTER_BODY, oldBody, body);
    propertyChangeSupport.firePropertyChange(PropertyName.MOVED_PLAYABLE_CHARACTER_FIG_BODY, oldFigBody, figBody);
  }

  public void move(boolean up, boolean right, boolean down, boolean left) {
    this.up = up;
    this.right = right;
    this.down = down;
    this.left = left;
  }

  public void moveRight(boolean right) {
    this.right = right;
    calcDxDy();
  }

  public void moveLeft(boolean left) {
    this.left = left;
    calcDxDy();
  }

  public void moveDown(boolean down) {
    this.down = down;
    calcDxDy();
  }

  public void moveUp(boolean up) {
    this.up = up;
    calcDxDy();
  }

  private void calcDxDy() {
    dx = 0.0f;
    if (right) {
      dx += 1.0f;
    }
    if (left) {
      dx -= 1.0f;
    }
    dy = 0.0f;
    if (up) {
      dy -= 1.0f;
    }
    if (down) {
      dy += 1.0f;
    }
  }

  public void mapScroll(float scroll, MapScrollDirection mapScrollDirection) {
    Rect camPos = this.map.getCameraModel().getCamPos();
    Rect oldBody = new Rect(this.getBody());
    if (!camPos.existOn(oldBody)) {
      gameOver();
      return;
    }
    if (mapScrollDirection == MapScrollDirection.horizontalPlus || mapScrollDirection == MapScrollDirection.horizontalMinus) {
      oldBody.move(dx * sensitivity.getX() + scroll, dy * sensitivity.getY());
    } else {
      oldBody.move(dx * sensitivity.getX(), dy * sensitivity.getY() + scroll);
    }
    if (camPos.wholeIn(oldBody)) {
      moveBodiesTo(oldBody.getMedian());
    }
    propertyChangeSupport.firePropertyChange(PropertyName.MOVED_PLAYABLE_CHARACTER_BODY, null, body);
    propertyChangeSupport.firePropertyChange(PropertyName.MOVED_PLAYABLE_CHARACTER_FIG_BODY, null, figBody);
  }

  private void gameOver() {
    this.map.gameOver();
  }

  private ObstacleModel backupTeleportKey;
  private Rect backupBody;
  private Rect backUpFigBody;
  private int backupHitPoint;
  private Behavior backupGenerateBehavior;
  private GenerateSupervisor backupGenerateSuperVisor;
  private int backupCount;
  private DamageSupervisor backupDamageSupervisor;

  public void doBackup() {
    this.backupTeleportKey = this.teleportKey;
    this.backupBody = new Rect(this.body);
    this.backUpFigBody = new Rect(this.figBody);
    this.backupHitPoint = this.hitPoint;
    this.backupGenerateBehavior = new Behavior(this.generateBehavior);
    this.backupGenerateSuperVisor = new GenerateSupervisor(this.generateSupervisor);
    this.backupCount = this.count;
    this.backupDamageSupervisor = new DamageSupervisor();
  }

  public void restoreBackup() {
    this.teleportKey = this.backupTeleportKey;
    this.body = this.backupBody;
    this.figBody = this.backUpFigBody;
    this.hitPoint = this.backupHitPoint;
    this.generateBehavior = this.backupGenerateBehavior;
    this.generateSupervisor = this.backupGenerateSuperVisor;
    this.count = this.backupCount;
    try {
      Field damageSupervisor = this.getClass().getSuperclass().getDeclaredField("damageSupervisor");
      damageSupervisor.setAccessible(true);
      damageSupervisor.set(this, new DamageSupervisor());
    } catch (NoSuchFieldException | IllegalAccessException e) {
      e.printStackTrace();
      throw new InternalError("playableModel can not restored.");
    }
  }

  @Override
  public void damage(ObjectModel o) {
    super.damage(o);
    if(hitPoint==0)
      gameOver();
  }

  @Override
  protected void summonServant() {
    if(fire)super.summonServant();
    count++;
  }

  @Override
  public void reaction(ObjectModel o) throws UnsupportedAudioFileException, LineUnavailableException, IOException {
    if(o.camp==ObjectCamp.PLAYER)return;
    if(teleportKey==o)
      map.nextMap();//returnしないので、teleportKey.collisionDamage<0なら回復もできる。
    damage(o);adjust(o);
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
    propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
    propertyChangeSupport.removePropertyChangeListener(propertyChangeListener);
  }
}
