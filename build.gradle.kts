plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

application {
  mainClass.set("Kyoto")
}

group = "com.example.informatics.media.team"
version = "0.1"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    implementation("net.objecthunter:exp4j:0.4.8")
    implementation("com.google.code.gson:gson:2.8.9")
    implementation("commons-io:commons-io:2.11.0")
    implementation("net.lingala.zip4j:zip4j:2.9.1")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

tasks.withType(JavaCompile::class) {
  options.encoding = "UTF-8"
}

tasks.getting(Jar::class) {
  manifest {
    attributes["Main-Class"] = "Kyoto"
  }
}
