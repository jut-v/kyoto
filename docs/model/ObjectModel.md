# ObjectModel

Assignee: @tkm3845

## メンバ変数

- public static final int STAGE:
  ステージなど、ヒットポイントを持たない場合は、hitPoint==ObjectModel.STAGEであることが保証されなければなりません。
  
- protected:
  - Rect
    - body:
      thisの当たり判定の範囲
    - figBody:
      thisの画像の範囲。無い時はnull
    
  - final Point2 initPoint:
    thisの生成位置を保持
    
  - Point2 revivalPoint:
    以下のreincarnationが召喚される位置を保持
  
  - BufferImage img:
    thisの画像。無い時はnull
    
  - MapModel map:
    thisの所属するMapModelを保持
    
  - ObstacleModel servant:
    弾幕と敵の召喚に使われます。初期値はnull
  
  - Behavior generateBehavior:
    servantの召喚位置の振る舞いを保持します。初期値はnull
  
  - GenerateSupervisor generateSupervisor:
    召喚するタイミングを保持します。初期値は毎秒FPS回
  
  - int
    
    - count:
      thisがマップ上で動くようになってから何フレーム(FPS的に)進んだかを数えます。当然初期値は0です。
    
    - hitPoint:
      ヒットポイントを保持。ステージなどの、ヒットポイントを持たないものではhitPoint==ObjectModel.STAGEとなっており、これが既定です。ヒットポイントがある場合は必ず設定してください。
    - collisionDamage:
      衝突相手にcollisionDamageだけダメージを与えることになっています。
    
  - ObjectModel reincarnation:
    thisが消える時にこれを召喚します。初期値はnullです。
  
  - ObjectCamp camp:
    thisの陣営を保持します。詳しくは[ObjectCamp](../others/ObjectCamp.md)をご覧下さい。

## メンバ関数

### ObjectModel(Rect rt, MapModel map)

ptの座標に画像を読み込まない状態でObjectModelを作ります。thisはmapに所属することになります。

### ObjectModel(Rect rt, String path, MapModel map)

ptの座標にpathの画像を読み込んだObjectModelを作ります。thisはmapに所属することになります。

### ObectModel(ObjectModel o)

コピーコンストラクタです。body, figBody, revivalPoint(それぞれ当たり判定、画像の範囲、次の形態を生成する位置)以外は同じものを参照するので注意してください。

### ObectModel(ObjectModel o, Point2 initPoint)

基本はコピーコンストラクタと同じですが、initPointで初期化する点で異なります。

### Rect getBody()

bodyを取得します。

### Rect getFigBody()

figBodyを取得します。fibBody==nullな場合はぬるぽを投げます。~~ガッ~~

### setBoyWithCamera(Rect rt)

bodyをrtに変更し、figBodyが存在していればその位置を合わせます。

### Rect getBodyFromCamera()

カメラの左上の座標を原点として、thisの相対的な当たり判定を返します。

### Rect getFigBodyFromCamera()

カメラの左上の座標を原点として、thisの相対的な画像の範囲を返します。fibBody==nullな場合はぬるぽを投げます。

### void moveBodies(float deltaX, float deltaY)

x軸方向にdeltaX、y軸方向にdeltaYだけ移動します。

### void moveBodiesTo(Point2 pt)

body, figBody共に、ptを中央に持つRectに変更します。

### void setImage(String path)

pathのファイルの画像を読み込んで、ObjectModelの画像をそれに変更します。失敗するとIOExceptionを投げます。

### BufferedImage getImage()

this.imgをそのまま返します。

### void setServant(ObjectModel ob)

obをコピーしたものをthisの召喚対称に設定します。

### void setGenerateBehavior(Behavior behavior)

behaviorを用いて召喚位置のBehaviorをセットします。

### void setGenerateBehavior(String formulaX,String formulaY)

formulaX, formulaYを用いて召喚位置のBehaviorをセットします。

### void setGenerateBehavior(Vector2 vector,float time)

vector, timeを用いて召喚位置のBehaviorをセットします。

### void Behavior getGenerateBehavior()

召喚位置のBehaviorを返します。behaviorの使い回しなどの用途が想定されています。

### void setGenerateSupervisor(String formula)

召喚するタイミングを設定します。既定では毎秒FPSと同数召喚するように設定されています。既定はこのクラスによる仕様ですが、召喚の仕方に関して詳しく知りたい方は[GenerateSupervisor](../others/GenerateSupervisor.md)をご覧下さい。

### GenerateSupervisor getGenerateSupervisor()

generateSupervisorを返します。

### setReincarnation(ObstacleModel ob)

thisが消える時に召喚するObstacleModelであるreincarnationを設定します。

### ObjectModel getReincarnation()

reincarnationを返します。

### void setRevivalPoint(Point2 pt)

reincarnationが召喚される位置を指定します。ここに明確にnullを渡すことにより、召喚位置をthisの消えた位置にする事もできます。既定もnullです。

### void setHitPoint(int hitPoint)

hitPointを設定します。

### int getHitPoint()

hitPointの値を返します。

### void setCollisionDamage(int collisionDamage)

this.collisionDamageを設定します。

### int getCollisionDamage()

collisionDamageを返します。

### void damage(int damage)

thisが耐久を持っていれば、hitPointをmax(hitPoint-damage, 0)に変更します。

### void setObjectCamp(ObjectCamp camp)

thisの所属する陣営を設定します。詳しくは[ObjectCamp](../others/ObjectCamp.md)をご覧下さい。

### void summonServant()

servantを召喚します。召喚にはservant, generateBehaviorが設定されている必要があります。これらが設定されない限り何もしません。

### boolean inCamera()

thisがカメラ内に入っているかを返します。

### void reaction(ObjectModel o)

thisがoと衝突した時の反応を定義します。ここでは何もしないので、オーバーライドしてください。
