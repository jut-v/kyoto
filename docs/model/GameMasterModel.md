# GameMasterModel

Assignee: @kk07

## フィールド

- GameMode mode
  - 現在のGameModeを保有している

## メソッド

### GameMode getMode()

現在のGameModeを返す

### void setMode(GameMode mode)

ゲームモードを変更する。また、propertyChangeSupportでイベントを発火し変更を伝える("gameMode", old, this.mode)

### ArrayList<GameInfo> getGameInfoList(String dirPath)

dirPathに存在するゲームのGameInfoを一覧にして返す

### void openGame(GameInfo gameInfo, GameMode gameMode)

gameを開く

### void createNewGame(String gameName)

新しいgameを作る。なお、その課程でMapが作成されるためpropertyChangeが発火される。

### setGameName(String gameName)

gameNameを変更する。propertyChangeも発火される。

### gameStart()

ゲームを開始します。
propertyChangeが発火します。(null, mapModel)を渡します。

### nextMap()

次のMapに移るときに呼びます。
propertyChangeを発火します。次のMapがある場合はMAP_CHANGEDを、次のMapがない場合はGAME_FINISHEDです。
