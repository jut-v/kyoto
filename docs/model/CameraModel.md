# CameraModel

Assignee: @kk07

## フィールド

## メソッド

### CameraModel(Rect camPos)

camPosの位置でカメラを作成する

### inCamera(ObjectModel objectModel)

objectModelがカメラに含まれているかを確認する

### setLinearMove(Rect limitRect, float xSpeed, float ySpeed)

CameraModelのmoveが呼ばれるたびに(xSpeed,ySpeed)だけcameraModelが動くようになる。

また、limitRect外に出ようとする場合はそこで動きが止まる。

### setFollowMove(ObjectModel targetModel, Rect limitRect, boolean horizontalLock, boolean verticalLock)

limitRect内でtargetModelを追いかけつねにカメラの中心にあるようにする。なお、horizontalLockやverticalLockがtrueの場合、その方向へは移動しない

### setCustomMove(CameraCustomMover cameraCustomMover)

cameraCustomMoverのcameraCustomMoveで定義されたような動き方をする。

### move()

定義された動き方に沿って移動する。
