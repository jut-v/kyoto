# MapModel

Assignee: @kk07

## フィールド (変数)

- `private Image background`;
  - 背景
- `private ArrayList<ObjectModel> objectModels`
  - Mapにあるオブジェクトモデルの集合

## メソッド

### MapModel(String name, String backgroundImagePath)

背景のパスをStringで指定、名前をstringで指定。

### Image getBackground()

backgroundのImageのインスタンスを返却

### ArayList<ObjectModel> getObjectModels

ObjectModelのインスタンスのリストを返す

### ObjectModel addObjectModel(Point2 point2, String path)

ObjectModelをpoint2の座標でかつ、背景をpathで指定された画像で作成し、objectModelsに格納する
また、propertyChangeSupportでイベントを発火し変更を伝える("addedObjectModel", null, objectModel)

### removeObstacleModel(ObjectModel removeObjectModel)

指定されたobjectModelをリストから削除する
また、propertyChangeSupportでイベントを発火し変更を伝える("removedObjectModel", null, removeObjectModel)

### ArrayList<ObjectModel> getObjectModels

Mapの保有するObjectModelをArrayListで返却する

### addActiveObstacleModel(ObjectModel objectModel)

objectModelがカメラに入ったら呼ばれます

### removeActiveObstacleModel(ObjectModel objectModel)

objectModelがカメラから出たら呼ばれます

### gameStarted

gameが開始されたら呼ばれます

### gameStop

gameが終了したら呼ばれます

### gamePause

gameが一時停止されたら呼ばれます
