# ObstacleModel

Assignee: @tkm3845

- extends [ObjectModel](./model/ObjectModel.md)

## ObstacleModel(Rect rt, MapModel map)

rtは当たり判定で、mapは所属するMapModelです。引数の情報を持ったObstacleModelを作成します。

## ObstacleModel(Rect rt, String path, MapModel map)

pathは画像ファイルへのパスです。引数の情報を持ったObstacleModelを作成します。

## ObstacleModel(ObstacleModel ob)

コピーコンストラクタです。但し、**当たり判定や画像のRect以外はコピーされず、同じものを参照するので注意してください。**

## ObstacleModel(ObstacleModel ob, Point2 initPoint)

基本的に通常のコピーコンストラクタと同じ動作をしますが、initPointで初期化する点で異なります。主に召喚のために使われます。

## void setBodyBehavior(Behavior behavior)

behaviorを用いて当たり判定のBehaviorをセットします。

## void setBodyBehavior(String formulaX,String formulaY)

formulaX, formulaYを用いて当たり判定のBehaviorをセットします。

## void setBodyBehavior(Vector2 vector, float time)

vector, timeを用いて当たり判定のBehaviorをセットします。

## void Behavior getBodyBehavior()

当たり判定のBehaviorを返します。behaviorの使い回しなどの用途が想定されています。

## boolean live()

thisが生きているかを返します。具体的には、一度もアクティブマップに登場していない、または一度もアクティブマップから排除されていない、を満たすかを返します。

## setReactionMode(ReactionMode mode)

反応の種類をmodeに設定します。既定は

- キャラクター:
  ReactionMode.VANISH
- キャラクター以外:
  ReactionMode.NEUTRAL

となっています。thisがキャラクターかそれ以外に分類し直される度に、modeは既定に変更されます。

## ReactionMode getReactionMode()

thisに設定されている反応の種類を返します。

## setHitPoint(int hitPoint)

super.setHitPoint(hitPoint)を行った後、下記を行います。

- hitPoint==ObjectModel.STAGEなら:
  thisの反応の種類をReaction.NEUTRALに変更
- hitPoint!=ObjectModel.STAGEなら:
  thisの反応の種類をReaction.VANISHに変更

## void damage(int damage)

super(damage)を行った後、hitPoint==0ならばthisをthis.map.ActiveObjectModelsから削除します。

## void reaction(ObjectModel o)

thisがoと衝突した時の反応を取ります。

## void move()

thisを次の位置に動かした上、カメラの外に出た時のthisの削除とObjectModelへの衝突時の反応、弾幕などの召喚を行います。
