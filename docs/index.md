# Kyoto ドキュメント

ちゃんと書きましょう。

## Model

- [GameMasterModel](./model/GameMasterModel.md)
- [ObjectModel](./model/ObjectModel.md)
  - [CharacterModel](./model/CharacterModel.md)
    - [PlayableCharacterModel](./model/PlayableCharacterModel.md)
    - [EnemyCharacterModel](./model/EnemyCharacterModel.md)
  - [ObstacleModel](./model/ObstacleModel.md)
- [MapModel](./model/MapModel.md)
- [CameraModel](./model/CameraModel.md)

## View & Controller

- [GameMasterView](./view/GameMasterView.md)
  View全体の切り替えを行います。
- [ContextMenuView](./view/ContextMenuView.md)
  右クリックで表示されるメニューの表示を行います。
  - [ObjectCollectionView](./view/ObjectCollectionView.md)
    設置可能なオブジェクトのメニューを表示します。
  - [ObjectPropertiesView](./view/ObjectPropertiesView.md)
    オブジェクトの設定を行うためのメニューを表示します。
- [NavigationView](./view/NavigationView.md)
  上部のナビゲーションメニューの表示を行います。
- [StatusView](./view/StatusView.md)
  プレイアブルキャラクターや敵キャラクターのステータスや、マップの情報などの表示を行います。
- [GameSelectView](./view/GameSelectView.md)
  プレイモード、編集モードの選択や、ゲームの選択画面の表示を行います。
- [PlayView](./view/PlayView.md)
  プレイモードでの画面の表示を行います。
- [EditView](./view/EditView.md)
  編集モードでの画面の表示を行います。
- [MapView](./view/MapView.md)
  マップの背景など、マップに固有の情報の表示を行います。
  - [PlayMapView](./view/PlayMapView.md)
    プレイモードでのマップの表示を行います。
  - [EditMapView](./view/EditMapView.md)
    編集モードでのマップの表示を行います。
    - [EditObjectsMapView](./view/EditObjectsMapView.md)
      編集モードでのマップに設置されているオブジェクトの表示を行います。
    - [EditCollidersMapView](./view/EditCollidersMapView.md)
      編集モードでのコライダーの範囲の描画と、コライダーの設置用のUIの表示を行います。
- [ObjectView](./view/ObjectView.md)
  マップに表示されるオブジェクトの表示を行います。
  - [PlayObjectView](./view/PlayObjectView.md)
    プレイモードでのオブジェクトの位置情報や背景情報の表示を行います。
    - [PlayPlayableCharacterView](./view/PlayPlayableCharacterView.md)
      プレイアブルキャラクターの表示を行います。
    - [PlayEnemyCharacterView](./view/PlayEnemyCharacterView.md)
      敵キャラクターの表示を行います。
    - [PlayObstacleView](./view/PlayObstacleView.md)
      障害物の表示を行います。
  - [EditObjectView](./view/EditObjectView.md)
    編集モードでの障害物の表示や、右クリック時のコンテキストメニューの表示までを行います。

## Others

- [Behavior](./others/Behavior.md)
- [Vector2](./others/Vector2.md)
- [Point2](./others/Point2.md)
- [Rect](./others/Rect.md)
