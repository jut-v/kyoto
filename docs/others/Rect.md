# Rect class

矩形を保持する

## フィールド (変数)

- `private Point2 p1, p2`;
  - `p1`が`p2`よりもxもyも短くなるように代入される

## メソッド

### Rect()

[Point2](./Point2.md)が引数なしに生成されたものが`p1`、`p2`に代入される。

### Rect(Point2 p1, Point2 p2)

`p1`と`p2`で指定された点が頂点となる矩形が作成される。

インスタンス変数の`p1`、`p2`には、正規化された点が代入される。

### Rect(Point2 p, float w, float h)

`p`を頂点とし、幅`w`、高さ`h`の矩形を生成する。

### boolean hasPoint2(Point2 p)

`p`が矩形の内部にあるかを返す。

### void move(float dx, float dy)

x軸方向に`dx`、y軸方向に`dy`だけ動かす。

### boolean wholeIn(Rect r)

rが完全に自分自身に入っているかどうかを調べます。入っていたらtrueを返します。
