# Behavior

Assignee: @tkm3845

## Behavior(String formulax,String formulaY)

### 表面的説明

x, yそれぞれについて、引数で渡された関数式を設定します。

### 内部的説明

式のビルダーを保持します。

## Point2 func(HashMap<String,Float> argX,HashMap<String,Float> argY)

### 表面的説明

x, yについて、設定した関数の引数に対応する値をそれぞれargX, argYとして渡すことにより、その時の(x, y)をPoint2として返します。

### 内部的説明

基本的には上記の通りですが、最初の1回では与えられた関数をビルドしています。
