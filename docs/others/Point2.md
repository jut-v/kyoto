# Point2

Assignee: @kk07

## Point2()

x = 0, y = 0のPoint2を作ります。

## Point2(float x, float y)

x = x, y = yのPoint2を作ります。

## float calcDistance()

原点からの距離を求めます。

## float calcDistance(Point2 point2)

2点間の距離を求めます。

## calcSqrDistance()

原点からの距離の2乗を求めます。パフォーマンスを求める場合などに使用してください。

## calcSqrDistance(float x, float y)

2点間の距離の2乗を求めます。パフォーマンスを求める場合などに使用してください。
