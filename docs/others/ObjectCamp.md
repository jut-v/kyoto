# ObjectCamp

Assignee: @tkm3845

## 概要

ObjectModelの所属する陣営を表わします。

## 陣営の酒類とその説明

- STAGE:
  ステージです。中立的立場で、プレイヤーにもエネミーにもダメージを与える可能性があります。この陣営のインスタンスが保持する反応がReactionMode.NEUTRALでなければ、この陣営の反応が優先されます。
- PLAYER:
  プレイヤー陣営です。PlayableCharacterModelの召喚したObstacleModelもこれに入ります。
- ENEMY:
  敵陣営です。