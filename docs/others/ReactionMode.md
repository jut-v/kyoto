# ReactionMode

Assignee: @tkm3845

## 概要

ObstacleModelの反応の仕方を表わします。

## 反応の種類とその説明

- NEUTRAL:
  重なり合わないように修正だけします。
- VANISH:
  消滅します。
- REFLECT:
  ぶつかったものに関して反射し(され)ます。
- PHANTOM:
  何もせず素通りします。
