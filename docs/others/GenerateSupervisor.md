# GenerateSupervisor

Assignee: @tkm3845

## 概要

$\LaTeX$記法を用いて詳細に説明します。先ず、関数$f:\mathbb N\to\mathbb R;t\mapsto f(t)$を定義し、それに対して1秒ごとに、つまり$t\in\mathbb N$について
$$
\min(\max(\lfloor f(t)\rfloor,0),\hbox{FPS})
$$
を求め、これをその1秒で召喚する個数とします。最後に、これをなるべく均等な間隔にする事で、召喚するタイミングを定めます。

## メソッドの説明

### GenerateSupervisor(String formula)

上記の$f(t)$を設定します。

### boolean judge(int count)

cout/FPS秒における判断を返します。trueで召喚して、falseで召喚しないでください。

### String getFormula()

thisが生成される時に与えられた式を返します。
