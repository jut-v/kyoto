# Vector2

Assignee: @kk07

## Vector2()

0ベクトルを作ります。

## Vector2(float x, float y)

ベクトル(x, y)を作ります。

## float calcMagniude()

ベクトルの長さを返します。

## float calcSqrMagnitude()

ベクトルの長さの2乗を返します。パフォーマンスを優先する場合に使ってください。

## Vector2 normalized()

単位ベクトルに変換したベクトルを返します。

## Vector2 add(Vector2 vector2)

ベクトルの足し算をします。

## Vector2 sub(Vector2 vector2)

ベクトルの引き算をします。this - vector2をした結果を返します。

## float dotProducts(Vector2 vector2)

ベクトルの内積を返します。

## float arg(Vector2 vector2)

2つのベクトルのなす角の大きさを0~$\pi$までのラジアンで返します。
