# GameInfoListView

- package ui.select
- implements [Clickable](./../Clickable.md)
- has a ArrayList of [GameInfoView](./GameInfoView.md)
- has a [GameInfoListViewSupportable](./GameInfoListViewSupportable.md)

## GameInfoListView(GameInfoListViewSupportable parent, Rect listSize, ArrayList&lt;GameInfo&gt; gameInfoList)

以下のものを引数として受け取ります。

- 呼び出されたクラス
- 描画すべき位置と大きさ
- 描画すべきGameInfoの情報

## void draw()

このUIを描画するためのメソッドです。

[GameInfoView](./GameInfoView.md)を用いて、ゲームの情報を描画します。

## Rect getArea()

UIが描画される範囲をRectで返します。

## Boolean clicked()

UIがクリックされたときに呼び出されるためのメソッドです。

[GameInfoView](./GameInfoView.md)がクリックされていた場合はその要素のclicked()呼び出し、`true`を返します。
そうでないときは、`false`を返します。
