# interface GameInfoListViewSupportable

[GameInfoListView](./GameInfoListView.md)を持つクラスのためのインターフェースです。

## gameInfoSelected(GameInfo gi)

ゲームが選択されたときに呼び出されます。
引数として、選択されたゲームのGameInfoを受け取ります。
