# interface Clickable

クリック可能なUIのクラスに追加するインターフェースです。

## Rect getArea()

要素の位置と大きさをRectで返します。

## boolean clicked(Point2 p, MouseEvent e)

クリックされたときの動作を定義します。

要素がクリックされたときに、そこで動作を終了させたいときは`true`、動作を継続したいときは`false`を返します。
