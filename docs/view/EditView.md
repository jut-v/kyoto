# EditView

Assignee: @jut-v

編集モードを描画するためのクラスです。
EditMapViewとNavigationViewを呼び出します。

マウスやキー操作はEditControllerにより受け付けられます。

- extends JPanel
- implements UITextFieldSupportable
- has a GameMasterModel
- has a MapModel
- has a EditMapView
- has a NavigationView
- has a EditController

## 主なフィールド

- GameMasterModel
- MapModel currentMapModel
  - 現在表示しているMapModel

## メソッド

### このクラスで新しく定義されたメソッド

#### EditView(GameMasterModel gmm)

EditViewを作成します。

#### void updateView()

この子クラスのViewから呼ばれ、このクラスの再描画を行います。

#### ArrayList&lt;MapModel&gt; getMapModelList()

NavigationViewから呼び出され、GameMasterModelから取得したMapModelのArrayListを返します。

#### MapModel getCurrentMapModel()

NavigationViewにより呼び出され、現在表示しているマップを返します。

#### void ChangeCurrentMap(String mapName)

NavigationViewにより呼び出され、表示するマップを切り替えます。

#### void createNewMap()

NavigationViewにより呼び出され、新しいマップを作成します。

#### void changeMapName()

NavigationViewにより呼び出され、現在表示しているマップ名を変更するためのUITextFieldを呼び出します。

#### changeMapBackground()

NavigationViewにより呼び出され、現在のマップの背景を変更します。

### JPanelのメソッド

#### void paintComponent(Graphics g)

mapViewとnavViewを描画します。

### UITextFieldSupportableのメソッド

#### void changeText(String text)

変更したいマップ名を受け取り、マップ名を変更します。
