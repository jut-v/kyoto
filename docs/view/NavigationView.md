# NavigationView

Assignee: @jut-v

- implements Clickable, DropDownMenuViewSupportable, ButtonSupportable
- has a EditView
- has a Rect
- has a createNewMapButton
- has a changeMapNameButton
- has a changeMapBackgroundButton

## 主なフィールド

- EditView ev
  - このViewを呼び出したEditView
- Rect r
  - このViewが描画される範囲

## メソッド

### 新しく定義されたメソッド

#### NavigationMenu(EditView ev, Rect r)

ナビゲーションメニューを指定された領域に作成します。

#### void draw(g)

ナビゲーションメニューを描画します。

### Clickableにより定義されたメソッド

#### boolean clicked(Point2 p, MouseEvent e)

ボタンやメニューがクリックされたときはtrue、そうでないときはfalseを返します。

### DropDownMenuViewSupportableにより定義されたメソッド

#### void menuSelected()

EditViewを再描画します。

#### void menuItemSelected(DropDownMenuItemContent content)

マップを切り替えた後にEditViewを再描画します。

### ButtonSupportableにより定義されたメソッド

#### void buttonSelected(BasicButton b)

クリックされたボタンを判定して、そのボタンに合わせてマップ作成、マップ名変更、背景変更を行います。

### 複数のインターフェースにより定義されたメソッド

#### Rect getArea()

ドロップダウンメニューが開かれていないときはr、開かれているときはrの高さをそのメニューの高さにしたものを返します。
