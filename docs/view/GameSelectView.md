# GameSelectView

Assignee: @jut-v

- extends JPanel
- implements MouseListenter
- implements [GameInfoListViewSupportable](./ui/select/GameInfoListViewSupportable.md)
- has a [GameMasterModel](../model/GameMasterModel.md)
- has a [GameInfoListView](./ui/select/GameInfoListView.md)
- has a ui.text.UIText
- has a ui.button.BasicButton

## メソッド

### このクラスで新しく定義されたメソッド

#### GameSelectView(GameMasterModel gmm)

Viewを生成するコンストラクタです。

### GameInfoListViewSupportableによるメソッド

#### void gameInfoSelected(GameInfo g)

[GameInfoListView](./ui/select/GameInfoListView.md)でゲームが選択されたときに呼び出されます。
