# GameMasterView

Assignee: @jut-v

選択、プレイ、編集モードの画面を切り替えるためのクラスです。

- extends JFrame
- implements PropertyChangeListener
- has a GameMasterModel
- has a GameSelectView
- has a PlayView
- has a EditView

## フィールド

- GameMasterModel
  - GameMasterModelのListenerに登録し、モードの変更を受け取ります。
- GameSelectView
  - このクラスが作成されたときに作成され、描画されます。
- PlayView
  - プレイモードになったときに作成され、描画されます。
- EditView
  - 編集モードになったときに作成され、描画されます。

## メソッド

### PropertyChangeListenerによるクラス

#### propertyChange(PropertyChnageEvent e)

gameModeプロパティを受け取り、ゲームモードを切り替えます。
